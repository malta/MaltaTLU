#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>

using namespace std;

int main(int argc, char **argv)
{
	QApplication app (argc, argv);
	QWidget window;
	window.setFixedSize(800, 800);
	QPushButton *button1 = new QPushButton("Plane 1", &window);
	button1->setGeometry(10, 10, 80, 30);
	QPushButton *button2 = new QPushButton("Plane 2", &window);
	button2->setGeometry(10, 60, 80, 30);
	window.show();
	return app.exec();
}
