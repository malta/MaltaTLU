#include "MaltaTLU/MaltaTLUdev.h"
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <bitset>
#include <cstring>
/*************************************
 * Base class for TLU control
 *
 * Include more slow control methods
 * @author Ignacio.Asensi@cern.ch
 * @author Carlos.Solans@cern.ch
 * @date Sep 2019
 * @date Feb 2020
 * @date March 2023 dev
 ************************************/
using namespace std;



MaltaTLUdev::MaltaTLUdev(){
  cout << endl << "TLU dev"  << endl;
  m_verbose=false;
  m_ipb=0;
}

MaltaTLUdev::~MaltaTLUdev(){
  if(m_ipb) delete m_ipb;
}

uint32_t MaltaTLUdev::GetVersion() {
  uint32_t value;
  m_ipb->Read(TLU_VERSION,value);
  return value;
}

void MaltaTLUdev::SetVerbose(bool val) {
  m_verbose=val;
  cout << " Verbose: " << val << endl;
}

void MaltaTLUdev::Connect(std::string connstr){
  if(m_verbose) cout << "Connecting to... " << connstr << endl;
  m_ipb = new ipbus::Uhal(connstr);
  if(m_verbose) cout << "Connected"  << endl;

}

uint32_t MaltaTLUdev::GetCounter(std::string plane){
  uint32_t cacheWord;
  int ipb_reg=0;
  std::string ipb_reg_str;
  if (plane.compare("L1A")==0){ipb_reg=TLU_COUNTER[0];     ipb_reg_str="TLU_COUNTER[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_COUNTER[1];   ipb_reg_str="TLU_COUNTER[1]";}
  else if(plane.compare("2")==0){ipb_reg=TLU_COUNTER[2];   ipb_reg_str="TLU_COUNTER[2]";}
  else if(plane.compare("3")==0){ipb_reg=TLU_COUNTER[3];   ipb_reg_str="TLU_COUNTER[3]";}
  else if(plane.compare("4")==0){ipb_reg=TLU_COUNTER[4];   ipb_reg_str="TLU_COUNTER[4]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_COUNTER[6];  ipb_reg_str="TLU_COUNTER[6]\tSC!";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_COUNTER[7]; ipb_reg_str="TLU_COUNTER[7]\tSC2!";}
  else if(plane.compare("HGTD")==0){ipb_reg=TLU_COUNTER[8]; ipb_reg_str="TLU_COUNTER[8]\tHGTD!";}
  else{ cout << "GetCounter: Plane not defined!! [" << plane << "]" <<  endl;return 0;}
  m_ipb->Read(ipb_reg, cacheWord);
  if(m_verbose) cout << endl <<  "GetCounter:\t Plane[" << plane << "].\t Value:\t" << cacheWord << ".\t IPBUS REG:\t" << ipb_reg_str << "ipbusreg: " << ipb_reg <<  endl;
  return cacheWord;
}


void MaltaTLUdev::SetVeto(std::string  plane, int value){//old SetLongInput
  if (value>=FWlimit_veto){cout << "Veto is too high!!!! Check FW limit!!";}
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_VETO[0];  ipb_reg_str="TLU_VETO[0]\tl1A MAX RATE";}
  else if(plane.compare("1")==0){ipb_reg=TLU_VETO[1];  ipb_reg_str="TLU_VETO[1]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_VETO[2];  ipb_reg_str="TLU_VETO[2]\tSC";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_VETO[3];  ipb_reg_str="TLU_VETO[3]\tSC2";}
  else{ cout << "ERROR!!! SetVeto: Plane not defined!!" << endl;}

  m_ipb->Write(ipb_reg, value);
  if(m_verbose) cout << endl <<  "SetVeto:\t" << value <<  ".\tIPBUS REG: " << ipb_reg_str <<   "\t["<< ipb_reg<<"]" << endl;
}

void MaltaTLUdev::SetWidth(std::string plane, int value){
  if (value>=FWlimit_width){cout << "Width is too high!!!! Check FW limit!!";}
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_WIDTH[0];  ipb_reg_str="TLU_WIDTH[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_WIDTH[1];    ipb_reg_str="TLU_WIDTH[1]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_WIDTH[2];   ipb_reg_str="TLU_WIDTH[2]\tSC";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_WIDTH[3];  ipb_reg_str="TLU_WIDTH[3]\tSC2";}
  else{ cout << "ERROR!!! SetWidth: Plane not defined!!" << endl;}

  m_ipb->Write(ipb_reg, value);
  if(m_verbose) cout << endl <<  "SetWidth:\t" << value <<  ".\tIPBUS REG: " << ipb_reg_str <<   "\t["<< ipb_reg<<"]" << endl;
}
uint32_t  MaltaTLUdev::GetVeto(std::string  plane){//old SetLongInput
  uint32_t cacheWord;
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_VETO[0];  ipb_reg_str="TLU_VETO[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_VETO[1];  ipb_reg_str="TLU_VETO[1]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_VETO[2];  ipb_reg_str="TLU_VETO[2]\tSC!";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_VETO[3];  ipb_reg_str="TLU_VETO[3]\tSC2!";}
  else{ cout << "ERROR!!! GetVeto: Plane not defined!!" << endl;return 0;}

  m_ipb->Read(ipb_reg, cacheWord);
  if(m_verbose) cout << endl <<  "GetVeto:\t Plane[" << plane << "].\t Value:\t" << cacheWord << ".\t IPBUS REG:\t" << ipb_reg_str << endl;
  return cacheWord;
}
uint32_t MaltaTLUdev::GetWidth(std::string plane){
  uint32_t cacheWord;
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_WIDTH[0];  ipb_reg_str="TLU_WIDTH[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_WIDTH[1];  ipb_reg_str="TLU_WIDTH[1]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_WIDTH[2];  ipb_reg_str="TLU_WIDTH[2]\tSC";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_WIDTH[3];  ipb_reg_str="TLU_WIDTH[3]\tSC8";}
  else{ cout << "ERROR!!! GetWidth: Plane not defined!!" << endl;return 0;}

  m_ipb->Read(ipb_reg, cacheWord);
  if(m_verbose) cout << endl <<  "GetWidth:\t Plane[" << plane << "].\t Value:\t" << cacheWord << ".\t IPBUS REG:\t" << ipb_reg_str << endl;
  return cacheWord;
}

void MaltaTLUdev::SetMain(bool enable, bool plane1, bool plane2, bool plane3, bool plane4, bool SCenable, bool SC2enable, bool HGTDenable, bool diffOutenable){
		//bool plane1, bool plane2, bool plane3, bool plane4, bool plane5, bool plane6, bool plane7, bool plane8){
  uint32_t p0=plane1 ;
  uint32_t p1=plane2 << 1;
  uint32_t p2=plane3 << 2;
  uint32_t p3=plane4 << 3;
  uint32_t p4=enable << 4;
  uint32_t p5=SCenable << 5;
  uint32_t p6=SC2enable << 6;
  uint32_t p7=HGTDenable << 7;
  uint32_t p8=diffOutenable << 8;
  uint32_t val = 0 | p0 | p1 | p2 | p3 | p4 | p5 | p6 | p7 | p8;
  m_ipb->Write(TLU_MAIN, val);
  if(m_verbose) cout << endl << "Set Main register:" << bitset<32>(val) <<  ". IPBUS REG: " << TLU_MAIN << endl;
}


uint32_t MaltaTLUdev::GetMain(){
  uint32_t cacheWord;
  m_ipb->Read(TLU_MAIN, cacheWord);
  if(m_verbose) cout << endl << "Get Main register:" << bitset<32>(cacheWord) <<  ". IPBUS REG: " << TLU_MAIN << endl;
  return cacheWord;
}



/*
stringstream MaltaTLUdev::systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}
*/
