/************************************
 * MaltaTLU
 * Brief: Python module for MaltaTLU
 * 
 * Author: Carlos.Solans@cern.ch
 *         Ignacio.Asensi@cern.ch
 * v2
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include <MaltaTLU/MaltaTLU.h>
#include <iostream>
#include <vector>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif


#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)\
  static struct PyModuleDef moduledef = {\
		     PyModuleDef_HEAD_INIT, name, doc,-1, methods\
  };\
  m = PyModule_Create(&moduledef);\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)\
  ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000


/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MaltaTLU *obj;
  std::vector<uint32_t> vec;
} PyMaltaTLU;

static int _PyMaltaTLU_init(PyMaltaTLU *self)
{

    self->obj = new MaltaTLU();
    return 0;
}

static void _PyMaltaTLU_dealloc(PyMaltaTLU *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyMaltaTLU_GetVersion(PyMaltaTLU *self)
{
    PyObject *py_ret;
    uint32_t ret = self->obj->GetVersion();
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}


PyObject * _PyMaltaTLU_SetVerbose(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_flag1 = NULL;
    if (!PyArg_ParseTuple(args, (char *) "O",&py_flag1)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool p1 = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
    self->obj->SetVerbose(p1);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_Connect(PyMaltaTLU *self, PyObject *args)
{
    char * str;
    if(PyArg_ParseTuple(args, (char *) "s",&str)){
      self->obj->Connect(std::string(str));
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_GetCounter(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_ret;
    char *plane;
    if (!PyArg_ParseTuple(args, (char *) "s",&plane)){
          Py_INCREF(Py_None);
          return Py_None;
        }
    uint32_t ret = self->obj->GetCounter(plane);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}


PyObject * _PyMaltaTLU_SetVeto(PyMaltaTLU *self, PyObject *args)
{
    uint32_t val;
    char *plane;
    if (!PyArg_ParseTuple(args, (char *) "sI",&plane,&val)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->SetVeto(plane, val);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_SetWidth(PyMaltaTLU *self, PyObject *args)
{
    uint32_t val;
    char *plane;
    if (!PyArg_ParseTuple(args, (char *) "sI",&plane,&val)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->SetWidth(plane, val);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_GetVeto(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_ret;
    char * plane;
    if (!PyArg_ParseTuple(args, (char *) "s",&plane)){
          Py_INCREF(Py_None);
          return Py_None;
        }
    uint32_t ret = self->obj->GetVeto(plane);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMaltaTLU_GetWidth(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_ret;
    char * plane;
    if (!PyArg_ParseTuple(args, (char *) "s",&plane)){
          Py_INCREF(Py_None);
          return Py_None;
        }
    uint32_t ret = self->obj->GetWidth(plane);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMaltaTLU_SetNumberOfPlanes(PyMaltaTLU *self, PyObject *args)
{
  PyObject *py_flag1 = NULL;
  PyObject *py_flag2 = NULL;
  PyObject *py_flag3 = NULL;
  PyObject *py_flag4 = NULL;
  PyObject *py_flag5 = NULL;
  PyObject *py_flag6 = NULL;
  PyObject *py_flag7 = NULL;
  PyObject *py_flag8 = NULL;


  if (!PyArg_ParseTuple(args, (char *) "OOOOOOOO",&py_flag1,&py_flag2,&py_flag3,&py_flag4,&py_flag5,&py_flag6,&py_flag7,&py_flag8)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool p1 = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
  bool p2 = py_flag2? (bool) PyObject_IsTrue(py_flag2) : false;
  bool p3 = py_flag3? (bool) PyObject_IsTrue(py_flag3) : false;
  bool p4 = py_flag4? (bool) PyObject_IsTrue(py_flag4) : false;
  bool p5 = py_flag5? (bool) PyObject_IsTrue(py_flag5) : false;
  bool p6 = py_flag6? (bool) PyObject_IsTrue(py_flag6) : false;
  bool p7 = py_flag7? (bool) PyObject_IsTrue(py_flag7) : false;
  bool p8 = py_flag8? (bool) PyObject_IsTrue(py_flag8) : false;


  if(py_flag1!=NULL)
  if(py_flag1!=NULL){
    self->obj->SetNumberOfPlanes(p1, p2, p3, p4, p5, p6, p7, p8);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaTLU_GetNumberOfPlanes(PyMaltaTLU *self)
{
    PyObject *py_ret;
    uint32_t ret = self->obj->GetNumberOfPlanes();
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}


PyObject * _PyMaltaTLU_SendTriggerToPlanes(PyMaltaTLU *self, PyObject *args)
{
  PyObject *py_flag1 = NULL;
  PyObject *py_flag2 = NULL;
  PyObject *py_flag3 = NULL;
  PyObject *py_flag4 = NULL;
  PyObject *py_flag5 = NULL;
  PyObject *py_flag6 = NULL;
  PyObject *py_flag7 = NULL;
  PyObject *py_flag8 = NULL;
  PyObject *py_flag9 = NULL;
  PyObject *py_flag10 = NULL;

  if (!PyArg_ParseTuple(args, (char *) "OOOOOOOOOO",&py_flag1,&py_flag2,&py_flag3,&py_flag4,&py_flag5,&py_flag6,&py_flag7,&py_flag8,&py_flag9,&py_flag10)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool p1 = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
  bool p2 = py_flag2? (bool) PyObject_IsTrue(py_flag2) : false;
  bool p3 = py_flag3? (bool) PyObject_IsTrue(py_flag3) : false;
  bool p4 = py_flag4? (bool) PyObject_IsTrue(py_flag4) : false;
  bool p5 = py_flag5? (bool) PyObject_IsTrue(py_flag5) : false;
  bool p6 = py_flag6? (bool) PyObject_IsTrue(py_flag6) : false;
  bool p7 = py_flag7? (bool) PyObject_IsTrue(py_flag7) : false;
  bool p8 = py_flag8? (bool) PyObject_IsTrue(py_flag8) : false;
  bool p9 = py_flag9? (bool) PyObject_IsTrue(py_flag9) : false;
  bool p10 = py_flag10? (bool) PyObject_IsTrue(py_flag10) : false;

  if(py_flag1!=NULL){
    self->obj->SendTriggerToPlanes(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaTLU_GetSendTriggerToPlanes(PyMaltaTLU *self)
{
    PyObject *py_ret;
    uint32_t ret = self->obj->GetSendTriggerToPlanes();
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMaltaTLU_SetSCenable(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_flag1 = NULL;

    if (!PyArg_ParseTuple(args, (char *) "O", &py_flag1)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool flag = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
    self->obj->SetSCenable(flag);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_SetHGTDenable(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_flag1 = NULL;

    if (!PyArg_ParseTuple(args, (char *) "O", &py_flag1)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool flag = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
    self->obj->SetHGTDenable(flag);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_SetSC2enable(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_flag1 = NULL;

    if (!PyArg_ParseTuple(args, (char *) "O", &py_flag1)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool flag = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
    self->obj->SetSC2enable(flag);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_Enable(PyMaltaTLU *self, PyObject *args)
{
    PyObject *py_flag1 = NULL;
    if (!PyArg_ParseTuple(args, (char *) "O",&py_flag1)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool flag = py_flag1? (bool) PyObject_IsTrue(py_flag1) : false;
    self->obj->Enable(flag);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_Reset(PyMaltaTLU *self)
{
    self->obj->Reset();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMaltaTLU_ResetCounters(PyMaltaTLU *self)
{
    self->obj->ResetCounters();
    Py_INCREF(Py_None);
    return Py_None;
}

////////////////////////////////////////////////////////////////

static PyMethodDef PyMaltaTLU_methods[] = {
	{(char *) "GetVersion"			,(PyCFunction) _PyMaltaTLU_GetVersion, METH_NOARGS, NULL },
	{(char *) "SetVerbose"			,(PyCFunction) _PyMaltaTLU_SetVerbose, METH_VARARGS, NULL },
	{(char *) "Connect"			,(PyCFunction) _PyMaltaTLU_Connect, METH_VARARGS, NULL },
	{(char *) "GetCounter"		        ,(PyCFunction) _PyMaltaTLU_GetCounter, METH_VARARGS, NULL },
	{(char *) "SetVeto"	               	,(PyCFunction) _PyMaltaTLU_SetVeto, METH_VARARGS, NULL },
	{(char *) "SetWidth"		        ,(PyCFunction) _PyMaltaTLU_SetWidth, METH_VARARGS, NULL },
	{(char *) "GetVeto"		        ,(PyCFunction) _PyMaltaTLU_GetVeto, METH_VARARGS, NULL },
	{(char *) "GetWidth"		        ,(PyCFunction) _PyMaltaTLU_GetWidth, METH_VARARGS, NULL },
	{(char *) "SetNumberOfPlanes"		,(PyCFunction) _PyMaltaTLU_SetNumberOfPlanes, METH_VARARGS, NULL },
	{(char *) "GetNumberOfPlanes"		,(PyCFunction) _PyMaltaTLU_GetNumberOfPlanes, METH_NOARGS, NULL },
	{(char *) "SendTriggerToPlanes"		,(PyCFunction) _PyMaltaTLU_SendTriggerToPlanes, METH_VARARGS, NULL },
	{(char *) "GetSendTriggerToPlanes"	,(PyCFunction) _PyMaltaTLU_GetSendTriggerToPlanes, METH_NOARGS, NULL },
	{(char *) "SetSCenable"			,(PyCFunction) _PyMaltaTLU_SetSCenable, METH_VARARGS, NULL },
	{(char *) "SetSC2enable"		,(PyCFunction) _PyMaltaTLU_SetSC2enable, METH_VARARGS, NULL },
	{(char *) "SetHGTDenable"		,(PyCFunction) _PyMaltaTLU_SetHGTDenable, METH_VARARGS, NULL },
	{(char *) "Enable"			,(PyCFunction) _PyMaltaTLU_Enable, METH_VARARGS, NULL },
	{(char *) "Reset"			,(PyCFunction) _PyMaltaTLU_Reset, METH_NOARGS, NULL },
	{(char *) "ResetCounters"		,(PyCFunction) _PyMaltaTLU_ResetCounters, METH_NOARGS, NULL },
	{NULL, NULL, 0, NULL}
};

PyTypeObject PyMaltaTLU_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMaltaTLU.MaltaTLU",            /* tp_name */
    sizeof(PyMaltaTLU),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    (destructor) _PyMaltaTLU_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    0,                                   /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMaltaTLU_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMaltaTLU_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
    ,0                               /* tp_version_tag */
#endif
};

static PyMethodDef module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMaltaTLU)
{
  PyObject *m;
  MOD_DEF(m, "PyMaltaTLU", NULL, module_methods);
  if(PyType_Ready(&PyMaltaTLU_Type)<0){
    return MOD_ERROR;
  }
  PyModule_AddObject(m, (char *) "MaltaTLU", (PyObject *) &PyMaltaTLU_Type);
  return MOD_RETURN(m);
}
