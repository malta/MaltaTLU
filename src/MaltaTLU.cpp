#include "MaltaTLU/MaltaTLU.h"
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <bitset>
#include <cstring>
/*************************************
 * Base class for TLU control
 *
 * Include more slow control methods
 * @author Ignacio.Asensi@cern.ch
 * @author Carlos.Solans@cern.ch
 * @date Sep 2019
 * @date Feb 2020
 ************************************/
using namespace std;



MaltaTLU::MaltaTLU(){
  m_verbose=false;
  m_ipb=0;
}

MaltaTLU::~MaltaTLU(){
  if(m_ipb) delete m_ipb;
}

uint32_t MaltaTLU::GetVersion() {
  uint32_t value;
  m_ipb->Read(TLU_VERSION,value);
  return value;
}

void MaltaTLU::SetVerbose(bool val) {
  m_verbose=val;
  cout << "Verbose: " << val << endl;
}

void MaltaTLU::Connect(std::string connstr){
  if(m_verbose) cout << "Connecting to... " << connstr << endl;
  m_ipb = new ipbus::Uhal(connstr);
  if(m_verbose) cout << "Connected"  << endl;
  
}

uint32_t MaltaTLU::GetCounter(std::string plane){
  uint32_t cacheWord;
  int ipb_reg=0;
  std::string ipb_reg_str;
  if (plane.compare("L1A")==0){ipb_reg=TLU_COUNTER[0];     ipb_reg_str="TLU_COUNTER[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_COUNTER[1];   ipb_reg_str="TLU_COUNTER[1]";}
  else if(plane.compare("2")==0){ipb_reg=TLU_COUNTER[2];   ipb_reg_str="TLU_COUNTER[2]";}
  else if(plane.compare("3")==0){ipb_reg=TLU_COUNTER[3];   ipb_reg_str="TLU_COUNTER[3]";}
  else if(plane.compare("4")==0){ipb_reg=TLU_COUNTER[4];   ipb_reg_str="TLU_COUNTER[4]";}
  else if(plane.compare("5")==0){ipb_reg=TLU_COUNTER[5];   ipb_reg_str="TLU_COUNTER[5]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_COUNTER[6];  ipb_reg_str="TLU_COUNTER[6]\tSC!";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_COUNTER[7]; ipb_reg_str="TLU_COUNTER[7]\tSC2!";}
  else if(plane.compare("HGTD")==0){ipb_reg=TLU_COUNTER[8]; ipb_reg_str="TLU_COUNTER[8]\tHGTD!";}
  else{ cout << "GetCounter: Plane not defined!! [" << plane << "]" <<  endl;return 0;}
  m_ipb->Read(ipb_reg, cacheWord);
  if(m_verbose) cout << endl <<  "GetCounter:\t Plane[" << plane << "].\t Value:\t" << cacheWord << ".\t IPBUS REG:\t" << ipb_reg_str << "ipbusreg: " << ipb_reg <<  endl;
  return cacheWord;
}

void MaltaTLU::ResetCounters(){
  if(m_verbose) cout << endl << "ResetCounters" << endl;
  m_ipb->Write(TLU_RESET_COUNTERS, 1 );
  m_ipb->Write(TLU_ENABLE, 0 );
  usleep(30);
  m_ipb->Write(TLU_RESET_COUNTERS, 0 );
  if(m_verbose) cout << endl << "Done ResetCounters" << endl;
}



void MaltaTLU::SetVeto(std::string  plane, int value){//old SetLongInput
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_VETO[0];  ipb_reg_str="TLU_VETO[0]\l1A MAX RATE";}
  else if(plane.compare("1")==0){ipb_reg=TLU_VETO[1];  ipb_reg_str="TLU_VETO[1]";}
  else if(plane.compare("2")==0){ipb_reg=TLU_VETO[2];  ipb_reg_str="TLU_VETO[2]";}
  else if(plane.compare("3")==0){ipb_reg=TLU_VETO[3];  ipb_reg_str="TLU_VETO[3]";}
  else if(plane.compare("4")==0){ipb_reg=TLU_VETO[4];  ipb_reg_str="TLU_VETO[4]";}
  else if(plane.compare("5")==0){ipb_reg=TLU_VETO[5];  ipb_reg_str="TLU_VETO[5]";}
  else if(plane.compare("6")==0){ipb_reg=TLU_VETO[6];  ipb_reg_str="TLU_VETO[6]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_VETO[7];  ipb_reg_str="TLU_VETO[7]\tSC";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_VETO[8];  ipb_reg_str="TLU_VETO[8]\tSC2";}
  else{ cout << "ERROR!!! SetVeto: Plane not defined!!" << endl;}
  
  m_ipb->Write(ipb_reg, value);
  if(m_verbose) cout << endl <<  "SetVeto:\t" << value <<  ".\tIPBUS REG: " << ipb_reg_str <<   "\t["<< ipb_reg<<"]" << endl;
}

void MaltaTLU::SetWidth(std::string plane, int value){
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_WIDTH[0];  ipb_reg_str="TLU_WIDTH[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_WIDTH[1];    ipb_reg_str="TLU_WIDTH[1]";}
  else if(plane.compare("2")==0){ipb_reg=TLU_WIDTH[2];    ipb_reg_str="TLU_WIDTH[2]";}
  else if(plane.compare("3")==0){ipb_reg=TLU_WIDTH[3];    ipb_reg_str="TLU_WIDTH[3]";}
  else if(plane.compare("4")==0){ipb_reg=TLU_WIDTH[4];    ipb_reg_str="TLU_WIDTH[4]";}
  else if(plane.compare("5")==0){ipb_reg=TLU_WIDTH[5];    ipb_reg_str="TLU_WIDTH[5]";}
  else if(plane.compare("6")==0){ipb_reg=TLU_WIDTH[6];    ipb_reg_str="TLU_WIDTH[6]\t CHECK THIS VALUE!!";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_WIDTH[7];   ipb_reg_str="TLU_WIDTH[7]\tSC";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_WIDTH[8];  ipb_reg_str="TLU_WIDTH[8]\tSC2";}
  else{ cout << "ERROR!!! SetWidth: Plane not defined!!" << endl;}
  
  m_ipb->Write(ipb_reg, value);
  if(m_verbose) cout << endl <<  "SetWidth:\t" << value <<  ".\tIPBUS REG: " << ipb_reg_str <<   "\t["<< ipb_reg<<"]" << endl;
}
uint32_t  MaltaTLU::GetVeto(std::string  plane){//old SetLongInput
  uint32_t cacheWord;
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_VETO[0];  ipb_reg_str="TLU_VETO[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_VETO[1];  ipb_reg_str="TLU_VETO[1]";}
  else if(plane.compare("2")==0){ipb_reg=TLU_VETO[2];  ipb_reg_str="TLU_VETO[2]";}
  else if(plane.compare("3")==0){ipb_reg=TLU_VETO[3];  ipb_reg_str="TLU_VETO[3]";}
  else if(plane.compare("4")==0){ipb_reg=TLU_VETO[4];  ipb_reg_str="TLU_VETO[4]";}
  else if(plane.compare("5")==0){ipb_reg=TLU_VETO[5];  ipb_reg_str="TLU_VETO[5]";}
  else if(plane.compare("6")==0){ipb_reg=TLU_VETO[6];  ipb_reg_str="TLU_VETO[6]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_VETO[7];  ipb_reg_str="TLU_VETO[7]\tSC!";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_VETO[8];  ipb_reg_str="TLU_VETO[8]\tSC2!";}
  else{ cout << "ERROR!!! GetVeto: Plane not defined!!" << endl;return 0;}
  
  m_ipb->Read(ipb_reg, cacheWord);
  if(m_verbose) cout << endl <<  "GetVeto:\t Plane[" << plane << "].\t Value:\t" << cacheWord << ".\t IPBUS REG:\t" << ipb_reg_str << endl;
  return cacheWord;
}
uint32_t MaltaTLU::GetWidth(std::string plane){
  uint32_t cacheWord;
  int ipb_reg=0;
  std::string ipb_reg_str;
  if(plane.compare("L1A")==0){ipb_reg=TLU_WIDTH[0];  ipb_reg_str="TLU_WIDTH[0]\tL1A";}
  else if(plane.compare("1")==0){ipb_reg=TLU_WIDTH[1];  ipb_reg_str="TLU_WIDTH[1]";}
  else if(plane.compare("2")==0){ipb_reg=TLU_WIDTH[2];  ipb_reg_str="TLU_WIDTH[2]";}
  else if(plane.compare("3")==0){ipb_reg=TLU_WIDTH[3];  ipb_reg_str="TLU_WIDTH[3]";}
  else if(plane.compare("4")==0){ipb_reg=TLU_WIDTH[4];  ipb_reg_str="TLU_WIDTH[4]";}
  else if(plane.compare("5")==0){ipb_reg=TLU_WIDTH[5];  ipb_reg_str="TLU_WIDTH[5]";}
  else if(plane.compare("6")==0){ipb_reg=TLU_WIDTH[6];  ipb_reg_str="TLU_WIDTH[6]";}
  else if(plane.compare("SC")==0){ipb_reg=TLU_WIDTH[7];  ipb_reg_str="TLU_WIDTH[7]\tSC";}
  else if(plane.compare("SC2")==0){ipb_reg=TLU_WIDTH[8];  ipb_reg_str="TLU_WIDTH[8]\tSC8";}
  else{ cout << "ERROR!!! GetWidth: Plane not defined!!" << endl;return 0;}
  
  m_ipb->Read(ipb_reg, cacheWord);
  if(m_verbose) cout << endl <<  "GetWidth:\t Plane[" << plane << "].\t Value:\t" << cacheWord << ".\t IPBUS REG:\t" << ipb_reg_str << endl;
  return cacheWord;
}

void MaltaTLU::SetNumberOfPlanes(bool plane1, bool plane2, bool plane3, bool plane4, bool plane5, bool plane6, bool plane7, bool plane8){
  uint32_t p1=plane1 ;
  uint32_t p2=plane2 << 1;
  uint32_t p3=plane3 << 2;
  uint32_t p4=plane4 << 3;
  uint32_t p5=plane5 << 4;
  uint32_t p6=plane6 << 5;
  uint32_t p7=plane7 << 6;
  uint32_t p8=plane8 << 7;
  uint32_t val = 0 | p1 | p2 | p3 | p4 | p5 | p6 | p7 | p8;
  m_ipb->Write(TLU_PLANES, val);
  if(m_verbose) cout << endl << "SetPlanes:" << bitset<32>(val) <<  ". IPBUS REG: " << TLU_PLANES << endl;
}


uint32_t MaltaTLU::GetNumberOfPlanes(){
  uint32_t cacheWord;
  m_ipb->Read(TLU_PLANES, cacheWord);
  if(m_verbose) cout << endl << "GetPlanes:" << bitset<32>(cacheWord) <<  ". IPBUS REG: " << TLU_PLANES << endl;
  return cacheWord;
}



void MaltaTLU::SendTriggerToPlanes(bool plane1, bool plane2, bool plane3, bool plane4, bool plane5, bool plane6, bool plane7, bool plane8, bool plane9, bool plane10){
  uint32_t p1=plane1 ;
  uint32_t p2=plane2 << 1;
  uint32_t p3=plane3 << 2;
  uint32_t p4=plane4 << 3;
  uint32_t p5=plane5 << 4;
  uint32_t p6=plane6 << 5;
  uint32_t p7=plane7 << 6;
  uint32_t p8=plane8 << 7;
  uint32_t p9=plane9 << 8;
  uint32_t p10=plane10 << 9;
  uint32_t val = 0 | p1 | p2 | p3 | p4 | p5 | p6 | p7 | p8 | p9 | p10;
  m_ipb->Write(TLU_COIN_OUT_PLANES, val);
  if(m_verbose) cout << endl << "SendTriggerToPlanes:\t" << bitset<32>(val) <<  ". IPBUS REG: " << TLU_COIN_OUT_PLANES << endl;
}


uint32_t MaltaTLU::GetSendTriggerToPlanes(){
  uint32_t cacheWord;
  m_ipb->Read(TLU_COIN_OUT_PLANES, cacheWord);
  if(m_verbose) cout << endl << "GetSendTriggerToPlanes:\t" <<  bitset<32>(cacheWord)<<  ". IPBUS REG: " << TLU_COIN_OUT_PLANES << endl;
  return cacheWord;
}



void MaltaTLU::Reset(){
  cout << endl << "RESET NOT AVAILABLE YET" << endl;
}

void MaltaTLU::SetSCenable(bool flag){
  int val;
  if (flag==true){
    val=1;
  }else{
    val=0;
  }
  m_ipb->Write(TLU_SC_ENABLE, val );
  if(m_verbose) cout << endl << "SetSCenable:" <<  val <<  ". IPBUS REG: " << TLU_SC_ENABLE << endl;
}

void MaltaTLU::SetSC2enable(bool flag){
  int val;
  if (flag==true){
    val=1;
  }else{
    val=0;
  }
  m_ipb->Write(TLU_SC2_ENABLE, val );
  if(m_verbose) cout << endl << "SetSC2enable:" <<  val <<  ". IPBUS REG: " << TLU_SC_ENABLE << endl;
}

void MaltaTLU::SetHGTDenable(bool flag){
  int val;
  if (flag==true){
    val=1;
  }else{
    val=0;
  }
  m_ipb->Write(TLU_SC2_ENABLE, val );
  if(m_verbose) cout << endl << "SetHGDTenable:" <<  val <<  ". IPBUS REG: " << TLU_SC_ENABLE << endl;
}

void MaltaTLU::Enable(bool flag){
  if (flag==true){
    if (m_verbose)  cout<< "----------------   Start run   ----------------"<< endl;
    m_ipb->Write(TLU_ENABLE, 1 );
  }else{
    if (m_verbose) cout<< "----------------   Stop run    ----------------"<< endl;
    m_ipb->Write(TLU_ENABLE, 0 );
  }
}


/*
stringstream MaltaTLU::systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}
*/
