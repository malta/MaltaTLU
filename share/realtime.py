#!/usr/bin/env python
##################################################
#
# Realtime plots for monitoring applications
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# May 2016
#
##################################################

import ROOT

class Plot:
    def __init__(self):
        self.build()
        pass
    def __init__(self,name):
        self.build()
        self.name = name
        pass
    def __init__(self,name,title):
        self.build()
        self.name = name
        self.title = title
        pass
    def build(self):
        self.name=None
        self.gv={}
        self.title=None
        self.opts=[]
        #self.leg=ROOT.TLegend(0.60,0.60,0.99,0.90)
        self.drawn=False
        self.ymin=1000000
        self.ymax=-1000000
        self.xmin=0
        self.xmax=0
        pass
    def SetTitle(self,title):
        self.title=title
        pass
    def AddPlot(self,opts, legend_name):
        g = ROOT.TGraph(0)
        i = len(self.gv)
        g.SetName("g%s%i"%(legend_name,i))
        if self.title!=None: 
            print("Set title %s " % self.title)
            g.SetTitle(self.title)
        g.SetMarkerColor(i+1)
        g.SetLineColor(i+1)
        g.SetMarkerStyle(21)
        #self.leg.AddEntry(g,"%s:%i"%(legend_name,i+1),"lp")
        self.gv[i]=g
        self.opts.append(opts)
        pass
    def SetPlot(self,g,opts):
        found=False
        for i in self.gv:
            if self.gv[i]==g: found=True
            pass
        if found==True: return
        i=len(self.gv)
        self.gv[i]=g
        self.opts.append(opts)
        pass
    def AddPoint(self,i,x,y):
        if not i in self.gv:
            print("Index out of range: %i" % i)
            return
        self.gv[i].SetPoint(self.gv[i].GetN(),x,y)
        if y>self.ymax: self.ymax=y
        if y<self.ymin: self.ymin=y
        if x>self.xmax: self.xmax=x
        if x<self.xmin: self.xmin=x
    def GetYmin(self):
        return self.ymin
    def GetYmax(self):
        return self.ymax
    def GetXmin(self):
        return self.xmin
    def GetXmax(self):
        return self.xmax
    def SetLegend(self,leg):
        self.leg = leg
    def Draw(self,opts):
        for i in self.gv:
            if self.gv[i].GetN()==0:
                print("dont draw")
                return
        if self.drawn:
            #print("Set y range = %.2f,%.2f" % (self.ymin,self.ymax))
            self.gv[0].GetYaxis().SetRangeUser(self.ymin*0.9,self.ymax*1.1)
            return
        #print("draw now")
        self.ForceDraw(opts)
        pass
    def ForceDraw(self,opts=""):
        for i in range(len(self.gv)):
            if self.title:
                if i==0: self.gv[i].Draw("A%s"%self.opts[i])
                else: self.gv[i].Draw("SAME%s"%self.opts[i])
        #if self.leg!=None:
        #    #print("Draw the legend")
        #    self.leg.Draw()
        self.drawn = True
        ROOT.gSystem.ProcessEvents()
        pass
    pass
