#!/usr/bin/env python
##############################################
# TLU command tool. This tool requires a configuration file
# ignacio.asensi@cern.ch
# 2020 Sept
# 2023 February updates. Versions are unified
##############################################

import os
import sys
import signal
import subprocess 
import PyMaltaTLUv4
import time
import datetime
import argparse
import array
import configparser
import socket


configFileName="conf_TLU_CMD.ini"
config = configparser.ConfigParser()

# SETUPs settings
cc_factor=3.125 #at 320MHz
current_hostname=socket.gethostname()
maphostnames={}
maphostnames["pcatlidps11"]="ep-ade-gw-02"
maphostnames["pcatlidps04"]="ep-ade-gw-01"
mapports={}
mapports["pcatlidps11"]=50000
mapports["pcatlidps04"]=50007
setupnames={}
setupnames["pcatlidps11"]="SPS"
setupnames["pcatlidps04"]="161"
# CONNECTION SETTINGS
default_hostname= maphostnames[current_hostname] if current_hostname in maphostnames else "ep-ade-gw-XX"
portname= mapports[current_hostname] if current_hostname in mapports else 50000
connection_str="udp://%s:%s" % (default_hostname,str( portname))

# VARS INITIALIZATION
tlu=""
connection_str=""
scintillators={}
widths_cc={} 
widths_ns={} 
vetos_cc={}
vetos_ns={}
planes={}
SC_enabled=True
SC2_enabled=False
HGTD_enabled=True
diffOut_enabled=False
#if hgtd: SC2_enabled=False
scintillators[0]=SC_enabled
scintillators[1]=SC2_enabled

planes[1]=True
planes[2]=True
planes[3]=True
planes[4]=True
MaxRate_hz=20000
MaxRate_ns=50000
widths_ns["L1A"]=50 # Output length
widths_ns["1"]=30
widths_ns["2"]=30
widths_ns["3"]=30
widths_ns["4"]=30
widths_ns["5"]=30
widths_ns["SC"]=33
widths_ns["SC2"]=30
widths_ns["HGTD"]=-1
vetos_ns["L1A"]=100
vetos_ns["1"]=44
vetos_ns["2"]=44
vetos_ns["3"]=44
vetos_ns["4"]=44
vetos_ns["5"]=44
vetos_ns["SC"]=158
vetos_ns["SC2"]=44
vetos_ns["HGTD"]=1
# CONVERTING DEFAULT VALUES (NS) TO CLOCK CYCLES (CC)
for w in widths_ns:
    widths_cc[w]=int(widths_ns[w]/cc_factor)
    pass

for v in vetos_ns:
    vetos_cc[v]=int(vetos_ns[v]/cc_factor)
    pass

MaxRate_cc=int(1/MaxRate_hz*1E3/cc_factor)

class cmd ():
    def __init__(self, v,c):
        global tlu, total_triggers, update_freq
        self.ignorefw=False
        self.currentsetupname=setupnames[current_hostname] if current_hostname in setupnames else "TLU unknown setup!"
        self.verbose= v
        self.reading_counter=0
        self.startingTime=int(time.time())
        self.dt=0
        self.cf=3.125 # conversion factor for 320Mhz clock
        self.print_counters_rate=False
        tlu=PyMaltaTLUv4.MaltaTLUv4()
        tlu.SetVerbose(self.verbose)
        self.loadConfiguration()
        print("Connecting to %s"% c)
        tlu.Connect(c)
        time.sleep(0.1)
        connectedTLU=tlu.GetVersion()
        self.last_count={}
        self.last_count["L1A"]=0
        self.diff=0
        self.total_triggers=0
        HGTD_enabled=False
        update_freq=100
        total_triggers=0
        if self.ignorefw:
            print("Connected to %s" % connection_str)
            print("Checking the firmware version in the TLU has been overridden")
            print("Firmware version [%s]." % str(connectedTLU)) 
        elif connectedTLU == self.valid_fw_version:
            print("ERROR: Firmware is not in the list of supported FWs. Process will continue...")
            pass
        pass
    def start(self):
        # disable TLU
        #self.TLU_reset_counters()
        tlu.SetMain(False,False , False,False , False , False, False, False,False)
        
        if SC_enabled==True:
            tlu.SetVeto("SC",vetos_cc["SC"])
            tlu.SetWidth("SC",widths_cc["SC"])
            pass
        if SC2_enabled==True:
            tlu.SetVeto("SC2",vetos_cc["SC2"])
            tlu.SetWidth("SC2",widths_cc["SC2"])
            pass
        print(self.MaxRate_cc)
        tlu.SetVeto("L1A",self.MaxRate_cc)
        tlu.SetVeto("1", vetos_cc["1"])
        tlu.SetWidth("L1A",widths_cc["L1A"])
        tlu.SetWidth("1", widths_cc["1"])
        tlu.SetMain(True, planes[1], planes[2], planes[3], planes[4], scintillators[0], scintillators[1], HGTD_enabled , diffOut_enabled)
        
        self.reading_counter=0
        pass
    def stop(self):
        tlu.SetMain(False, False, False,False,False, False, False, False, False)
        #tlu.ResetCounters()
        pass
    
    def updateRates(self):
        x="L1A"# for the moment we only monitor L1A
        unit=1#KHz
        cur_value=0
        m_value=0
        m_value= tlu.GetCounter(x)
        self.dt= int(time.time())- self.startingTime
        #print("cur_value: %i" % m_value)
        self.reading_counter+= 1
        self.total_triggers= m_value
        if (float(self.dt)%10<0.1 and self.print_counters_rate == True):
            diff= cur_value - self.last_count[x]
            cur_rate= diff/update_freq
            self.last_count[x]= cur_value
            print("Triggers (FW): %i.\tDt[s]: %i. \tTimes updated: %i." % (self.total_triggers, self.dt, self.reading_counter))
            self.print_counters_rate=False
            pass
        if (float(self.dt)%10>0.1 and self.print_counters_rate== False): self.print_counters_rate= True
        return self.total_triggers
    def loadConfiguration(self):
        if verbose: print("Loading configuration [%s] from %s" % (self.currentsetupname,configFileName))
        config.read(configFileName)
        for x in planes:
            planes[x]=config.getboolean(self.currentsetupname,'planes_%s' % str(x))
            print(planes[x])
            pass
        for x in vetos_cc:
            vetos_cc[x]=config.getint(self.currentsetupname,'vetos_cc_%s' % str(x))
            pass
        for x in vetos_ns:
            vetos_ns[x]=config.getint(self.currentsetupname,'vetos_ns_%s' % str(x))
            pass
        for x in widths_cc:
            widths_cc[x]=config.getint(self.currentsetupname,'widths_cc_%s' % str(x))
            pass
        for x in widths_ns:
            widths_ns[x]=config.getint(self.currentsetupname,'widths_ns_%s' % str(x))
            pass
        for x in scintillators:
            scintillators[x]=config.getboolean(self.currentsetupname,'scintillators_%s' % str(x))
            pass
        differential_output_enabled=config.getboolean(self.currentsetupname,'differential_output_enabled')
        HGTD_enabled=config.getboolean(self.currentsetupname,'HGTD_enabled')
        self.MaxRate_ns = config.getint(self.currentsetupname,'MaxRate_ns')
        self.MaxRate_cc = int(config.getfloat(self.currentsetupname,'MaxRate_cc'))
        self.MaxRate_hz = config.getint(self.currentsetupname,'MaxRate_hz')
        self.L1A_cc = config.getint(self.currentsetupname,'L1A_cc')
        self.valid_fw_version = config.getint(self.currentsetupname,'valid_fw_version')
        self.numberofINplanes = config.getint(self.currentsetupname,'numberofINplanes')
        self.numberofOUTplanes = config.getint(self.currentsetupname,'numberofOUTplanes')
        pass
    pass

if __name__=="__main__":
    # Settings
    cont=True
    def signal_handler(signal, frame):
        print('You pressed ctrl+C')
        print("Stop TLU run")
        global cont
        c.stop()
        cont = False
        return
        pass

    signal.signal(signal.SIGINT, signal_handler)
    # Arguments
    parser=argparse.ArgumentParser()
    parser.add_argument('--start',help="Start run", action='store_true')
    parser.add_argument('--stop',help="Stop run", action='store_true')
    parser.add_argument('-dl','--durationlimit',help="Time [seconds] to stop",type=int,default=100000)
    parser.add_argument('-tl','--triggerlimit',help="Number of triggers to stop.", type=int, default=100000)
    #parser.add_argument('--delay',help="Delay time to start [minutes]. No delay by default",type=int,default=0)
    parser.add_argument('-c','--conf',help="Configuration mode. Default SPS", required="SPS")
    parser.add_argument('-v','--verbose',help="Verbose mode",action='store_true')
    parser.add_argument('-b','--batch',help='sort by noise',action='store_true')
    args=parser.parse_args()
    
    start=args.start
    stop=args.stop
    durationlimit=args.durationlimit
    triggerlimit=args.triggerlimit
    verbose=args.verbose
    #Action!
    c=cmd(verbose,connection_str)
    
    if cont==False:
        print("Stop TLU requested")
    if start and stop:
        print("Error: start and stop are not compatible")
        pass
    elif start:
        print("TLU is set to: Start Run")
        c.start()
        while cont:
            #print("Loop")
            time.sleep(0.1)
            current_triggers=c.updateRates()
            if cont==False:
                print("Run unexpected stop")
                c.stop()
                print("Total triggers: %i" % current_triggers)
                break
            elif current_triggers >= triggerlimit: 
                print("Trigger limit reached. Total triggers: %i" % current_triggers)
                c.stop()
                break
            elif c.dt >= durationlimit: 
                print("Time is over [%i s]. Total triggers: %i" % (durationlimit,current_triggers))
                c.stop()
                break
            pass
        pass
    elif stop:
        c.stop()
        print("TLU is set to: Stop")
        pass
    else:
        print("No action requested!")
    pass

