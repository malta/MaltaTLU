import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import ROOT
import sip
import time

app = QApplication(sys.argv)
win = QWidget()
win.resize(1000,500)

b1 = QPushButton("Button 1")
b2 = QPushButton("Button 2")
master = QHBoxLayout()

vbox1 = QVBoxLayout()
vbox1.addStretch(1)
vbox1.addWidget(b1)
vbox1.addStretch(1)
master.addLayout(vbox1)

vbox2 = QVBoxLayout()
vbox2.addWidget(b2)

# ROOT 
c1 = ROOT.TCanvas("c1", "c1", 800, 800)
hist = ROOT.TH1F("pipo","pipo", 100, 0, 100)

Address = sip.unwrapinstance(win)
Canvas = ROOT.TQtWidget(sip.voidptr(Address).ascobject())
ROOT.SetOwnership( Canvas, False )

vbox2.addWidget(sip.wrapinstance(ROOT.AddressOf(Canvas)[0],QWidget), 0, 0)
hist.Draw()
c1.Update()

master.addLayout(vbox2)
win.setLayout(master)

win.setWindowTitle("ROOT Test")
win.show()
#time.sleep(5)
sys.exit(app.exec_())
