#!/usr/bin/env python
##############################################
# TLU command tool
# ignacio.asensi@cern.ch
# 2020 Sept
##############################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLUv2
import time
import datetime
import argparse
import ROOT
import AtlasStyle
import array
import numpy as np

for_maxrate=[]
tlulog=0
m_avg_rates=0
m_avg_rates2=0
print_counters_rate=True
last=0
max_rate=0

class cmd ():
    def __init__(self, cnf):
        self.verbose= True
        self.startingTime=time.time()
        self.dt=0
        self.cf=3.125 # conversion factor for 320Mhz clock
        global tlu
        global conf
        conf=cnf
        tlu=PyMaltaTLUv2.MaltaTLUv2()
        tlu.SetVerbose(False) #self.verbose)
        tlu.Connect(conf["connect_string"])
        time.sleep(0.1)
        connectedTLU=tlu.GetVersion()
        self.last_count={}
        self.last_count["L1A"]=0
        self.last_count["1"]=0
        self.last_count["2"]=0
        self.last_count["3"]=0
        self.last_count["4"]=0
        self.last_count["SC"]=0
        self.last_count["HGTD"]=0
        
        if connectedTLU not in compatible_TLUs:
            print("ERROR FIRMWARE NOT COMPATIBLE")
            pass
        pass
    def start(self):
        tlu.ResetCounters()
        # disable TLU
        if self.verbose: print("Conf::::")
        if self.verbose: print(conf)
        tlu.Enable(False)
        time.sleep(0.01)
        tlu.ResetCounters()
        time.sleep(0.01)
        tlu.SetNumberOfPlanes(False, False, False,False , False , False, False, False)        
        # Send settings
        for p in conf["planes"]:
            if  conf["planes"][p]==False: continue
            tlu.SetVeto(p, int(conf["veto"][p]/self.cf))
            tlu.SetWidth(p, int(conf["width"][p]/self.cf))
            pass
        tlu.SetWidth("L1A", int(conf["width"]["L1A"]/self.cf))
        tlu.SetVeto("L1A", int(conf["veto"]["L1A"]/self.cf))
        if conf["SC_enabled"]==True:
            tlu.SetSCenable(True)
            tlu.SetVeto("SC", int(conf["veto"]["SC"]/self.cf))
            tlu.SetWidth("SC", int(conf["width"]["SC"]/self.cf))
            pass
        else:
            tlu.SetSCenable(False)
            pass
        #tlu.SetHGTDenable(conf["HGTD_enabled"])
        # Set trigger
        tlu.SendTriggerToPlanes(conf["trigger"]["1"],conf["trigger"]["2"],conf["trigger"]["3"],conf["trigger"]["4"], False, False, False, False, False, False)
        # Enable
        tlu.SetNumberOfPlanes(conf["planes"]["1"], conf["planes"]["2"], conf["planes"]["3"], conf["planes"]["4"], False, False,False, False)
        tlu.Enable(True)
        pass
    def stop(self):
        tlu.Enable(False)
        tlu.ResetCounters()
        pass
    def TLU_GetCounter(self, s_plane):
        value=-1
        value=tlu.GetCounter(s_plane)
        return value
    def updateRates(self):

        global tlulog, conf
        #tlulog.write(str(datetime.datetime.now())+'\n')
        update_freq=100
        SC_enabled=conf["SC_enabled"]
        debugthisfunction=False #True
        unit=1#KHz
        tmp=0
        global m_avg_rates, print_counters_rate, last, m_avg_rates2, max_rate, for_maxrate
        # calculate dt
        self.dt=time.time()-self.startingTime
        #if debugthisfunction: print("Time since start [s]:\t%i" %  self.dt)
        # update rates
        #for x in [0]:#range(0,conf["numberofplanes"]+1):
        x="L1A"#str(x)
        cur_value=self.TLU_GetCounter(x)
        if cur_value== 0: cur_value=self.TLU_GetCounter(x)
        #cur_rate=1000*(cur_value - last)/1.6 #self.last_count[str(x)]) / 1.6
        #max_rate=max(cur_rate,max_rate)
        for_maxrate.append(cur_value - last)
        if len(for_maxrate) ==200:
          for_maxrate = np.array(for_maxrate)
          cur_rate=5*for_maxrate.sum()/1.5
          for_maxrate=[]
          max_rate=max(cur_rate,max_rate)
        diff=cur_value - last #self.last_count[str(x)]
        if diff < 0 : diff =0 
        m_avg_rates=cur_value#+=diff
        m_avg_rates2+=diff
        #tmp=cur_value
        tmp=m_avg_rates2
       
        last=cur_value
        #cur_value=self.TLU_GetCounter(x)
        avg_rate=float(cur_value)/float(self.dt)
        #avg_rate=float(m_avg_rates)/float(self.dt)
        if (float(self.dt)%10<0.5 and print_counters_rate == True): 
               print("[%s]\t. Cur val: %i.\t\t Previous val: %i.\t\tIncreased: %i.\t\tTotal Rate: %f.\t\tDt: %f. # triggers: %f %f" % (x, cur_value, self.last_count[str(x)], diff, avg_rate, float(self.dt), m_avg_rates, m_avg_rates2))
               tlulog.write("%s avg_rate = %f Hz, max_rate_last10s = %f Hz, max_rateallowed = %s Hz\n" % (str(datetime.datetime.now()), avg_rate, max_rate, 1/(conf["veto"]["L1A"]*0.000000001)))
               max_rate=0
               print_counters_rate=False
        if (float(self.dt)%10>0.5 and print_counters_rate==False) : print_counters_rate=True
        #m_avg_rates2=0
        """ 
        if tmp%4096 > 3900 :
          tlu.Enable(False)
          m_avg_rates2=0
          #tlu.ResetCounters()
          os.system("python /home/sbmuser/MaltaSW/MaltaDAQ/andrea_reset.py")
          tlu.Enable(True)
        pass
        """
        
    pass


if __name__=="__main__":
    # Settings
    compatible_TLUs=[2]
    cont=True
    def signal_handler(signal, frame):
        print('You pressed ctrl+C')
        print("Stop TLU run")
        global cont
        c.stop()
        cont = False
        return
        pass

    signal.signal(signal.SIGINT, signal_handler)
    # Arguments
    parser=argparse.ArgumentParser()
    parser.add_argument('--start',help="Start run", action='store_true')
    parser.add_argument('--stop',help="Stop run", action='store_true')
    parser.add_argument('-d','--duration',help="Run duration [minutes]. No stop by default",type=int,default=0)
    parser.add_argument('-r','--runNumber',help="RunNumber",type=int,default=0)
    parser.add_argument('--delay',help="Delay time to start [minutes]. No delay by default",type=int,default=0)
    parser.add_argument('-c','--conf',help="Configuration file. Default SPS_v2.txt", default="SPS_v2.py")
    parser.add_argument('-v','--verbose',help="Verbose mode",action='store_true')
    parser.add_argument('--counters',help="Debug counters",action='store_true')
    parser.add_argument('-b','--batch',help='sort by noise',action='store_true')
    args=parser.parse_args()
    
    start=args.start
    stop=args.stop
    duration=args.duration
    delay=args.delay
    verbose=args.verbose
    conf_file=args.conf
    debug_counters=args.counters
    tlulog = open ('log/tlu%06i.txt'%(args.runNumber),'a')
    if ".py" in conf_file:
        import importlib
        config = importlib.import_module(conf_file.replace(".py", ""))
        pass
    else:
        print("Error in configuration file!! Check is python file")
        cont=False
        pass


    c=cmd(config.conf)
    #print(dir(c))
    #Action!
    if cont==False:
        print("Stoped")
    if start and stop:
        print("Error: start and stop are not compatible")
        pass
    elif start:
        print("Starting TLU")
        c.start()
        while cont:
            #print("Loop")
            if debug_counters==True: c.updateRates()
            time.sleep(0.001)
            if cont==False:break
            #if (int(c.dt)/60) > duration: 
            if (m_avg_rates) > duration or (m_avg_rates ==0 and int(c.dt)/60 > 180): 
                print("Run finished.")
                print("Total triggers: %i" % m_avg_rates) #c.last_count["L1A"])
                c.stop()
                break
            pass
        pass
    elif stop:
        c.stop()
        print("Stoping TLU")
        pass
    else:
        print("No action requested!")
    print("Bye") 
    pass

