#!/usr/bin/env python
# Ignacio.Asensi@cern.ch
##############################################
# Oct 2019
# updates Feb 2020
# adding scintillator and UI updates May 2020
##############################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLU
import time
import datetime
import argparse
import ROOT
#import AtlasStyle
import array
import socket
import threading
import realtime

# SETUPs settings
current_hostname=socket.gethostname()
maphostnames={}
maphostnames["pcatlidps15"]="ep-ade-gw-02"
maphostnames["pcatlidps04"]="ep-ade-gw-01"
mapports={}
mapports["pcatlidps15"]=50000
mapports["pcatlidps04"]=50007
setupnames={}
setupnames["pcatlidps15"]="SPS"
setupnames["pcatlidps04"]="161"
ignorefw=False

# CONNECTION SETTINGS
default_hostname= maphostnames[current_hostname] if current_hostname in maphostnames else "ep-ade-gw-XX"
portname= mapports[current_hostname] if current_hostname in mapports else 50000
default_host=4
minport=50000
maxport=50021


# VARS INITIALIZATION
tlu=""
connection_str=""
scintillators={}
widths_cc={} 
widths_ns={} 
vetos_cc={}
vetos_ns={}

range_min_width={}
range_max_width={}
range_min_veto={}
range_max_veto={}


# TLU GENERAL SETTINGS
hgtd=True
HGTD_enabled=hgtd
default_refreshRate=100
refresh_plot_divider=10
debug=True
numberofplanes=5
resetCountersOnEachStart=False
compatible_TLUs={}
compatible_TLUs["pcatlidps15"]=[2,8]#Number of FW version. Check with FW
compatible_TLUs["pcatlidps04"]=[2]#Number of FW version. Check with FW
cc_factor=3.125 #at 320MHz
SC_enabled=True
SC2_enabled=True
#if hgtd: SC2_enabled=False
scintillators[0]=SC_enabled
scintillators[1]=SC2_enabled
show_console=True
show_L1Ahist=True

to_be_saved=["count", "rate"]
to_be_plotted=["rate"]# plots only one. count || rate
planes_to_be_plotted=["L1A","1"]                                                                                  
planes_to_be_saved=["L1A","1"]#["SC","SC2","1","2","3","4","L1A"]


# INPUT SIGNAL FROM PLANES. FALSE MEANS PLANE IS DISABLED or IT IS DUT. CANNOT BE ENABLED
planes={}
planes[1]=True
planes[2]=True
planes[3]=True
planes[4]=True
planes[5]=True

color={}
color["L1A"]=1
color["1"]=2
color["2"]=3
color["3"]=4
color["4"]=5
color["5"]=6
color["SC"]=7
color["SC2"]=8

# OUTPUT TRIGGER TO PLANES. FALSE MEANS TRIGGER NOT PROVIDED TO PLANE
triggerplanes={}
triggerplanes[1]=True
triggerplanes[2]=True
triggerplanes[3]=True
triggerplanes[4]=True
triggerplanes[5]=True
if hgtd:
    triggerplanes[6]=True
    triggerplanes[7]=True
    triggerplanes[8]=True
    triggerplanes[9]=True
    triggerplanes[10]=True


# Trigger counters
last_count={}
last_count["L1A"]=0
last_count["1"]=0
last_count["2"]=0
last_count["3"]=0
last_count["4"]=0
last_count["5"]=0
last_count["SC"]=0
last_count["SC2"]=0
last_count["HGTD"]=0


# GUI DEFAULT VALUES
range_min_width["L1A"]=0
range_min_width["1"]=0
range_min_width["2"]=0
range_min_width["3"]=0
range_min_width["4"]=0
range_min_width["5"]=0
range_min_width["SC"]=0
range_min_width["SC2"]=0
range_min_width["HGTD"]=-1
range_max_width["L1A"]=500
range_max_width["1"]=100299
range_max_width["2"]=100299
range_max_width["3"]=100299
range_max_width["4"]=100299
range_max_width["5"]=100299
range_max_width["SC"]=299
range_max_width["SC2"]=299
range_max_width["HGTD"]=-1
widths_ns["L1A"]=50 # Output length
widths_ns["1"]=30
widths_ns["2"]=30
widths_ns["3"]=30
widths_ns["4"]=30
widths_ns["5"]=30
widths_ns["SC"]=30
widths_ns["SC2"]=30
widths_ns["HGTD"]=-1

range_min_maxrate=0
range_max_maxrate=10000000
MaxRate_ns=50000
range_min_veto["SC"]=1
range_min_veto["SC2"]=1
range_min_veto["1"]=1
range_min_veto["2"]=1
range_min_veto["3"]=1
range_min_veto["4"]=1
range_min_veto["5"]=1
range_min_veto["HGTD"]=-1
range_max_veto["SC"]=1000
range_max_veto["SC2"]=1000
range_max_veto["HGTD"]=-1
range_max_veto["1"]=1000
range_max_veto["2"]=1000
range_max_veto["3"]=1000
range_max_veto["4"]=1000
range_max_veto["5"]=1000

vetos_ns["L1A"]=100
vetos_ns["1"]=44
vetos_ns["2"]=44
vetos_ns["3"]=44
vetos_ns["4"]=44
vetos_ns["5"]=44
vetos_ns["SC"]=44
vetos_ns["SC2"]=44
vetos_ns["HGTD"]=1



# CONVERTING DEFAULT VALUES (NS) TO CLOCK CYCLES (CC)
for w in widths_ns:
    widths_cc[w]=int(widths_ns[w]/cc_factor)
    pass

for v in vetos_ns:
    vetos_cc[v]=int(vetos_ns[v]/cc_factor)
    pass

MaxRate_cc=int(MaxRate_ns/cc_factor)


# FSM
STATUS_NAMES={}
STATUS_NAMES[1]="Not connected"
STATUS_NAMES[2]="Connected"
STATUS_NAMES[3]="Configured"
STATUS_NAMES[4]="Running"

# others
current_plot_try=0
###################################################


class GUI(QtWidgets.QMainWindow):    
    def __init__(self, plot, v, save, rate, hgtd, ifw):
        global verbose
        global numberofplanes
        global show_console
        global show_L1Ahist
        global SC_enabled
        global SC2_enabled
        global HGTD_enabled
        global cc_factor
        global counters
        global box_counter_avg_rate
        global box_counter_dt
        global box_counter_cur_rate
        global box_counter_SC
        global tlu
        global buttons
        global checkboxes
        global scintillators
        global startingTime
        global last_read
        global last_count
        global connection_str
        global compatible_TLUs
        global minport
        global maxport
        global MaxRate_ns
        global MaxRate_cc
        global rates
        global timestamps
        global selected_hostname
        global NOTCONNECTED
        global CONNECTED
        global CONFIGURED
        global RUNNING
        global resetCountersOnEachStart
        global LABEL
        global triggerplanes
        global to_be_saved
        global to_be_plotted
        global planes_to_be_plotted
        global planes_to_be_saved
        global labels_help
        global color
        global refreshRate
        global refresh_plot_divider
        global ignorefw
        
        super(GUI, self).__init__()
        verbose=v
        refreshRate=rate
        HGTD_enabled=hgtd
        ignorefw=ignorefw
        if hgtd:
            compatible_TLUs["pcatlidps15"]=[2]#Number of FW version. Check with FW
            compatible_TLUs["pcatlidps04"]=[2]#Number of FW version. Check with FW
            pass
        self.current_plot_try = 0
        #FSM states
        NOTCONNECTED=1
        CONNECTED   =2
        CONFIGURED  =3
        RUNNING     =4
        #Buttons labels
        LABEL={}
        LABEL["MaxRate"] ="Max rate (veto in ns)"#"L1A width [ms]:"
        LABEL["HELP_MaxRate"] ="Max rate (veto in ns)\n(Max:%s. Min:%s)" % (str(range_max_maxrate), str(range_min_maxrate))
        LABEL["Veto"]    ="Veto [ns]:"
        LABEL["Width"]   ="Width [ns]:"
        LABEL["Trigger"]  ="Trigger:"
        LABEL["widthL1A"]="Output length [ns]"
        LABEL["HELP_widthL1A"]="Output length [ns]\n(Max:%s. Min:%s)" % (str(range_max_width["L1A"]),str(range_min_width["L1A"]))
        LABEL["panel1.5"]="Provide trigger to:"
        LABEL["panel1.6"]="Connection:"
        #initialize vars
        self.dt=""
        self.startingTime=0
        self.numberofplanes=numberofplanes
        self.plotOnFly=plot
        self.saveToRoot=save
        self.timeoutstarted=False
        self.overwriteConditions=False
        self.c1=""
        self.g=""
        self.timeofrun=""
        self.logHeaderInitialConditions=""
        self.rfileOpened=False
        self.lastRateCheck=False
        #GUI vars
        box_counter_SC={}
        box_counter_avg_rate={}
        box_counter_dt={}
        box_counter_cur_rate={}
        buttons={}
        checkboxes={}
        labels_help={}
        #counters and triggers bufffers
        rates={}
        timestamps={}
        counters={}
        last_read=""
        #Prepare root 
        if self.plotOnFly:
            if verbose: print("Online plot is enabled")
            #AtlasStyle.SetAtlasStyle()
            self.prepareOnlinePlot()
            pass
        else:
            if verbose: print("Online plot is disabled")
            pass
        if self.saveToRoot:
            if verbose: print("Save log is enabled")
            pass
        else:
            if verbose: print("Save log is disabled")
            pass
        #Qt GUI vars
        font = QtGui.QFont("Helvetica");
        font.setPointSize(10)
        QtWidgets.QApplication.setFont(font);
        QtWidgets.QApplication.setStyle("Plastique")
        self.settings=QtCore.QSettings('TLUGUI')
        self.setGeometry(1,1,350,300)
        #self.setFixedWidth(900)
        #self.setFixedHeight(600)
        self.setWindowTitle( setupnames[current_hostname] if current_hostname in setupnames else "TLU unknown setup!")
        wid=QtWidgets.QWidget(self)
        self.setCentralWidget(wid)
        pane=QtWidgets.QGridLayout(wid)
        
        #Planes box--------------------------------------------------------------------------------------------------------
        bPlanes=QtWidgets.QGroupBox("Planes:",wid)
        pPlanes=QtWidgets.QVBoxLayout(bPlanes)
        pane.addWidget(bPlanes,0,0,4,1)
        
        buttons["SC"]=QtWidgets.QPushButton("Scintillator",wid)
        buttons["SC"].setCheckable(SC_enabled)
        buttons["SC"].setStyleSheet("background-color: green") if SC_enabled else buttons["SC"].setStyleSheet("background-color: grey") 
        buttons["SC"].toggle()
        buttons["SC"].clicked.connect(lambda state: self.setSC(state))
        pPlanes.addWidget(buttons["SC"])
        if hgtd:
            buttons["HGTD"]=QtWidgets.QPushButton("HGTD",wid)
            buttons["HGTD"].setCheckable(HGTD_enabled)
            buttons["HGTD"].setStyleSheet("background-color: green") if HGTD_enabled else buttons["HGTD"].setStyleSheet("background-color: grey") 
            buttons["HGTD"].toggle()
            buttons["HGTD"].clicked.connect(lambda state: self.setHGTD(state))
            pPlanes.addWidget(buttons["HGTD"])
        else:
            buttons["SC2"]=QtWidgets.QPushButton("Scintillator 2",wid)
            buttons["SC2"].setCheckable(SC2_enabled)
            buttons["SC2"].setStyleSheet("background-color: green") if SC2_enabled else buttons["SC2"].setStyleSheet("background-color: grey") 
            buttons["SC2"].toggle()
            buttons["SC2"].clicked.connect(lambda state: self.setSC2(state))
            pPlanes.addWidget(buttons["SC2"])
        for plane in range(1,numberofplanes+1):
            i="iPlane%i" % plane #"i" as input planes
            if verbose: print("Creating button:\t%s" % plane)
            buttons[i]=QtWidgets.QPushButton("Plane %i" % plane,wid)
            buttons[i].setCheckable(True)
            buttons[i].setStyleSheet("background-color: green") if planes[plane] else buttons[i].setStyleSheet("background-color: grey") 
            if planes[plane]: buttons[i].toggle()
            buttons[i].clicked.connect(lambda state, pn=plane : self.setPlanes(pn,state))
            pPlanes.addWidget(buttons[i])
            pass
        
        #Veto box--------------------------------------------------------------------------------------------------------
        bVeto=QtWidgets.QGroupBox(LABEL["Veto"],wid)
        pVeto=QtWidgets.QVBoxLayout(bVeto)
        pane.addWidget(bVeto,0,1,4,1)
        #SC1
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_veto["SC"],range_max_veto["SC"])
        b.setSingleStep(1)
        b.setValue(vetos_ns["SC"])
        b.valueChanged.connect(lambda state: self.SetVeto("SC", state))
        pVeto.addWidget(b)
        if verbose:
            labels_help["veto"]={}
            labels_help["veto"]["SC"]=QtWidgets.QLabel(wid)
            labels_help["veto"]["SC"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["veto"]["SC"].setText(self.dolabel(vetos_cc["SC"])+"\n(Max:%s. Min:%s)" % (str(range_max_veto["SC"]), str(range_min_veto["SC"])))
            pVeto.addWidget(labels_help["veto"]["SC"])
            pass
        if hgtd:
            #HGTD
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_veto["HGTD"],range_max_veto["HGTD"])
            b.setSingleStep(1)
            b.setValue(vetos_ns["HGTD"])
            b.valueChanged.connect(lambda state: self.SetVeto("HGTD", state))
            b.setDisabled(True)
            pVeto.addWidget(b)
            if verbose:
                labels_help["veto"]={}
                labels_help["veto"]["HGTD"]=QtWidgets.QLabel(wid)
                labels_help["veto"]["HGTD"].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["veto"]["HGTD"].setText(self.dolabel(vetos_cc["HGTD"])+"\n(Max:%s. Min:%s)" % (str(range_max_veto["HGTD"]), str(range_min_veto["HGTD"])))
                pVeto.addWidget(labels_help["veto"]["HGTD"])
                pass
            pass
        else:
            #SC2
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_veto["SC2"],range_max_veto["SC2"])
            b.setSingleStep(1)
            b.setValue(vetos_ns["SC2"])
            b.valueChanged.connect(lambda state: self.SetVeto("SC2", state))
            pVeto.addWidget(b)
            if verbose:
                labels_help["veto"]={}
                labels_help["veto"]["SC2"]=QtWidgets.QLabel(wid)
                labels_help["veto"]["SC2"].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["veto"]["SC2"].setText(self.dolabel(vetos_cc["SC2"])+"\n(Max:%s. Min:%s)" % (str(range_max_veto["SC2"]), str(range_min_veto["SC2"])))
                pVeto.addWidget(labels_help["veto"]["SC2"])
                pass
            pass
        #planes
        for p in range(1,numberofplanes+1):
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_veto[str(p)],range_max_veto[str(p)])
            b.setSingleStep(1)
            b.setValue(vetos_ns[str(p)])
            b.valueChanged.connect(lambda state, pn=p : self.SetVeto(str(pn),state))
            pVeto.addWidget(b)
            if verbose:
                labels_help["veto"][str(p)]=QtWidgets.QLabel(wid)
                labels_help["veto"][str(p)].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["veto"][str(p)].setText(self.dolabel(vetos_cc[str(p)])+"\n(Max:%s. Min:%s)" % (str(range_max_veto[str(p)]), str(range_min_veto[str(p)])))
                pVeto.addWidget(labels_help["veto"][str(p)])
                pass
            pass
        
        #Width box--------------------------------------------------------------------------------------------------------
        bWidth=QtWidgets.QGroupBox(LABEL["Width"],wid)
        pWidth=QtWidgets.QVBoxLayout(bWidth)
        pane.addWidget(bWidth,0,2,4,1)
        #SC1
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_width["SC"],range_max_width["SC"])
        b.setSingleStep(1)
        b.setValue(widths_ns["SC"]) 
        b.valueChanged.connect(lambda val : self.SetWidth("SC",val))
        pWidth.addWidget(b)
        if verbose:
            labels_help["width"]={}
            labels_help["width"]["SC"]=QtWidgets.QLabel(wid)
            labels_help["width"]["SC"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["width"]["SC"].setText(self.dolabel(widths_cc["SC"])+"\n(Max:%s. Min:%s)" % (str(range_max_width["SC"]), str(range_min_width["SC"])))
            pWidth.addWidget(labels_help["width"]["SC"])
            pass
        if hgtd:
            #HGTD
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_width["HGTD"],range_max_width["HGTD"])
            b.setSingleStep(1)
            b.setValue(widths_ns["HGTD"]) 
            b.valueChanged.connect(lambda val : self.SetWidth("HGTD",val))
            b.setDisabled(True)
            pWidth.addWidget(b)
            if verbose:
                labels_help["width"]={}
                labels_help["width"]["HGTD"]=QtWidgets.QLabel(wid)
                labels_help["width"]["HGTD"].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["width"]["HGTD"].setText(self.dolabel(widths_cc["HGTD"])+"\n(Max:%s. Min:%s)" % (str(range_max_width["HGTD"]), str(range_min_width["HGTD"])))
                pWidth.addWidget(labels_help["width"]["HGTD"])
                pass
            pass
        else:
            #SC2
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_width["SC2"],range_max_width["SC2"])
            b.setSingleStep(1)
            b.setValue(widths_ns["SC2"]) 
            b.valueChanged.connect(lambda val : self.SetWidth("SC2",val))
            pWidth.addWidget(b)
            if verbose:
                labels_help["width"]={}
                labels_help["width"]["SC2"]=QtWidgets.QLabel(wid)
                labels_help["width"]["SC2"].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["width"]["SC2"].setText(self.dolabel(widths_cc["SC2"])+"\n(Max:%s. Min:%s)" % (str(range_max_width["SC2"]), str(range_min_width["SC2"])))
                pWidth.addWidget(labels_help["width"]["SC2"])
                pass
            pass
        #planes
        for p in range(1,numberofplanes+1):
            if p== 0 : continue # 0 is widthL1A and it goes later
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_width[str(p)],range_max_width[str(p)])
            b.setSingleStep(1)
            b.setValue(widths_ns[str(p)])
            b.valueChanged.connect(lambda state, pn=p : self.SetWidth(str(pn),state))
            pWidth.addWidget(b)
            if verbose:
                labels_help["width"][str(p)]=QtWidgets.QLabel(wid)
                labels_help["width"][str(p)].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["width"][str(p)].setText(self.dolabel(widths_cc[str(p)])+"\n(Max:%s. Min:%s)" % (str(range_max_width[str(p)]), str(range_min_veto[str(p)])))
                pWidth.addWidget(labels_help["width"][str(p)])
                pass
            pass
        
        #l1A trigger box--------------------------------------------------------------------------------------------------------
        bMaxRate=QtWidgets.QGroupBox(LABEL["Trigger"],wid)
        pMaxRate=QtWidgets.QVBoxLayout(bMaxRate)
        pane.addWidget(bMaxRate,0,3,4,1)
        b=QtWidgets.QLabel(wid)
        if verbose:
            b.setText(LABEL["HELP_MaxRate"])
        else:
            b.setText(LABEL["MaxRate"])
        pMaxRate.addWidget(b)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_maxrate, range_max_maxrate)
        b.setSingleStep(1)
        b.setValue(MaxRate_ns)
        b.valueChanged.connect(lambda val : self.SetMaxRate(val))
        pMaxRate.addWidget(b)
        if verbose:
            labels_help["MaxRate"]={}
            labels_help["MaxRate"]=QtWidgets.QLabel(wid)
            labels_help["MaxRate"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["MaxRate"].setText(self.dolabel(MaxRate_cc))
            pMaxRate.addWidget(labels_help["MaxRate"])
            pass
        #L1A veto
        b=QtWidgets.QLabel(wid)
        if verbose:
            b.setText(LABEL["HELP_widthL1A"])
        else:
            b.setText(LABEL["widthL1A"])
        pMaxRate.addWidget(b)
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_width["L1A"],range_max_width["L1A"])
        b.setSingleStep(1)
        b.setValue(widths_ns["L1A"]) 
        b.valueChanged.connect(lambda val : self.SetWidth("L1A", val))
        pMaxRate.addWidget(b)
        if verbose:
            labels_help["width"]["L1A"]=QtWidgets.QLabel(wid)
            labels_help["width"]["L1A"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["width"]["L1A"].setText(self.dolabel(widths_cc["L1A"]))
            pMaxRate.addWidget(labels_help["width"]["L1A"])
            pass

                
        #Provide trigger to box--------------------------------------------------------------------------------------------------------
        bTrig=QtWidgets.QGroupBox(LABEL["panel1.5"],wid)
        pTrig=QtWidgets.QVBoxLayout(bTrig)
        pane.addWidget(bTrig,0,4,4,1)
        provide_trigger_to=numberofplanes+1
        button_tag="Plane"
        if hgtd: 
            provide_trigger_to=11
            button_tag="SMA"
            pass
        for n in range(1,provide_trigger_to):
            i="tPlane%i" % n #t as trigger plane
            buttons[i]=QtWidgets.QPushButton("%s %i" % (button_tag,n),wid)
            buttons[i].setCheckable(True)
            buttons[i].setStyleSheet("background-color: green") if triggerplanes[n] else buttons[i].setStyleSheet("background-color: grey")
            if triggerplanes[plane]: buttons[i].toggle()
            buttons[i].clicked.connect(lambda state, pn=n : self.setTriggerplanes(pn,state))
            pTrig.addWidget(buttons[i])
            pass
        
        
        #Commands box --------------------------------------------------------------------------------------------------------
        bCmds=QtWidgets.QGroupBox("Commands:",wid)
        pCmds=QtWidgets.QHBoxLayout(bCmds)
        pane.addWidget(bCmds,4,0,1,6)
        buttons["Connect"]=QtWidgets.QPushButton("Connect",wid)
        buttons["Connect"].clicked.connect(self.button_connect)
        pCmds.addWidget(buttons["Connect"])
        
        buttons["Run"]=QtWidgets.QPushButton("Run",wid)
        buttons["Run"].clicked.connect(self.button_run)
        pCmds.addWidget(buttons["Run"])
        
        checkboxes["overwriteConditions"]=QtWidgets.QCheckBox("Overwrite constrains",wid)
        checkboxes["overwriteConditions"].setChecked(self.overwriteConditions)
        checkboxes["overwriteConditions"].clicked.connect(self.checkbox_overwriteConditions)
        pCmds.addWidget(checkboxes["overwriteConditions"])
        
        self.fsm_label=QtWidgets.QLabel(wid)
        self.fsm_label.setAlignment(QtCore.Qt.AlignCenter)
        self.fsm_label.setText("Initial")
        pCmds.addWidget(self.fsm_label)
        
        #Avg Rates     box--------------------------------------------------------------------------------------------------------
        bRCounts=QtWidgets.QGroupBox("Avg Rates [Hz]",wid)
        pRCounts=QtWidgets.QVBoxLayout(bRCounts)
        pane.addWidget(bRCounts,5,0,1,1)
        #L1A rate
        b=QtWidgets.QLabel(wid)
        b.setText("L1A")
        pRCounts.addWidget(b)
        box_counter_avg_rate["L1A"]=QtWidgets.QLineEdit(wid)
        box_counter_avg_rate["L1A"].setPlaceholderText("")
        pRCounts.addWidget(box_counter_avg_rate["L1A"])
        #Scintillator rate
        b=QtWidgets.QLabel(wid)
        b.setText("Scintillator")
        pRCounts.addWidget(b)
        box_counter_avg_rate["SC"]=QtWidgets.QLineEdit(wid)
        box_counter_avg_rate["SC"].setPlaceholderText("")
        pRCounts.addWidget(box_counter_avg_rate["SC"])
        if hgtd:
            #HGTD rate
            b=QtWidgets.QLabel(wid)
            b.setText("HGTD")
            pRCounts.addWidget(b)
            box_counter_avg_rate["HGTD"]=QtWidgets.QLineEdit(wid)
            box_counter_avg_rate["HGTD"].setPlaceholderText("")
            pRCounts.addWidget(box_counter_avg_rate["HGTD"])
        else:
            #Scintillator 2 rate
            b=QtWidgets.QLabel(wid)
            b.setText("Scintillator 2")
            pRCounts.addWidget(b)
            box_counter_avg_rate["SC2"]=QtWidgets.QLineEdit(wid)
            box_counter_avg_rate["SC2"].setPlaceholderText("")
            pRCounts.addWidget(box_counter_avg_rate["SC2"])
            pass
        for x in range(1,self.numberofplanes+1):
            b=QtWidgets.QLabel(wid)
            x=str(x)
            label="Plane %s" % x
            b.setText(label)
            pRCounts.addWidget(b)
            box_counter_avg_rate[x]=QtWidgets.QLineEdit(wid)
            box_counter_avg_rate[x].setPlaceholderText("")
            pRCounts.addWidget(box_counter_avg_rate[x])
            pass
        #Current Rates     box--------------------------------------------------------------------------------------------------------
        bcurRCounts=QtWidgets.QGroupBox("Current Rates [Hz]",wid)
        pcurRCounts=QtWidgets.QVBoxLayout(bcurRCounts)
        pane.addWidget(bcurRCounts,5,1,1,1)
        #L1A current rate
        b=QtWidgets.QLabel(wid)
        b.setText("Rate L1A")
        pcurRCounts.addWidget(b)
        box_counter_cur_rate["L1A"]=QtWidgets.QLineEdit(wid)
        box_counter_cur_rate["L1A"].setPlaceholderText("")
        pcurRCounts.addWidget(box_counter_cur_rate["L1A"])
        #Scintillator 1 current rate
        b=QtWidgets.QLabel(wid)
        b.setText("Scintillator")
        pcurRCounts.addWidget(b)
        box_counter_cur_rate["SC"]=QtWidgets.QLineEdit(wid)
        box_counter_cur_rate["SC"].setPlaceholderText("")
        pcurRCounts.addWidget(box_counter_cur_rate["SC"])
        if hgtd:
            #HGTD current rate
            b=QtWidgets.QLabel(wid)
            b.setText("HGTD")
            pcurRCounts.addWidget(b)
            box_counter_cur_rate["HGTD"]=QtWidgets.QLineEdit(wid)
            box_counter_cur_rate["HGTD"].setPlaceholderText("")
            pcurRCounts.addWidget(box_counter_cur_rate["HGTD"])
            pass
        else:
            #Scintillator 2 current rate
            b=QtWidgets.QLabel(wid)
            b.setText("Scintillator 2")
            pcurRCounts.addWidget(b)
            box_counter_cur_rate["SC2"]=QtWidgets.QLineEdit(wid)
            box_counter_cur_rate["SC2"].setPlaceholderText("")
            pcurRCounts.addWidget(box_counter_cur_rate["SC2"])
            pass
        #Planes current rate
        for x in range(1,self.numberofplanes+1):
            #if planes[x]==True:
            x=str(x)
            b=QtWidgets.QLabel(wid)
            label="Plane rate %s" % x
            b.setText(label)
            pcurRCounts.addWidget(b)
            box_counter_cur_rate[x]=QtWidgets.QLineEdit(wid)
            box_counter_cur_rate[x].setPlaceholderText("")
            pcurRCounts.addWidget(box_counter_cur_rate[x])
            pass

        #Counters      box--------------------------------------------------------------------------------------------------------
        bCounts=QtWidgets.QGroupBox("Counters",wid)
        pCounts=QtWidgets.QVBoxLayout(bCounts)
        pane.addWidget(bCounts,5,2,1,1)
        #L1A
        label="Trigger"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters["L1A"]=QtWidgets.QLineEdit(wid)
        counters["L1A"].setPlaceholderText("")
        pCounts.addWidget(counters["L1A"])
        #SC1
        label="Scintillator"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters["SC"]=QtWidgets.QLineEdit(wid)
        counters["SC"].setPlaceholderText("")
        pCounts.addWidget(counters["SC"])
        if hgtd:
            #HGTD
            label="HGTD"
            b=QtWidgets.QLabel(wid)
            b.setText(label)
            pCounts.addWidget(b)
            counters["HGTD"]=QtWidgets.QLineEdit(wid)
            counters["HGTD"].setPlaceholderText("")
            pCounts.addWidget(counters["HGTD"])
            pass
        else:
            #SC2
            label="Scintillator 2"
            b=QtWidgets.QLabel(wid)
            b.setText(label)
            pCounts.addWidget(b)
            counters["SC2"]=QtWidgets.QLineEdit(wid)
            counters["SC2"].setPlaceholderText("")
            pCounts.addWidget(counters["SC2"])
            pass
        #Planes
        for x in range(1,self.numberofplanes+1):
            x=str(x)
            b=QtWidgets.QLabel(wid)
            label="Plane %s" % x
            b.setText(label)
            pCounts.addWidget(b)
            counters[x]=QtWidgets.QLineEdit(wid)
            counters[x].setPlaceholderText("")
            pCounts.addWidget(counters[x])
            pass
        
        #Run      box--------------------------------------------------------------------------------------------------------
        bdtCounts=QtWidgets.QGroupBox("Run",wid)
        pdtCounts=QtWidgets.QVBoxLayout(bdtCounts)
        pdtCounts.setAlignment(QtCore.Qt.AlignTop)
        pane.addWidget(bdtCounts,5,3,1,1)
        label="Time [s]"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pdtCounts.addWidget(b)
        box_counter_dt[0]=QtWidgets.QLineEdit(wid)
        box_counter_dt[0].setPlaceholderText("")
        pdtCounts.addWidget(box_counter_dt[0])
                
        buttons["ResetCounters"]=QtWidgets.QPushButton("Reset counters",wid)
        buttons["ResetCounters"].clicked.connect(self.TLU_reset_counters)
        pdtCounts.addWidget(buttons["ResetCounters"])
        
        if show_L1Ahist==True:
            b=QtWidgets.QLabel(wid)
            b.setText("L1A rate hist.")
            pdtCounts.addWidget(b)
            self.pLogL1A = QtWidgets.QTextEdit(wid)
            self.pLogL1A.setFontPointSize(8)
            self.pLogL1A.resize(50,50)
            pdtCounts.addWidget(self.pLogL1A)
            pass
        
        #Connection box--------------------------------------------------------------------------------------------------------
        bConn=QtWidgets.QGroupBox(LABEL["panel1.6"],wid)
        pConn=QtWidgets.QVBoxLayout(bConn)
        pane.addWidget(bConn,5,4,1,1)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(minport,maxport)
        b.setSingleStep(1)
        b.setValue(portname)
        b.valueChanged.connect(lambda p : self.TLU_ChangePort(p))
        pConn.addWidget(b)
        
        addedhosts={}
        b=QtWidgets.QComboBox(wid)
        addedhosts[default_hostname]=""
        b.addItem(default_hostname)
        selected_hostname=default_hostname
        for x in range(1,10):
            name="ep-ade-gw-0%i" % x
            if name not in addedhosts:
                b.addItem(name)
                addedhosts[name]=""
                pass
            pass
        b.setCurrentIndex(0)
        b.currentTextChanged.connect(lambda h : self.TLU_ChangeHost(h))
        pConn.addWidget(b)
        
        buttons["Close"]=QtWidgets.QPushButton("Close panel",wid)
        buttons["Close"].setStyleSheet("background-color: red")
        buttons["Close"].clicked.connect(self.button_close)
        pConn.addWidget(buttons["Close"])
        
        
        if show_console==True:
            self.pLog = QtWidgets.QTextEdit(wid)
            self.pLogL1A.setFontPointSize(8)
            self.pLog.resize(50,50)
            pConn.addWidget(self.pLog)
            pass
        
        # LAUNCH GUI -------------------------------------------------------------------------------------------------------
        self.show()
        self.timer = QtCore.QTimer()
        self.set_fsm(NOTCONNECTED)
        pass
    
    #fsm     1       not connected
    #fsm     2       connected
    #fsm     3       configured
    #fsm     4       running
    def button_connect(self):
        if debug==True: print("Status:  %s" % self.status)
        if self.checkConfiguration() == False :
            # configuration is not valid
            self.set_fsm(NOTCONNECTED)
        elif self.status==NOTCONNECTED:      # FSM IS NOT CONNECTED
            # wants to connect
            self.set_fsm(CONNECTED)
            pass
        elif self.status==CONFIGURED or self.status==CONNECTED:    # FSM IS CONNECTED
            # wants to disconnect
            self.set_fsm(NOTCONNECTED)
            pass
        else:           # FSM IS IN ILEGAL STATE
            pass
        pass
    
    def checkbox_overwriteConditions(self, state):
        self.overwriteConditions=state
        pass
    def button_run(self):
        if debug==True: print("Status: %s" % self.status)
        if self.checkConfiguration() == False :
            # configuration is not valid
            self.set_fsm(NOTCONNECTED)
        elif self.status==CONFIGURED:      # FSM IS CONFIGURED
            self.set_fsm(RUNNING)
            pass
        elif self.status==RUNNING:    # FSM IS RUNNING
            self.set_fsm(CONFIGURED)
            pass
        else:           # FSM IS IN ILEGAL STATE
            pass
        pass
    
    def set_fsm(self, next_fsm):
        self.status=next_fsm
        if next_fsm   == NOTCONNECTED:
            if self.disconnect() == NOTCONNECTED:
                self.fsm_label.setText("Not connected")
                buttons["Connect"].setText("Connect (%s)" % setupnames[current_hostname] if current_hostname in setupnames else "Unknown")
                buttons["Connect"].setStyleSheet("");
                buttons["Run"].setEnabled(False)
                pass
            else:
                print("ERROR 1")
            pass
        elif next_fsm == CONNECTED:
            if self.connect() == CONNECTED:
                self.fsm_label.setText("Connected")
                buttons["Connect"].setText("Disconnect (%s)" % setupnames[current_hostname] if current_hostname in setupnames else "Unknown")
                buttons["Connect"].setStyleSheet("background-color: green")
                buttons["Run"].setEnabled(True)
                self.set_fsm(CONFIGURED)
                pass
            else:
                self.set_fsm(NOTCONNECTED)
                print("ERROR 2: Connection to TLU failed")
            pass
        elif next_fsm == CONFIGURED:
            self.TLU_set_NOT_RUNNING()
            buttons["Run"].setText("Run")
            self.fsm_label.setText("Configured")
            buttons["Connect"].setText("Disconnect (%s)" % setupnames[current_hostname] if current_hostname in setupnames else "Unknown")
            buttons["Run"].setEnabled(True)
            pass
        elif next_fsm == RUNNING:
            if self.TLU_set_RUNNING()==RUNNING:
                self.fsm_label.setText("Running")
                buttons["Run"].setText("Stop run")
                buttons["Run"].setEnabled(True)
                pass
            else:
                print("ERROR 3: Cannot start TLU")
                self.set_fsm(CONFIGURED)
                pass
            pass
        self.updatestatusBar()
        pass
    
    def button_close(self):
        self.stop()
        pass
    
    def stop(self):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                "Are you sure you wish to quit?",
                QtWidgets.QMessageBox.Yes |
                QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            #self.timer.stop()
            if self.saveToRoot==True: self.saveFiles()
            #self.disconnect()
            tlu=None
            sys.exit()
            pass
        pass
    
    def killproc(self):
        sys.exit()
        pass
    
    def dolabel(self,cc):
        return "%i cc" % cc

    def button_reset_counters(self):
        tlu.ResetCounters()
        pass
    
    def geTime(self):
        now=datetime.datetime.now()
        return now.strftime("%Y/%m/%d %H:%M:%S")
    def getHour(self):
        now=datetime.datetime.now()
        return now.strftime("%H:%M:%S")
    
    def setPlanes(self,pn, state):
        planes[pn]=state
        if state==True: buttons["iPlane%i" % pn].setStyleSheet("background-color: green")
        else: buttons["iPlane%i" % pn].setStyleSheet("background-color: grey")
        if show_console and verbose: self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def setTriggerplanes(self,pn, state):
        #global triggerplanes FIXME is this needed
        triggerplanes[pn]=state
        if state==True: buttons["tPlane%i" % pn].setStyleSheet("background-color: green")
        else: buttons["tPlane%i" % pn].setStyleSheet("background-color: grey")
        if show_console and verbose: self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        if verbose: print("Verbose\tPlane[%s] %s" % (str(pn), str(state)))
        pass
    
    def SetVeto(self,pn, state):
        cc=int(state/cc_factor)
        vetos_cc[pn]=cc
        if show_console: self.pLog.append("%s: Set veto on plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(cc)))
        if verbose: print("Verbose:\tSet veto on plane[%s] to %s [clock cycles]" % (str(pn), str(cc)))
        if verbose: labels_help["veto"][pn].setText(self.dolabel(vetos_cc[pn]))
        pass
        
    def SetWidth(self,pn, state):
        cc=int(state/cc_factor)
        widths_cc[pn]=cc
        if show_console: self.pLog.append("%s: Set width on Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(cc)))
        if verbose: print("Verbose\tSet width on Plane[%s] to %s [clock cycles]" % (str(pn), str(cc)))
        if verbose: labels_help["width"][pn].setText(self.dolabel(widths_cc[pn]))
        pass
    
    def SetMaxRate(self, state):
        global MaxRate_cc #FIXME is this needed
        cc=int(state/cc_factor)
        MaxRate_cc=cc
        if show_console: self.pLog.append("%s: Set maxrate to %s [clock cycles]" % (self.geTime(), str(cc)))
        if verbose: print("Verbose:\tSet MaxRate to %s [clock cycles]" % (str(cc)))
        if verbose: labels_help["MaxRate"].setText(self.dolabel(MaxRate_cc))
        pass
    
    def setSC(self, state):
        if state==True: buttons["SC"].setStyleSheet("background-color: green")
        else: buttons["SC"].setStyleSheet("background-color: grey")
        if verbose: print("Scintillator 1 -> ", state)
        scintillators[0]=state
        pass
    
    def setSC2(self, state):
        if state==True: buttons["SC2"].setStyleSheet("background-color: green")
        else: buttons["SC2"].setStyleSheet("background-color: grey")
        if verbose: print("Scintillator 2 -> ", state)
        scintillators[1]=state
        pass

    def setHGTD(self, state):
        if state==True: buttons["HGTD"].setStyleSheet("background-color: green")
        else: buttons["HGTD"].setStyleSheet("background-color: grey")
        if verbose: print("HGTD  -> ", state)
        scintillators[1]=state
        pass
    
    def disconnect(self):
        #self.timer.stop()
        tlu=None
        return NOTCONNECTED
    
    def connect(self):
        # initialize tlu class
        global tlu
        tlu=PyMaltaTLU.MaltaTLU()
        tlu.SetVerbose(verbose)
        # connect to tlu
        connection_str="udp://%s:%s" % (selected_hostname,str( portname))
        tlu.Connect(connection_str)
        time.sleep(0.1)
        # check version and return connection status
        connectedTLU=tlu.GetVersion()
        if ignorefw:
            print("Connected to %s" % connection_str)
            print("Firmware version [%s]. Ignored." % str(connectedTLU)) 
        elif connectedTLU in compatible_TLUs[current_hostname]:
            if show_console: self.pLog.append("%s: Connected to %s" % (self.geTime(),connection_str))
            if show_console: self.pLog.append("%s: Firmware version %s" % (self.geTime(),str(connectedTLU)))
            print("Connected to %s" % connection_str)
            return CONNECTED
        else:
            print("TLU versionr returned: %i" % connectedTLU)
            if show_console: self.pLog.append("%s: Error: Could not connect to %s. Wrong plane? Wrong host? Or firmware not compatible with the GUI version" % (self.geTime(),connection_str))
            return NOTCONNECTED
        pass
    
    def updateRates(self ):
        debugthisfunction=False
        unit=1#KHz
        global t_counters
        # calculate dt
        self.dt=time.time()-self.startingTime
        if verbose: print("updateRates() Dt[s]:\t%s" %  str(self.dt))
        # update rates
        if self.status==RUNNING or self.lastRateCheck==True:
            for x in range(0,numberofplanes+1):
                time.sleep(0.001)
                x=str(x)
                if x=="0": x="L1A"
                cur_value=self.TLU_GetCounter(x)
                if cur_value==0 and x!="5" and x!="2": print("0 value counter in %s at %s" % (x,str(self.dt)))
                diff=cur_value - last_count[str(x)]
                cur_rate=diff*10
                avg_rate=float(cur_value)/float(self.dt)
                if debugthisfunction: print("[%s]\t. Cur val: %i.\t\t Previous val: %i.\t\tTotal Rate: %f.\t\tDt: %f." % (x, cur_value, last_count[str(x)], avg_rate, float(self.dt)))
                if avg_rate<0: print("NEGATIVE!!! AVG. [%s]\t. Cur val: %i.\t\t Previous val: %i.\t\tTotal Rate: %f.\t\tDt: %f." % (x, cur_value, last_count[str(x)], avg_rate, float(self.dt)))
                if cur_value<last_count[str(x)]: 
                    print("------>Regresion in counter. Plane [%s]\t. Cur val: %i.\t\t Previous val: %i.\t\tRate(total): %f.\t\tDt: %f.2\tTimestamp: %s" % (x, cur_value, last_count[str(x)], avg_rate, self.dt, str(time.time())))
                #if x=="1": print(cur_value)
                rates[x]=avg_rate
                box_counter_dt[0].setPlaceholderText("%i" % (self.dt))
                box_counter_avg_rate[x].setPlaceholderText("%s" % ("{:,.2f}".format(avg_rate)))
                box_counter_cur_rate[x].setPlaceholderText("%s" % ("{:,.2f}".format(cur_rate)))
                counters[x].setPlaceholderText("%s" % ("{:,}".format(cur_value)))
                last_count[x]=cur_value
                #Write log
                if show_L1Ahist and x=="L1A": self.pLogL1A.append("%s: %s" % (self.getHour(),("{:,}".format(cur_rate))))
                pass
            #Same for the SCS
            #SC_enabled=False
            if SC_enabled==True:
                cur_value=self.TLU_GetCounter("SC")
                diff=cur_value - last_count["SC"]
                cur_rate=diff*10
                avg_rate=float(cur_value)/float(self.dt)
                if debugthisfunction: print("last_count\t[SC]:\t%i" % last_count["SC"])
                rates["SC"]=avg_rate
                box_counter_avg_rate["SC"].setPlaceholderText("%.2f" % (avg_rate/unit))
                box_counter_cur_rate["SC"].setPlaceholderText("%.2f" % (cur_rate/unit))
                counters["SC"].setPlaceholderText("%i" % (cur_value))
                last_count["SC"]=cur_value
                pass
            if SC2_enabled==True and not hgtd:
                cur_value=self.TLU_GetCounter("SC2")
                diff=cur_value - last_count["SC2"]
                cur_rate=diff*10
                avg_rate=float(cur_value)/float(self.dt)
                if debugthisfunction: print("last_count\t[SC2]:\t%i" % last_count["SC2"])
                rates["SC2"]=avg_rate
                box_counter_avg_rate["SC2"].setPlaceholderText("%.2f" % (avg_rate/unit))
                box_counter_cur_rate["SC2"].setPlaceholderText("%.2f" % (cur_rate/unit))
                counters["SC2"].setPlaceholderText("%i" % (cur_value))
                last_count["SC2"]=cur_value
                pass
            if hgtd:
                cur_value=self.TLU_GetCounter("HGTD")
                diff=cur_value - last_count["HGTD"]
                cur_rate=diff*10
                avg_rate=float(cur_value)/float(self.dt)
                if debugthisfunction: print("last_count\t[HGTD]:\t%i" % last_count["HGTD"])
                rates["HGTD"]=avg_rate
                box_counter_avg_rate["HGTD"].setPlaceholderText("%.2f" % (avg_rate/unit))
                box_counter_cur_rate["HGTD"].setPlaceholderText("%.2f" % (cur_rate/unit))
                counters["HGTD"].setPlaceholderText("%i" % (cur_value))
                last_count["HGTD"]=cur_value
                pass
            #if self.saveToRoot or self.plotOnFly: self.updatePlot()
            pass
        pass
    
    def updatestatusBar(self):
        self.statusBar().showMessage('%s' % STATUS_NAMES[self.status])
        self.fsm_label.setText(STATUS_NAMES[self.status])
        pass
    
    def save(self):
        name = QtWidgets.QFileDialog.getSaveFileName(None)
        if not name[0]: return
        cwd = os.getcwd()
        f = open(name, "w")
        if show_console: text = self.pLog.toPlainText()
        f.write(text)
        f.close()
        pass
    
    def closeEvent(self,event):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            #self.stop()
            event.accept()
            sys.exit()
        else:
            event.ignore()
            pass
        pass
    
    def TLU_ChangePort(self, p):
        global portname
        portname=int(p)
        if show_console: self.pLog.append("%s: New port [%s]" % (self.geTime(),str(portname)))
        pass
    
    def TLU_ChangeHost(self, h):
        global selected_hostname
        selected_hostname=h
        if show_console: self.pLog.append("%s: New host [%s]" % (self.geTime(),str(selected_hostname)))
        pass
    def checkConfiguration(self):
        if self.overwriteConditions==True: 
            if verbose: print("Configuration constrains overwritten")
            return True
        conf=True
        if MaxRate_cc < widths_cc["L1A"]:
            conf=False
            self.pLog.append("ERROR!!\nMax rate can not be lower than Output length!!")
            pass
        for p in planes:
            if widths_ns[str(p)]>vetos_ns[str(p)]:
                conf=False
                self.pLog.append("ERROR!!\nWidth can't be higher than veto. Check plane: %s" % p)
                pass
            pass
        if scintillators[0]:
            if widths_ns["SC"]>vetos_ns["SC"]:
                conf=False
                self.pLog.append("ERROR!!\nWidth can't be higher than veto. Check scintillator!!")
                pass
            pass
        if scintillators[1]:
            if widths_ns["SC2"]>vetos_ns["SC2"]:
                conf=False
                self.pLog.append("ERROR!!\nWidth can't be higher than veto. Check scintillator2!!")
                pass
            pass
        print("Configuration is %s" % ("OK" if conf else "NOT OK"))
        return conf
    def TLU_GetCounter(self, s_plane):
        value=-1
        value=tlu.GetCounter(s_plane)
        return value
    
    def TLU_reset_counters(self):
        # disable TLU
        tlu.Enable(False)
        time.sleep(0.2)
        # reset counters
        tlu.ResetCounters()
        # reset starting time for time measurements
        self.startingTime = time.time()
        if verbose:
            print("Reseting counters at ", self.startingTime)
            pass
        time.sleep(0.01)
        for c in last_count:
            print
            last_count[c]=0
            pass
        #tlu.Enable(True)FIXME should this be enabled? Not when reseting counters and TLU is disabled!
        pass
    
    def TLU_set_RUNNING(self):
        if self.saveToRoot: self.prepareSaveLog()
        #OBdebug=True
        #Reset counters
        self.TLU_reset_counters()
        #Set input planes
        tlu.SetNumberOfPlanes(False,False , False,False , False , False, False, False)
        tlu.SetNumberOfPlanes(planes[1], planes[2], planes[3], planes[4], planes[5], False, False, False)
        if debug==True:tlu.GetNumberOfPlanes()
        # Set scintillator
        if SC_enabled==True:
            tlu.SetSCenable(scintillators[0])
            tlu.SetVeto("SC",vetos_cc["SC"])
            tlu.SetWidth("SC",widths_cc["SC"])
            pass
        if SC2_enabled==True:
            tlu.SetSC2enable(scintillators[1])
            tlu.SetVeto("SC2",vetos_cc["SC2"])
            tlu.SetWidth("SC2",widths_cc["SC2"])
            pass
        # Set vetos
        tlu.SetVeto("L1A",MaxRate_cc)
        print("Setting maxrate at %scc " % MaxRate_cc)
        if debug==True:tlu.GetVeto("L1A")
        for p in range(1,numberofplanes+1):
            s=str(p)
            tlu.SetVeto(s, vetos_cc[s])
            if debug==True:tlu.GetVeto(s)
            pass
        # Set widths
        tlu.SetWidth("L1A",widths_cc["L1A"])
        if debug==True:tlu.GetWidth("L1A")
        for p in range(1,numberofplanes+1):
            s=str(p)
            tlu.SetWidth(s, widths_cc[s])
            if debug==True:tlu.GetWidth(s)
            pass
        # Set trigger
        if hgtd:
            tlu.SendTriggerToPlanes(triggerplanes[1], triggerplanes[2], triggerplanes[3], triggerplanes[4], triggerplanes[5], triggerplanes[6], triggerplanes[7], triggerplanes[8], triggerplanes[9], triggerplanes[10])
        else:
            tlu.SendTriggerToPlanes(triggerplanes[1], triggerplanes[2], triggerplanes[3], triggerplanes[4], triggerplanes[5], False, False, False, False, False)
        if debug==True:tlu.GetSendTriggerToPlanes()
        # Enable
        tlu.Enable(True)
        # Start GUI loop
        self.timer.start(refreshRate)
        if self.timeoutstarted==False:
            self.timer.timeout.connect(self.updateRates)
            if self.plotOnFly: self.timer.timeout.connect(self.updateOnlinePlot)
            if self.saveToRoot: self.timer.timeout.connect(self.updateSaveLog)
            self.timeoutstarted=True
            pass
        self.logHeaderInitialConditions ="Number of planes: %s,%s,%s,%s,%s\n" % (str(planes[1]),str(planes[2]),str(planes[3]),str(planes[4]),str(planes[5]))
        self.logHeaderInitialConditions+="SC_enabled: %s\n" % (str(SC_enabled))
        self.logHeaderInitialConditions+="SC2_enabled: %s\n" % (str(SC2_enabled))
        self.logHeaderInitialConditions+="HGTD_enabled: %s\n" % (str(HGTD_enabled))
        self.logHeaderInitialConditions+="Planes veto [ns]: %s,%s,%s,%s,%s\n" % (str(vetos_ns["1"]),str(vetos_ns["2"]),str(vetos_ns["3"]),str(vetos_ns["4"]),str(vetos_ns["5"]))
        self.logHeaderInitialConditions+="Planes width [ns]: %s,%s,%s,%s,%s\n" % (str(widths_ns["1"]),str(widths_ns["2"]),str(widths_ns["3"]),str(widths_ns["4"]),str(widths_ns["5"]))
        self.logHeaderInitialConditions+="Max Rate [ns]: %s\n" % (str(MaxRate_ns))
        self.logHeaderInitialConditions+="Output length [ns]: %s\n" % (str(vetos_ns["L1A"]))
        self.logHeaderInitialConditions+="\n"
        
        # Return state
        return RUNNING
        
    def TLU_set_NOT_RUNNING(self):
        #self.timer.stop()
        self.lastRateCheck=True
        tlu.Enable(False)
        print("Last check counters")
        self.updateRates()
        if self.saveToRoot: self.saveFiles()
        self.lastRateCheck=False
        #if show_console: self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
    
    def prepareOnlinePlot(self):
        self.c1 = ROOT.TCanvas("c1","TLU monitoring", 800,600)
        self.c1.Divide(1,2)
        self.g1 = realtime.Plot("L",";Time [s]; Rate [u]")
        self.g2 = realtime.Plot("S",";Time [s]; Rate [u]")
        self.g1.AddPlot("PL","L1A")
        self.g2.AddPlot("PL","1")
        pass
    
    def updateOnlinePlot(self):
        if self.status==RUNNING:
            if self.current_plot_try < refresh_plot_divider:
                self.current_plot_try += 1
                if verbose: print("Plot skip %i" % self.current_plot_try)
                return
                pass
            else:
                self.current_plot_try=0
                pass
            self.g1.AddPoint(0,self.dt,rates["L1A"])
            self.g2.AddPoint(0,self.dt,rates["1"])
            self.c1.cd(1)
            self.g1.Draw("A")
            self.c1.cd(2)
            self.g2.Draw("A")
            self.c1.Update()
            #ROOT.gSystem.ProcessEvents()
            pass 
        pass
    def prepareSaveLog(self):
        if self.saveToRoot==True:
            self.rfileOpened=True
            if verbose:print("Preparing ROOT file")
            # Prepare root file
            now=datetime.datetime.now()
            now_st=now.strftime("%Y%m%d_%H_%M_%S")
            self.timeofrun=now_st
            self.rfn="TLU_output_%s.root" % self.timeofrun
            self.rf = ROOT.TFile(self.rfn,"RECREATE")
            self.ntuple          = ROOT.TTree("tlu","")
            self.n_timestamp     = array.array('L',(0,)) 
            self.ntuple.Branch("timestamp", self.n_timestamp, "timestamp/l")
            if "rate"  in to_be_saved: self.n_rate={} 
            if "count" in to_be_saved: self.n_count={}
            for x in planes_to_be_saved:
                if "count" in to_be_saved: 
                    self.n_count[x] = array.array('f',(0,))
                    self.ntuple.Branch("count%s" % x, self.n_count[x], "count%s/f" % x)
                    pass
                if "rate" in to_be_saved: 
                    self.n_rate[x]  = array.array('f',(0,))
                    self.ntuple.Branch("rate%s" % x, self.n_rate[x], "rate%s/i" % x)
                    pass
                pass
            pass
        pass

    def updateSaveLog(self):
        debugthisfunction=False
        if self.saveToRoot and self.status==RUNNING:
            if debugthisfunction: print("updateSaveLog[s]:\t%s" %  str(time.time()))
            for x in planes_to_be_saved:
                self.n_timestamp[0]=int(time.time())
                if debugthisfunction: print("Saving plane [%s]:\tCount\t[%s]" % (x,last_count[x]))
                if debugthisfunction: print("Saving plane [%s]:\tRates\t[%s]" % (x,rates[x]))
                if "count" in to_be_saved: self.n_count[x][0]=float(last_count[x])
                if "rate" in to_be_saved:  self.n_rate[x][0]=int(rates[x])
                self.n_timestamp[0]=int(time.time())
                pass
            self.ntuple.Fill()
            pass
        pass
    def saveFiles(self):
        debugthisfunction=True
        if self.rfileOpened==False:return
        #Save root data
        self.ntuple.Write()
        self.rf.Close()
        #Save TLU log
        #name = QtWidgets.QFileDialog.getSaveFileName( None, "Save File", ".", ".txt;;*")
        #print (name)
        #if not name[0]: return
        #cwd = os.getcwd()
        #name_st=name[0]+name[1]
        tlu_log_filename="TLU_log_%s.txt" % self.timeofrun
        f = open(tlu_log_filename, "w")
        text = self.logHeaderInitialConditions + self.pLog.toPlainText()
        f.write(text)
        f.close()
        if debugthisfunction: print("Saving ROOT file: %s" % self.rfn)
        if debugthisfunction: print("Saving txt  file: %s" % tlu_log_filename)
        self.rfileOpened=False
        pass
                
if __name__=="__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("-p"    ,"--plot",help="Enable plotting",action='store_true')
    parser.add_argument("-i"    ,"--ignorefw",help="Ignore firmware version",action='store_true')
    parser.add_argument("-s"    ,"--save",help="Save to root file",action='store_true')
    parser.add_argument("-v"    ,"--verbose",help="Enable verbose",action='store_true')
    parser.add_argument("--hgtd",help="Enable HGTD mode",action='store_true')
    parser.add_argument("-a"    ,"--address",help="ipbus address")
    parser.add_argument("-r"    ,"--refreshRate",help="GUI refresh rate (Default 100ms)",type=int, default= default_refreshRate)
    args=parser.parse_args()
    plotOnFly=args.plot
    saveToRoot=args.save
    verbose=args.verbose
    refreshRate=args.refreshRate
    hgtd=args.hgtd
    ifw=args.ignorefw
    print("Starting TLU GUI v4. Refresh rate %.2f Hz" % (1000/refreshRate))
    if hgtd: print("Runing with HGTD")
    updateRate=1
    app = QtWidgets.QApplication(sys.argv)
    
    gui = GUI(plotOnFly, verbose, saveToRoot, refreshRate, hgtd, ifw)
    sys.exit(app.exec_())
