####################################################
#
# THIS IS PYTHON CODE. USE PYTHON SYNTAX
# ignacio.asensi@cern.ch
#
####################################################
conf={}
conf["connect_string"]="udp://ep-ade-gw-01:50007"
conf["numberofplanes"]=5
conf["planes"]={}
conf["trigger"]={}
conf["veto"]={}
conf["width"]={}
conf["SC_enabled"]=True # True if there is one scintillator
conf["HGTD_enabled"]=False # True if there is one scintillator


# Listen from planes. True/False
conf["planes"]["1"]=True
conf["planes"]["2"]=False
conf["planes"]["3"]=True
conf["planes"]["4"]=True
conf["planes"]["HGTD"]=False
conf["planes"]["SC"]=True #scintillator

# Provide trigger to planes. True/False
conf["trigger"]["1"]=True
conf["trigger"]["2"]=True
conf["trigger"]["3"]=True
conf["trigger"]["4"]=True


# Veto in ns (will be converted n/3.125 to clock cycles)
conf["veto"]["L1A"]=1000 #40000 #L1A
conf["veto"]["1"]=44
conf["veto"]["2"]=44
conf["veto"]["3"]=44
conf["veto"]["4"]=44
conf["veto"]["SC"]=1

# Signal width  in ns (will be converted n/3.125 to clock cycles)
conf["width"]["L1A"]=120
conf["width"]["1"]=30
conf["width"]["2"]=30
conf["width"]["3"]=30
conf["width"]["4"]=30
conf["width"]["SC"]=2
