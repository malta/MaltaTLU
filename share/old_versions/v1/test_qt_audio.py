#!/usr/bin/env python

import os
import sys
import argparse
from PyQt5 import QtWidgets,QtCore,QtGui,QtMultimedia

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',required=True)
args=parser.parse_args()

playlist = QtMultimedia.QMediaPlaylist()
url = QtCore.QUrl.fromLocalFile(args.file)
playlist.addMedia(QtMultimedia.QMediaContent(url))
#playlist.setPlaybackMode(QMediaPlaylist.Loop)

player = QtMultimedia.QMediaPlayer()
player.setVolume(100)
player.setPlaylist(playlist)
player.play()

print("Playing file: %s" % args.file)

raw_input()

print("Have a nice day")
