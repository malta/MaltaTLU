#!/usr/bin/env python
#########################################
# Ignacio.Asensi@cern.ch
# Oct 2019
#########################################

import os
import sys
import signal
import subprocess 
import array
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLU
import time
import datetime
import argparse
import ROOT
import AtlasStyle
#from __main__ import port


hostname="ep-ade-gw-05"
portname=50005
L1AWidth=100
default_host=4

defaultNumberOfPlanes=4
defaultTriggerWidth=3
defaultL1ATriggerWidth=100#ms

compatible_TLUs=[1]
tlu=""

t_counters={}
t_counters[0]=0
t_counters[1]=0
t_counters[2]=0
t_counters[3]=0
t_counters[4]=0
t_counters[5]=0



linputs={}
linputs[1]=False
linputs[2]=False
linputs[3]=False
linputs[4]=False
linputs[5]=False


planes={}
planes[1]=True
planes[2]=True
planes[3]=True
planes[4]=True
planes[5]=False


widths={}
widths[1]=3
widths[2]=3
widths[3]=3
widths[4]=3
widths[5]=3

triggerplanes={}
triggerplanes[1]=True
triggerplanes[2]=True
triggerplanes[3]=True
triggerplanes[4]=True
triggerplanes[5]=True
triggerplanes[6]=True

last_count={}
last_count[0]=0
last_count[1]=0
last_count[2]=0
last_count[3]=0
last_count[4]=0
last_count[5]=0

connection_str=""
    
class TLU(QtWidgets.QMainWindow):    
    def __init__(self, numberofplanes, disableROOT, run):
        self.disableROOT=disableROOT
        self.run=run
        global verbose
        verbose=False
        super(TLU, self).__init__()
        global counters
        global rcounters
        global tlu
        global buttons
        global startingTime
        global last_read
        global last_count
        global connection_str
        global compatible_TLUs
        
        
        connection_str="udp://%s:%s" % (hostname,str( portname))
        startingTime=0
        tlu=PyMaltaTLU.MaltaTLU()
        counters={}
        rcounters={}
        buttons={}
        last_read=""
        self.numberofplanes=numberofplanes
        self.dt=""
        #timing
        
        font = QtGui.QFont("Helvetica");
        QtWidgets.QApplication.setFont(font);
        QtWidgets.QApplication.setStyle("Plastique")
        self.settings=QtCore.QSettings('TLUGUI')
        self.setGeometry(50,50,850,800)
        self.setWindowTitle("TLU GUI")
        wid=QtWidgets.QWidget(self)
        self.setCentralWidget(wid)
        self.status="Disconnected"
        self.log=""
        self.plane=self.settings.value('plane')
        self.mods=[]
        self.tlu=""
        self.pending=False
        global L1AWidth
        
        #Planes box--------------------------------------------------------------------------------------------------------
        pane=QtWidgets.QGridLayout(wid)
        bPlanes=QtWidgets.QGroupBox("Planes:",wid)
        pPlanes=QtWidgets.QVBoxLayout(bPlanes)
        pane.addWidget(bPlanes,0,0,4,1)
        
        for plane in planes:
            b=QtWidgets.QPushButton("Plane %i" % plane,wid)
            b.setCheckable(planes[plane])
            b.toggle()
            b.clicked.connect(lambda state, pn=plane : self.setPlanes(pn,state))
            pPlanes.addWidget(b)
            pass
        
        #Width box--------------------------------------------------------------------------------------------------------
        bWidth=QtWidgets.QGroupBox("Width:",wid)
        pWidth=QtWidgets.QVBoxLayout(bWidth)
        pane.addWidget(bWidth,0,1,4,1)
        
        for w in widths:
            b=QtWidgets.QSpinBox(wid)
            b.setRange(0,5)
            b.setSingleStep(1)
            b.setValue(widths[w])
            b.valueChanged.connect(lambda state, pn=w : self.SetTriggerWidth(pn,state))
            pWidth.addWidget(b)
            pass
        
        
        #Long input box--------------------------------------------------------------------------------------------------------
        bLinput=QtWidgets.QGroupBox("Long input:",wid)
        pLinput=QtWidgets.QVBoxLayout(bLinput)
        pane.addWidget(bLinput,0,2,4,1)
        
        for linput in linputs:
            b=QtWidgets.QCheckBox(wid)
            b.setChecked(linputs[linput])
            b.clicked.connect(lambda state, pn=linput : self.SetLongInput(pn,state))
            pLinput.addWidget(b)
            pass
        
        #l1A trigger box--------------------------------------------------------------------------------------------------------
        bTWidth=QtWidgets.QGroupBox("Max rate [ms]:",wid)#Khz
        pTWidth=QtWidgets.QVBoxLayout(bTWidth)
        pane.addWidget(bTWidth,0,3,4,1)
        #global twidths
        b=QtWidgets.QSpinBox(wid)
        b.setRange(0,200)
        b.setSingleStep(1)
        b.setValue(L1AWidth)
        b.valueChanged.connect(lambda val : self.SetL1AWidth(val))
        pTWidth.addWidget(b)
        
        
        #Provide trigger to box--------------------------------------------------------------------------------------------------------
        bTrig=QtWidgets.QGroupBox("Provide trigger to:",wid)
        pTrig=QtWidgets.QVBoxLayout(bTrig)
        pane.addWidget(bTrig,0,4,4,1)
        
        for n in triggerplanes:
            b=QtWidgets.QPushButton("Plane %i" % n,wid)
            b.setCheckable(triggerplanes[n])
            b.setChecked(triggerplanes[n])
            b.clicked.connect(lambda state, pn=n : self.setTriggerplanes(pn,state))
            pTrig.addWidget(b)
            pass
        
        
        
        #Connection box--------------------------------------------------------------------------------------------------------
        bConn=QtWidgets.QGroupBox("Connection:",wid)
        pConn=QtWidgets.QVBoxLayout(bConn)
        pane.addWidget(bConn,0,5,3,1)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(50000,50016)
        b.setSingleStep(1)
        b.setValue(portname)
        b.valueChanged.connect(lambda p : self.TLU_ChangePort(p))
        pConn.addWidget(b)
        
        b=QtWidgets.QComboBox(wid)
        for x in xrange(1,10):
            b.addItem("ep-ade-gw-0%i" % x)
            pass
        b.setCurrentIndex(default_host)
        b.currentTextChanged.connect(lambda h : self.TLU_ChangeHost(h))
        pConn.addWidget(b)
        
        
        self.cs=""
        self.g={}
        self.mg =""
        
        AtlasStyle.SetAtlasStyle()
        if disableROOT==False: 
            self.preparePlot()
            b=QtWidgets.QLabel(wid)
            b.setText("Plot")
            pConn.addWidget(b)
            b=QtWidgets.QPushButton("Update",wid)
            b.clicked.connect(self.updatePlotDraw)
            pConn.addWidget(b)
            #
            b=QtWidgets.QSpinBox(wid)
            b.setRange(0,99999)
            b.setSingleStep(1)
            b.setValue(self.run)
            b.valueChanged.connect(lambda val : self.setRunNumber(val))
            pConn.addWidget(b)
            self.bSavePlot=QtWidgets.QPushButton("Save",wid)
            self.bSavePlot.clicked.connect(self.savePlot)
            pConn.addWidget(self.bSavePlot)
            pass
        #Commands box --------------------------------------------------------------------------------------------------------
        bCmds=QtWidgets.QGroupBox("Commands:",wid)
        pCmds=QtWidgets.QHBoxLayout(bCmds)
        pane.addWidget(bCmds,4,0,1,6)
        buttons["Connect"]=QtWidgets.QPushButton("Connect",wid)
        buttons["Connect"].clicked.connect(self.connect)
        pCmds.addWidget(buttons["Connect"])
        
        buttons["Enable"]=QtWidgets.QPushButton("Enable",wid)
        buttons["Enable"].clicked.connect(self.TLU_enable)
        pCmds.addWidget(buttons["Enable"])
        
        buttons["Disable"]=QtWidgets.QPushButton("Disable",wid)
        buttons["Disable"].clicked.connect(self.TLU_disable)
        pCmds.addWidget(buttons["Disable"])
        
        buttons["Close"]=QtWidgets.QPushButton("Close",wid)
        buttons["Close"].clicked.connect(self.stop)
        pCmds.addWidget(buttons["Close"])
        
        buttons["ResetCounters"]=QtWidgets.QPushButton("Reset counters",wid)
        buttons["ResetCounters"].clicked.connect(self.TLU_reset_counters)
        pCmds.addWidget(buttons["ResetCounters"])
        
        self.disableButtons()
        
        #Debug box --------------------------------------------------------------------------------------------------------
        self.pLog = QtWidgets.QTextEdit(wid)
        self.pLog.resize(500,500)
        pane.addWidget(self.pLog,5,0,1,4)
        
        #Counters
        #Rate box--------------------------------------------------------------------------------------------------------
        bRCounts=QtWidgets.QGroupBox("Rates:",wid)
        pRCounts=QtWidgets.QVBoxLayout(bRCounts)
        pane.addWidget(bRCounts,5,4,1,1)
        label="Rate L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pRCounts.addWidget(b)
        rcounters[0]=QtWidgets.QLineEdit(wid)
        rcounters[0].setPlaceholderText("")
        pRCounts.addWidget(rcounters[0])
        for x in xrange(1,self.numberofplanes+1):
            #if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane rate %i" % x
            b.setText(label)
            pRCounts.addWidget(b)
            rcounters[x]=QtWidgets.QLineEdit(wid)
            rcounters[x].setPlaceholderText("")
            pRCounts.addWidget(rcounters[x])
             #   pass
            pass
        
        #Counters box--------------------------------------------------------------------------------------------------------
        bCounts=QtWidgets.QGroupBox("Counters:",wid)
        pCounts=QtWidgets.QVBoxLayout(bCounts)
        pane.addWidget(bCounts,5,5,1,5)
        label="Counter L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters[0]=QtWidgets.QLineEdit(wid)
        counters[0].setPlaceholderText("")
        pCounts.addWidget(counters[0])
        for x in xrange(1,self.numberofplanes+1):
           # if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane %i" % x
            b.setText(label)
            pCounts.addWidget(b)
            counters[x]=QtWidgets.QLineEdit(wid)
            counters[x].setPlaceholderText("")
            pCounts.addWidget(counters[x])
          #  pass
            pass
        self.show()
        pass
    
    def geTime(self):
        now=datetime.datetime.now()
        return now.strftime("%Y/%m/%d %H:%M:%S")
    
    def setPlanes(self,pn, state):
        #print ("Set active plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        planes[pn]=state
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def setTriggerplanes(self,pn, state):
        #print ("Set active plane: %s as %s" % (str(pn), str(state)))
        global triggerplanes
        self.pending=True
        triggerplanes[pn]=state
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def SetTriggerWidth(self,pn, state):
        #print ("SetTriggerWidth plane: %s to %s" % (str(pn), str(state)))
        self.pending=True
        self.pLog.append("%s: Trigger width of Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(state)))
        widths[pn]=state
        pass
    
    def SetLongInput(self,pn, state):
        #print ("Set long input plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        linputs[pn]=state
        self.pLog.append("%s: Set long input on Plane[%s] to %s" % (self.geTime(),str(pn), str(state)))
        #self.settings.setValue('scan',v)
        pass
    def SetL1AWidth(self, val):
        global L1AWidth
        self.pending=True
        L1AWidth=val
        self.pLog.append("%s: Set L1AWidth to %s [ms]" % (self.geTime(),str(val)))
        pass
    def TLU_reset_counters(self):
        global startingTime
        tlu.Enable(False)
        time.sleep(0.2)
        for x in last_count:
            last_count[x]=0
            pass
        tlu.ResetCounters()
        startingTime = time.time()
        time.sleep(0.2)
        tlu.Enable(True)
        pass
    def connect(self):
        connection_str="udp://%s:%s" % (hostname,str( portname))
        if self.status == "Connected":
            self.pLog.append("Already connected. Disconnecting....")
            self.TLU_disable()
            self.status == "Disconnected"
            self.disableButtons()
            pass
        else:
            self.TLU_connect(connection_str)
            time.sleep(0.2)
            connectedTLU=tlu.GetVersion()
            tlu.SetVerbose(verbose)
            if connectedTLU in compatible_TLUs:
                self.status="Connected"
                self.pLog.append("%s: Connected to %s" % (self.geTime(),connection_str))
                print ("Connected to %s" % connection_str)
                self.enableButtons()
                pass
            else:
                self.status="Disconnected"
                self.pLog.append("%s: Error: Could not connect to %s. Wrong plane? Wrong host?" % (self.geTime(),connection_str))
                pass
            pass
        self.updateStatusBar()
        timer = QtCore.QTimer()
        timer.timeout.connect(self.updateCounter)
        timer.start(1000)  # every 10,000 milliseconds
        pass
    
    def stop(self):
        self.TLU_disable()
        self.killproc()
        pass
    def updateCounter(self):
        #print planes
        global t_counters
        if self.status=="Enabled":
            for x in rcounters:
                #if x>4: continue
                #if planes[x]==True:
                value=self.TLU_GetCounter(x)
                last_count[x]=value
                counters[x].setPlaceholderText(str(value))
                #self.n_timestamp[0]=int(time.time())
                if disableROOT==False: self.n_plane[x][0]=float(last_count[x])
               # pass
                pass
            pass
        pass
    def updateRCounter(self):
        #print planes
        global t_counters
        if self.status=="Enabled":
            #Get value
            countedtriggers=self.TLU_GetCounter(0)
            #Get rate
            self.dt=time.time()-startingTime
            for x in rcounters:
                #if x>4: continue
                #if planes[x]==True:
                countedtriggers=self.TLU_GetCounter(x)
                diff=countedtriggers - last_count[x]
                rate=diff/updateRate
                #print "diff=value - last_count[0]" 
                #print "%i=%i- %i" % ( diff, value, last_count[x])
                totalrate=float(countedtriggers)/float(self.dt)
                rcounters[x].setPlaceholderText("%i [Tot %i] Hz" % (rate, totalrate))
                #pass
                pass
            if disableROOT==False: self.updatePlot()
            pass
        pass
    def getRate(self, value):
        pass
    
    def updateStatusBar(self):
        self.statusBar().showMessage('%s' % self.status)
        pass
    def save(self):
        name = QtWidgets.QFileDialog.getSaveFileName(None)
        if not name[0]: return
        cwd = os.getcwd()
        f = open(name, "w")
        text = self.pLog.toPlainText()
        f.write(text)
        f.close()
        pass
    
    def closeEvent(self,event):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            event.accept()
            sys.exit()
        else:
            event.ignore()
            pass
        pass

    def execute(self,cmd):
        popen = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True,
                                 universal_newlines=True,
                                 preexec_fn=os.setsid)
        self.pid=popen.pid
        for stdout_line in iter(popen.stdout.readline, ""):
            yield stdout_line 
            pass
        popen.stdout.close()
        return_code = popen.wait()
        self.pid=0
        pass
    
    def killproc(self):
        #if self.pid==0: return
        #print("Kill process %i" % self.pid)
        sys.exit()
        pass
    
    #TLU
    
    def TLU_ChangePort(self, p):
        global portname
        portname=int(p)
        self.pLog.append("%s: New port [%s]" % (self.geTime(),str(portname)))
        pass
    
    def TLU_ChangeHost(self, h):
        global hostname
        hostname=h
        self.pLog.append("%s: New host [%s]" % (self.geTime(),str(hostname)))
        pass
    
    def TLU_GetCounter(self, plane):
        value=-1
        value=tlu.GetCounterTrigger(plane)
        return value
    
    def TLU_connect(self, str):
        tlu.Connect(str)
        print ("Connecting to %s" % str)
        pass
    
    def TLU_enable(self):
        global startingTime
        try:
            time.sleep(0.1)
            #Reset counters
            tlu.ResetCounters()
            #input planes
            tlu.SetPlanes(planes[1], planes[2], planes[3], planes[4], planes[5], False, False, False)
            #widths  
            for w in widths:
                tlu.SetTriggerWidth(w, widths[w])
            #long inputs
            for linput in linputs:
                state=0
                if linputs[linput]==True: state=1
                tlu.SetLongInput(linput, state)
                pass
            #trigger width
            tlu.SetL1AWidth(L1AWidth)
            #outputs
            tlu.SetCoincidenceOutputPlanes(triggerplanes[1], triggerplanes[2], triggerplanes[3], triggerplanes[4], triggerplanes[5], triggerplanes[6], False, False)
            #All ok
            tlu.Enable(True)
            print "startingTime", startingTime
            if startingTime==0:
                startingTime=time.time()
                pass
            self.status="Enabled"
            self.pending=False
            buttons["Disable"].setEnabled(True)
            buttons["Enable"].setEnabled(False)
            pass
        except:
            self.status="Disabled"
            tlu.Enable(False)
            pass
        self.updateStatusBar()
        self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
    
    def TLU_disable(self):
        buttons["Disable"].setEnabled(False)
        buttons["Enable"].setEnabled(True)
        tlu.Enable(False)
        self.status="Disabled"
        self.updateStatusBar()
        self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
    
    
    #GUI functions
    def enableButtons(self):
        buttons["Enable"].setEnabled(True)
        buttons["Disable"].setEnabled(True)
        pass
    
    def disableButtons(self):
        buttons["Enable"].setEnabled(False)
        buttons["Disable"].setEnabled(False)
        pass
    def preparePlot(self):
        print "preparePlot"
        self.fw = ROOT.TFile("TLU_%i_.root" % self.run,"RECREATE")
        self.cs = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
        self.mg = ROOT.TMultiGraph()
        title="Triggers;T[s];N"
        self.mg.SetTitle(title)
        self.lg=ROOT.TLegend(0.2, 0.6, 0.3, 0.9)
        self.lg.SetLineColor(1)
        self.lg.SetLineWidth(1)
        self.lg.SetBorderSize(1)
        self.lg.SetTextSize(0.02)
        self.lg.SetHeader("Planes")
        
        self.ntuple          = ROOT.TTree("data","")
        self.n_timestamp     = array.array('L',(0,))
        self.n_plane={}
        self.ntuple.Branch("timestamp", self.n_timestamp, "timestamp/i")
        
        for x in xrange(0,self.numberofplanes+1):
            self.n_plane[x]= array.array('f',(0,))
            self.ntuple.Branch("plane%i" % x,   self.n_plane[x],   "plane/F")
            self.g[x]=ROOT.TGraph()
            pointSize=1.13
            self.g[x].SetMarkerColor(x+1)
            self.g[x].SetMarkerStyle(20)
            self.g[x].SetMarkerSize(pointSize)
            #self.g[x].SetPoint(self.g[x].GetN(), x, x)
            self.lg.AddEntry(self.g[x],str(x) , "p")
            self.mg.Add(self.g[x])
            pass
        #self.lg.Draw()
        self.cs.SetLogy()
        #self.mg.Draw("AP")
        pass
    def updatePlot(self):
        for x in xrange(0,self.numberofplanes+1):
            self.g[x].SetPoint(self.g[x].GetN(), float(self.dt), float(last_count[x]))
            self.mg.Add(self.g[x])
            pass
        self.ntuple.Fill()
        pass
    
    def updatePlotDraw(self):
        if True:
            self.mg.Draw("AP")
            self.lg.Draw()
            self.cs.Update()
            self.cs.Draw()
            pass
        pass
    
    def savePlot(self):
        print "savePlot"
        #self.cs.SaveAs("TLU_%i.root" % self.run)
        self.mg.Write("plot")
        self.ntuple.Write()
        self.fw.Close()
        self.bSavePlot.setEnabled(False)
        pass
    def setRunNumber(self, val):
        self.run=val
        pass
    pass

if __name__=="__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("-nop","--numberofplanes",help="Number of planes",type=int,default=4)
    parser.add_argument("-NR","--NoROOT",help="Disable root",action='store_true')
    parser.add_argument("-r","--runnumber",help="Run number",type=int,default=1)
    args=parser.parse_args()
    numberofplanes=args.numberofplanes
    run=args.runnumber
    disableROOT=args.NoROOT
    print("Have a happy Testbeam")
    updateRate=1
    app = QtWidgets.QApplication(sys.argv)
    
    gui = TLU(numberofplanes, disableROOT, run)
    timer = QtCore.QTimer()
    timer.timeout.connect(gui.updateRCounter)
    timer.timeout.connect(gui.updateCounter)
    timer.start(updateRate*1000)
    sys.exit(app.exec_())

