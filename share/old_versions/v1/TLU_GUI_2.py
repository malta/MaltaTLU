#!/usr/bin/env python
#########################################
# Ignacio.Asensi@cern.ch
# Oct 2019
#########################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLU
import time
import datetime
import argparse
import ROOT

defaults={'host':"192.168.200.20",
          'port':50001,
          'numInputs':5,
          'numOutputs':6,
          'trigInput':True,
          'trigOutput':True,
          'l1aWidth':100,
          'longInput':False,
          'trigWidth':3,
          'coincWidth':100}

settings={
    'minport':50000,
    'maxport':50021, 
    'routerhostname':"ep-ade-gw-0"
    
    
    }
class TLU(QtWidgets.QMainWindow):    
    def __init__(self, enableROOT):
        super(TLU, self).__init__()
        global defaults
        global settings
        
        font = QtGui.QFont("Helvetica");
        QtWidgets.QApplication.setFont(font);
        QtWidgets.QApplication.setStyle("Plastique")
        self.settings=QtCore.QSettings('TLUGUI')
        self.setGeometry(50,50,850,800)
        self.setWindowTitle("TLU GUI")
        wid=QtWidgets.QWidget(self)
        self.setCentralWidget(wid)
        
        self.plotting=enableROOT
        self.tlu=PyMaltaTLU.MaltaTLU("")
        self.counters={}
        self.rcounters={}
        self.pcounters={}
        self.ptime={}
        self.buttons={}
        self.log=""
        self.host=self.settings.value('host',defaults['host'])
        self.port=self.settings.value('port',defaults['port'])
        self.numInputs=defaults['numInputs']#self.settings.value('numInputs',defaults['numInputs'],type=int)
        self.numOutputs=defaults['numInputs']#self.settings.value('numOutputs',defaults['numOutputs'],type=int)
        self.l1aWidth=self.settings.value('l1aWidth',defaults['l1aWidth'],type=int)
        self.trigInputs={}
        self.trigWidths={}
        self.longInputs={}
        self.trigOutputs={}
        for plane in xrange(1,self.numInputs+1):
            self.trigInputs[plane]=self.settings.value('trigInput_%i'%plane,defaults['trigInput'],type=bool)
            self.trigWidths[plane]=self.settings.value('trigWidth_%i'%plane,defaults['trigWidth'],type=int)
            self.longInputs[plane]=self.settings.value('longInput_%i'%plane,defaults['longInput'],type=bool)
            pass
        for plane in xrange(1,self.numOutputs+1):
            self.trigOutputs[plane]=self.settings.value('trigOutput_%i'%plane,defaults['trigOutput'],type=bool)
            pass
        #timing
        if self.plotting:
            self.cs=""
            self.g={}
            self.mg =""
            import ROOT
            import AtlasStyle
            AtlasStyle.SetAtlasStyle()
            self.showPlot()
            pass
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateCounters)        
        
        self.status="Disconnected"
        self.updateStatusBar()

        pane=QtWidgets.QGridLayout(wid)
        pane.setColumnStretch(0,2)
                
        #Planes box--------------------------------------------------------------------------------------------------------
        bPlanes=QtWidgets.QGroupBox("Planes:",wid)
        pPlanes=QtWidgets.QVBoxLayout(bPlanes)
        pane.addWidget(bPlanes,0,0,4,1)

        for i in self.trigInputs:
            b=QtWidgets.QPushButton("Plane %i" % i,wid)
            b.setCheckable(True)
            b.setChecked(self.trigInputs[i])
            b.clicked.connect(lambda state, pn=i : self.setInput(pn,state))
            pPlanes.addWidget(b)
            pass
        
        #Width box--------------------------------------------------------------------------------------------------------
        bWidth=QtWidgets.QGroupBox("Width:",wid)
        pWidth=QtWidgets.QVBoxLayout(bWidth)
        pane.addWidget(bWidth,0,1,4,1)
        
        for w in self.trigWidths:
            b=QtWidgets.QSpinBox(wid)
            b.setRange(0,5)
            b.setSingleStep(1)
            b.setValue(self.trigWidths[w])
            b.valueChanged.connect(lambda state, pn=w : self.SetTriggerWidth(pn,state))
            pWidth.addWidget(b)
            pass
        
        #Long input box--------------------------------------------------------------------------------------------------------
        bLinput=QtWidgets.QGroupBox("Long input:",wid)
        pLinput=QtWidgets.QVBoxLayout(bLinput)
        pane.addWidget(bLinput,0,2,4,1)
        
        for i in self.longInputs:
            b=QtWidgets.QCheckBox(wid)
            b.setChecked(self.longInputs[i])
            b.clicked.connect(lambda state, pn=i : self.SetLongInput(pn,state))
            pLinput.addWidget(b)
            pass
        
        #l1A trigger box--------------------------------------------------------------------------------------------------------
        bTWidth=QtWidgets.QGroupBox("L1A width [ms]:",wid)#Khz
        pTWidth=QtWidgets.QVBoxLayout(bTWidth)
        pane.addWidget(bTWidth,0,3,4,1)

        b=QtWidgets.QSpinBox(wid)
        b.setRange(0,200)
        b.setSingleStep(1)
        b.setValue(self.l1aWidth)
        b.valueChanged.connect(lambda val : self.SetL1AWidth(val))
        pTWidth.addWidget(b)
        
        #Provide trigger to box--------------------------------------------------------------------------------------------------------
        bTrig=QtWidgets.QGroupBox("Provide trigger to:",wid)
        pTrig=QtWidgets.QVBoxLayout(bTrig)
        pane.addWidget(bTrig,0,4,4,1)
        
        for n in self.trigOutputs:
            b=QtWidgets.QPushButton("Plane %i" % n,wid)
            b.setCheckable(True)
            b.setChecked(self.trigOutputs[n])
            b.clicked.connect(lambda state, pn=n : self.setOutput(pn,state))
            pTrig.addWidget(b)
            pass
        
        #Connection box--------------------------------------------------------------------------------------------------------
        bConn=QtWidgets.QGroupBox("Connection:",wid)
        pConn=QtWidgets.QVBoxLayout(bConn)
        pane.addWidget(bConn,0,5,3,1)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(self.settings['minport'],self.settings['maxport'])
        b.setSingleStep(1)
        b.setValue(int(self.port))
        b.valueChanged.connect(lambda p : self.TLU_ChangePort(p))
        pConn.addWidget(b)
        
        b=QtWidgets.QComboBox(wid)
        b.addItem("192.168.200.20")
        for x in xrange(1,10):
            b.addItem("%s%i" % (settings['routerhostname'],x))
            pass
        b.setCurrentIndex(0)
        b.currentTextChanged.connect(lambda h : self.TLU_ChangeHost(h))
        pConn.addWidget(b)
        
        
        
        #Commands box --------------------------------------------------------------------------------------------------------
        bCmds=QtWidgets.QGroupBox("Commands:",wid)
        pCmds=QtWidgets.QHBoxLayout(bCmds)
        pane.addWidget(bCmds,4,0,1,6)

        self.buttons["Connect"]=QtWidgets.QPushButton("Connect",wid)
        self.buttons["Connect"].clicked.connect(self.connect)
        pCmds.addWidget(self.buttons["Connect"])
        
        self.buttons["Enable"]=QtWidgets.QPushButton("Enable",wid)
        self.buttons["Enable"].clicked.connect(self.enable)
        pCmds.addWidget(self.buttons["Enable"])        
        
        self.buttons["Disable"]=QtWidgets.QPushButton("Disable",wid)
        self.buttons["Disable"].clicked.connect(self.disable)
        pCmds.addWidget(self.buttons["Disable"])
        
        self.buttons["Disconnect"]=QtWidgets.QPushButton("Disconnect",wid)
        self.buttons["Disconnect"].clicked.connect(self.disconnect)
        pCmds.addWidget(self.buttons["Disconnect"])
        
        self.buttons["ResetCounters"]=QtWidgets.QPushButton("Reset counters",wid)
        self.buttons["ResetCounters"].clicked.connect(self.reset)
        pCmds.addWidget(self.buttons["ResetCounters"])
        
        self.buttons["Connect"].setEnabled(True)
        self.buttons["Enable"].setEnabled(False)
        self.buttons["Disable"].setEnabled(False)
        self.buttons["Disconnect"].setEnabled(False)

        
        #Debug box --------------------------------------------------------------------------------------------------------
        self.pLog = QtWidgets.QTextEdit(wid)
        #self.pLog.setMinimumSize(500,500)
        pane.addWidget(self.pLog,5,0,1,4)
        
        #Counters
        #Rate box--------------------------------------------------------------------------------------------------------
        bRCounts=QtWidgets.QGroupBox("Rates:",wid)            
        pRCounts=QtWidgets.QVBoxLayout(bRCounts)
        pane.addWidget(bRCounts,5,4,1,1)
        b=QtWidgets.QLabel(wid)
        b.setText("Rate L1A [Hz]")
        pRCounts.addWidget(b)
        self.rcounters[0]=QtWidgets.QLineEdit(wid)
        self.rcounters[0].setPlaceholderText("")
        pRCounts.addWidget(self.rcounters[0])
        for x in xrange(1,self.numInputs+1):
            b=QtWidgets.QLabel(wid)
            b.setText("Rate plane %i [Hz]" % x)
            pRCounts.addWidget(b)
            self.rcounters[x]=QtWidgets.QLineEdit(wid)
            self.rcounters[x].setPlaceholderText("")
            pRCounts.addWidget(self.rcounters[x])
            pass
        
        #Counters box--------------------------------------------------------------------------------------------------------
        bCounts=QtWidgets.QGroupBox("Counters:",wid)
        pCounts=QtWidgets.QVBoxLayout(bCounts)
        pane.addWidget(bCounts,5,5,1,1)
        b=QtWidgets.QLabel(wid)
        b.setText("Counter L1A")
        pCounts.addWidget(b)
        self.counters[0]=QtWidgets.QLineEdit(wid)
        self.counters[0].setPlaceholderText("")
        pCounts.addWidget(self.counters[0])
        for x in xrange(1,self.numInputs+1):
            b=QtWidgets.QLabel(wid)
            b.setText("Plane %i" % x)
            pCounts.addWidget(b)
            self.counters[x]=QtWidgets.QLineEdit(wid)
            self.counters[x].setPlaceholderText("")
            pCounts.addWidget(self.counters[x])
            pass
        self.show()
        
        pass
    
    def geTime(self):
        now=datetime.datetime.now()
        return now.strftime("%Y/%m/%d %H:%M:%S")
    
    def setInput(self, pn, state):
        self.trigInputs[pn]=state
        self.settings.setValue('trigInput_%i'%pn,state)
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def setOutput(self, pn, state):
        self.trigOutputs[pn]=state
        self.settings.setValue('trigOutput_%i'%pn,state)
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def SetTriggerWidth(self,pn, state):
        self.trigWidths[pn]=state
        self.settings.setValue('trigWidth_%i'%pn,state)
        self.pLog.append("%s: Trigger width of Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(state)))
        pass
    
    def SetLongInput(self,pn, state):
        self.longInputs[pn]=state
        self.pLog.append("%s: Set long input on Plane[%s] to %s" % (self.geTime(),str(pn), str(state)))
        pass

    def SetL1AWidth(self, val):
        self.l1aWidth=val
        self.pLog.append("%s: Set L1AWidth to %s [ms]" % (self.geTime(),str(val)))
        pass

    def connect(self):
        if self.status=="Connected": return
        connection_str="udp://%s:%s" % (self.host,self.port)
        self.tlu.Connect(connection_str)
        self.timer.start(1000)
        self.buttons["Connect"].setEnabled(False)
        self.buttons["Enable"].setEnabled(True)
        self.buttons["Disable"].setEnabled(False)
        self.buttons["Disconnect"].setEnabled(True)
        self.pLog.append("%s: Connected to %s" % (self.geTime(),connection_str))
        self.status="Connected"
        self.updateStatusBar()
        pass

    def disconnect(self):
        if self.status=="Disconnected": return
        self.pLog.append("Disconnecting...")
        self.timer.stop()
        self.buttons["Connect"].setEnabled(True)
        self.buttons["Enable"].setEnabled(False)
        self.buttons["Disable"].setEnabled(False)
        self.buttons["Disconnect"].setEnabled(False)
        self.pLog.append("%s: Disconnect" % (self.geTime()))
        self.status="Disconnected"
        self.updateStatusBar()
        pass
        
    def enable(self):
        if self.status=="Enabled": return
        self.tlu.ResetCounters()
        self.tlu.SetPlanes(self.trigInputs[1], self.trigInputs[2], self.trigInputs[3], self.trigInputs[4], self.trigInputs[5], False, False, False)
        for w in self.trigWidths:
            self.tlu.SetTriggerWidth(w, self.trigWidths[w])
            pass
        for i in self.longInputs:
            val=1 if self.longInputs[i]==True else 0
            self.tlu.SetLongInput(i,val)
            pass
        self.tlu.SetL1AWidth(self.l1aWidth)
        self.tlu.SetCoincidenceOutputPlanes(self.trigOutputs[1],self.trigOutputs[2],self.trigOutputs[3], self.trigOutputs[4], self.trigOutputs[5], False, False, False)
        self.tlu.Enable(True)
        self.buttons["Connect"].setEnabled(False)
        self.buttons["Enable"].setEnabled(False)
        self.buttons["Disable"].setEnabled(True)
        self.buttons["Disconnect"].setEnabled(True)
        self.pLog.append("%s: Enabled" % (self.geTime()))
        self.status="Enabled"
        self.updateStatusBar()
        pass
    
    def disable(self):
        if self.status=="Disabled": return
        self.tlu.Enable(False)
        self.buttons["Connect"].setEnabled(False)
        self.buttons["Enable"].setEnabled(True)
        self.buttons["Disable"].setEnabled(False)
        self.buttons["Disconnect"].setEnabled(True)
        self.pLog.append("%s: %s" % (self.geTime(),self.status))
        self.status="Disabled"
        self.updateStatusBar()
        pass

    def reset(self):
        if self.status=="Disabled" or self.status=="Disconnected": return
        self.tlu.Enable(False)
        time.sleep(0.2)
        self.tlu.ResetCounters()
        time.sleep(0.2)
        self.tlu.Enable(True)
        self.pLog.append("%s: Counters reset" % self.geTime())        
        pass

    def updateCounters(self):
        if self.status!="Enabled": return
        for p in xrange(self.numInputs+1):
            if not p in self.pcounters: self.pcounters[p]=0
            if not p in self.ptimes: self.ptimes[p]=time.time()
            pass
        for p in xrange(self.numInputs+1):
            newtime=time.time()
            newcount=self.tlu.GetCounterTrigger(p)
            dt=newtime-self.ptimes[p]
            dn=newcount-self.pcounters[p]
            rate=float(dn)/float(dt)
            self.counters[p].setPlaceholderText(str(newcount))
            self.rcounters[p].setPlaceholderText("%.2f Hz" % (rate))
            self.pcounters[p]=newcount
            self.ptimes[p]=newtime
            pass
        if self.plotting: self.updatePlot()
        pass
        
    def updateStatusBar(self):
        self.statusBar().showMessage('%s' % self.status)
        pass     
    
    def closeEvent(self,event):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            event.accept()
            sys.exit()
        else:
            event.ignore()
            pass
        pass
         
    def TLU_ChangePort(self, p):
        self.port=int(p)
        self.pLog.append("%s: New port [%s]" % (self.geTime(),str(self.port)))
        pass
    
    def TLU_ChangeHost(self, h):
        self.host=h
        self.pLog.append("%s: New host [%s]" % (self.geTime(),str(self.host)))
        pass
    
    def showPlot(self):
        if enableROOT==False:
            print "ROOT NOT ENABLED"
            pass
        else:
            self.cs = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
            self.mg = ROOT.TMultiGraph()
            title="Triggers; N;T[s]"
            self.mg.SetTitle(title)
            for x in xrange(1,self.numberofplanes+1):
                self.g[x]=ROOT.TGraph()
                pointSize=1.13
                self.g[x].SetMarkerColor(x)
                self.g[x].SetMarkerStyle(20)
                self.g[x].SetMarkerSize(pointSize)
                #self.g[x].SetPoint(self.g[x].GetN(), x, x)
                self.mg.Add(self.g[x])
                pass
            pass
        self.mg.Draw("AP")
        pass

    def updatePlot(self):
        print "dt 2 :", self.dt
        for x in xrange(1,self.numberofplanes+1):
            self.g[x].SetPoint(self.g[x].GetN(), float(self.dt), float(last_count[x]))
            self.mg.Add(self.g[x])
            pass
        self.cs.Update()
        self.cs.Draw()
        pass
    
    pass

if __name__=="__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("-p","--plot",help="Enable plotting",action='store_true')
    parser.add_argument("-a","--address",help="ipbus address")
    args=parser.parse_args()

    print("Starting TLU GUI")
    app = QtWidgets.QApplication(sys.argv)
    
    gui = TLU(args.plot)
    sys.exit(app.exec_())

