import time
import PyMaltaTLU


tlu=PyMaltaTLU.MaltaTLU()
#print tlu.GetVersion()
print "Connecting"
tlu.Connect("udp://ep-ade-gw-05:50005")
print "TLU version:"
tlu.GetVersion()


def testL1A():
	print "------------------------------------------"
	print "Testing L1A"
	testvalueforL1A=5000
	tlu.SetL1AWidth(testvalueforL1A)
	if testvalueforL1A ==tlu.GetL1AWidth(): 
		print "-----> SetL1AWidth and GetL1AWidth.......OK"
	else:
		print "-----> SetL1AWidth and GetL1AWidth.......NOT OK!!!!!!!!!!"
		pass
	pass
print "------------------------------------------"

def testTriggerWith():
	print "Testing trigger width"
	testvalueforSetTriggerWidth=3
	plane=1
	tlu.SetTriggerWidth(plane, testvalueforSetTriggerWidth)
	if testvalueforSetTriggerWidth ==tlu.GetTriggerWidth(plane): 
		print "-----> SetTriggerWidth and GetTriggerWidth........OK"
	else:
		print "-----> SetTriggerWidth and GetTriggerWidth........NOT OK!!!!"
		pass
	pass

def testLongInput():
	print "------------------------------------------"
	print "Testing Getting Long input"
	testval=False
	testplane=1
	tlu.SetLongInput(testplane,testval)
	print "----"
	print tlu.GetLongInput(testplane)
	pass





def testReset():
	print "Getting input Planes"
	activeplanes=tlu.GetPlanes()
	print "{0:b}".format(activeplanes)
	print "Change planes"
	tlu.SetPlanes(False, False, False, False, False, False, False, False )
	tlu.SetPlanes(True, True, True, False, False, False, False, False )
	print "Reset"
	tlu.Reset()
	print "Getting input Planes"
	activeplanes=tlu.GetPlanes()
	print "{0:b}".format(activeplanes)
	pass



def testEnable():
	print "Testing enable"
	tlu.SetPlanes(True, False, False, False, False, False, False, False )
	tlu.Enable(False)
	print "Output lights should be ON"
	raw_input()
	tlu.Enable(True)
	print "Output lights should be OFF"
	pass


def testTelescope():
	tlu.SetL1AWidth(100)
	tlu.SetPlanes(True, False, True, False, False, False, False, False )
	tlu.SetTriggerWidth(1, 3)
	tlu.SetTriggerWidth(2, 3)
	tlu.SetTriggerWidth(2, 3)
	tlu.SetLongInput(0,0)
	tlu.SetLongInput(1,0)
	tlu.SetLongInput(2,0)
	tlu.Enable(True)
	tlu.SetCoincidenceOutputPlanes(True, True, True, True, False, False, False, False )
	print "Done"



#tlu.Enable(False)

#testLongInput()
#testTriggerWith()
#testReset()
#testEnable()

testTelescope()

	
'''
if testval == tlu.GetLongInput(testplane):
	print "-----> SetLongInput and GetLongInput........OK"
else:
	print tlu.GetLongInput(testplane)
	print "-----> SetLongInput and GetLongInput.........NOT OK!!!!"
print "------------------------------------------"
'''




'''
print "Setting input Planes"
#tlu.SetPlanes(False, False, False, False, False, False, False, False )
tlu.SetPlanes(True, True, True, False, False, False, False, False )
#tlu.SetPlanes(True, True, False, False, False, False, False, False )
#tlu.SetPlanes(True, True, True, True, True, False, False, False )
print "Getting input Planes"
activeplanes=tlu.GetPlanes()
print "{0:b}".format(activeplanes)




print "Setting CoincidenceOutput Planes"
#tlu.SetPlanes(False, False, False, False, False, False, False, False )
#tlu.SetPlanes(True, False, False, False, False, False, False, False )
#tlu.SetPlanes(True, True, False, False, False, False, False, False )
tlu.SetCoincidenceOutputPlanes(True, True, True, True, False, False, False, False )
print "Getting CoincidenceOutput Planes"
CoincidenceOutputplanes=tlu.GetCoincidenceOutputPlanes()
print "{0:b}".format(CoincidenceOutputplanes)
'''
'''

import Herakles
ipb=Herakles.Uhal("udp://ep-ade-gw-05:50005")
print ipb.Read(0)
'''