#!/usr/bin/env python
#########################################
# Ignacio.Asensi@cern.ch
# Oct 2019
# updates Feb 2020
#########################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLU
import time
import datetime
import argparse
import ROOT
import AtlasStyle
import array

default_hostname="ep-ade-gw-02"
portname=50020
MaxRate=100
default_host=4
minport=50000
maxport=50021
debug=True
defaultNumberOfPlanes=4
defaultTriggerWidth=3
defaultL1ATriggerWidth=100#ms

compatible_TLUs=[3,4,7]
tlu=""
cc_factor=3.125 #at 320MHz

STATUS_NAMES={}
STATUS_NAMES[1]="Not connected"
STATUS_NAMES[2]="Connected"
STATUS_NAMES[3]="Configured"
STATUS_NAMES[4]="Running"




t_counters={}
t_counters[0]=0
t_counters[1]=0
t_counters[2]=0
t_counters[3]=0
t_counters[4]=0
t_counters[5]=0
t_counters[6]=0


min_width=0
max_width=10
widths={}
widths[0]=2
widths[1]=4
widths[2]=4
widths[3]=4
widths[4]=4
widths[5]=4
widths[6]=4

# FALSE MEANS PLANE IS DISABLED AND CAN NOT BE ENABLED
planes={}
planes[1]=True
planes[2]=False
planes[3]=True
planes[4]=True
planes[5]=False
planes[6]=False

min_veto=1
max_veto=1000
vetos={}
vetos[1]=100
vetos[2]=100
vetos[3]=100
vetos[4]=100
vetos[5]=100
vetos[6]=100

triggerplanes={}
triggerplanes[1]=True
triggerplanes[2]=True
triggerplanes[3]=True
triggerplanes[4]=True
triggerplanes[5]=False
triggerplanes[6]=False

last_count={}
last_count[0]=0
last_count[1]=0
last_count[2]=0
last_count[3]=0
last_count[4]=0
last_count[5]=0
last_count[6]=0

connection_str=""


class GUI(QtWidgets.QMainWindow):    
    def __init__(self, numberofplanes, enableROOT):
        global verbose
        verbose=True
        super(GUI, self).__init__()
        global cc_factor
        global counters
        global box_counter_avg_rate
        global box_counter_dt
        global box_counter_cur_rate
        global tlu
        global buttons
        global startingTime
        global last_read
        global last_count
        global connection_str
        global compatible_TLUs
        global minport
        global maxport
        global MaxRate
        global rates
        global fsm
        global selected_hostname
        global NOTCONNECTED
        global CONNECTED
        global CONFIGURED
        global RUNNING
        NOTCONNECTED=1
        CONNECTED   =2
        CONFIGURED  =3
        RUNNING     =4
        fsm=1
        global LABEL
        LABEL={}
        LABEL["MaxRate"]={}
        LABEL["Veto"]={}
        LABEL["Width"]={}
        LABEL["Others"]={}
        LABEL["longoutput"]={}
        LABEL["panel1.5"]={}
        LABEL["panel1.6"]={}
        LABEL["MaxRate"][0]="Max rate (KHz)"#"L1A width [ms]:"
        LABEL["Veto"][0]="Veto [ns]:"
        LABEL["Width"][0]="Width [ns]:"
        LABEL["Others"][0]="Others:"
        LABEL["longoutput"][0]="Long output [ns]"
        LABEL["panel1.5"][0]="Provide trigger to:"
        LABEL["panel1.6"][0]="Connection:"
        #connection_str="udp://%s:%s" % (hostname,str( portname))
        #connection_str="%s" % (hostname)
        self.startingTime=0
        rates={}
        counters={}
        box_counter_avg_rate={}
        box_counter_dt={}
        box_counter_cur_rate={}
        buttons={}
        last_read=""
        self.numberofplanes=numberofplanes
        self.dt=""
        #timing
        if enableROOT:
            self.cs=""
            self.g={}
            self.mg =""
            AtlasStyle.SetAtlasStyle()
            self.prepareROOT()
            pass
        else:
            print ("Root not enabled")
        
        
        #self.timer = QtCore.QTimer()
        #self.timer.timeout.connect(self.updateRates)
        
        font = QtGui.QFont("Helvetica");
        QtWidgets.QApplication.setFont(font);
        QtWidgets.QApplication.setStyle("Plastique")
        self.settings=QtCore.QSettings('TLUGUI')
        self.setGeometry(50,50,850,800)
        self.setWindowTitle("TLU GUI")
        wid=QtWidgets.QWidget(self)
        self.setCentralWidget(wid)
        self.log=""
        self.plane=self.settings.value('plane')
        self.mods=[]
        self.tlu=""
        self.pending=False
        
        
        #Planes box--------------------------------------------------------------------------------------------------------
        pane=QtWidgets.QGridLayout(wid)
        bPlanes=QtWidgets.QGroupBox("Planes:",wid)
        pPlanes=QtWidgets.QVBoxLayout(bPlanes)
        pane.addWidget(bPlanes,0,0,4,1)
        
        #for plane in planes:
        for plane in range(1,numberofplanes+1):
            i="iPlane%i" % plane
            buttons[i]=QtWidgets.QPushButton("Plane %i" % plane,wid)
            buttons[i].setCheckable(True)
            buttons[i].setStyleSheet("background-color: green") if planes[plane] else buttons[i].setStyleSheet("background-color: grey") 
            if planes[plane]: buttons[i].toggle()
            buttons[i].clicked.connect(lambda state, pn=plane : self.setPlanes(pn,state))
            pPlanes.addWidget(buttons[i])
            #self.setPlanes(plane, planes[plane])
            pass
        
        #Width box--------------------------------------------------------------------------------------------------------
        bWidth=QtWidgets.QGroupBox(LABEL["Veto"][0],wid)
        pWidth=QtWidgets.QVBoxLayout(bWidth)
        pane.addWidget(bWidth,0,1,4,1)
        
        for w in range(1,numberofplanes+1):
            b=QtWidgets.QSpinBox(wid)
            b.setRange(min_veto,max_veto)
            b.setSingleStep(1)
            b.setValue(vetos[w])
            b.valueChanged.connect(lambda state, pn=w : self.SetVeto(pn,state))
            pWidth.addWidget(b)
            pass
        
        
        #Long input box--------------------------------------------------------------------------------------------------------
        bLinput=QtWidgets.QGroupBox(LABEL["Width"][0],wid)
        pLinput=QtWidgets.QVBoxLayout(bLinput)
        pane.addWidget(bLinput,0,2,4,1)
        

        for p in range(1,numberofplanes+1):
            if p== 0 : continue # 0 is longoutput
            b=QtWidgets.QSpinBox(wid)
            b.setRange(min_width,max_width)
            b.setSingleStep(1)
            b.setValue(widths[p])
            b.valueChanged.connect(lambda state, pn=p : self.SetLongInput(pn,state))
            pLinput.addWidget(b)
            pass
        

        
        
        #l1A trigger box--------------------------------------------------------------------------------------------------------
        bTWidth=QtWidgets.QGroupBox(LABEL["Others"][0],wid)#Khz
        pTWidth=QtWidgets.QVBoxLayout(bTWidth)
        pane.addWidget(bTWidth,0,3,4,1)
        
        b=QtWidgets.QLabel(wid)
        b.setText(LABEL["MaxRate"][0])
        pTWidth.addWidget(b)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(0,1000)
        b.setSingleStep(1)
        b.setValue(MaxRate)
        b.valueChanged.connect(lambda val : self.SetMaxRate(val))
        pTWidth.addWidget(b)
        
        b=QtWidgets.QLabel(wid)
        b.setText(LABEL["longoutput"][0])
        pTWidth.addWidget(b)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(0,10)
        b.setSingleStep(1)
        b.setValue(widths[0]) 
        b.valueChanged.connect(lambda val : self.SetLongOutput(val))
        pTWidth.addWidget(b)
        
        
        #Provide trigger to box--------------------------------------------------------------------------------------------------------
        bTrig=QtWidgets.QGroupBox(LABEL["panel1.5"][0],wid)
        pTrig=QtWidgets.QVBoxLayout(bTrig)
        pane.addWidget(bTrig,0,4,4,1)
        
        for n in range(1,numberofplanes+1):
            i="tPlane%i" % n
            buttons[i]=QtWidgets.QPushButton("Plane %i" % n,wid)
            buttons[i].setCheckable(True)
            buttons[i].setStyleSheet("background-color: green") if triggerplanes[n] else buttons[i].setStyleSheet("background-color: grey")
            #buttons[i].setChecked(triggerplanes[n])
            if triggerplanes[plane]: buttons[i].toggle()
            buttons[i].clicked.connect(lambda state, pn=n : self.setTriggerplanes(pn,state))
            pTrig.addWidget(buttons[i])
            pass
        
        
        
        #Connection box--------------------------------------------------------------------------------------------------------
        bConn=QtWidgets.QGroupBox(LABEL["panel1.6"][0],wid)
        pConn=QtWidgets.QVBoxLayout(bConn)
        pane.addWidget(bConn,0,5,3,1)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(minport,maxport)
        b.setSingleStep(1)
        b.setValue(portname)
        b.valueChanged.connect(lambda p : self.TLU_ChangePort(p))
        pConn.addWidget(b)
        
        addedhosts={}
        b=QtWidgets.QComboBox(wid)
        addedhosts[default_hostname]=""
        b.addItem(default_hostname)
        selected_hostname=default_hostname
        for x in range(1,10):
            name="ep-ade-gw-0%i" % x
            if name not in addedhosts:
                b.addItem(name)
                addedhosts[name]=""
                pass
            pass
        
        b.setCurrentIndex(0)
        b.currentTextChanged.connect(lambda h : self.TLU_ChangeHost(h))
        pConn.addWidget(b)
        
        buttons["Close"]=QtWidgets.QPushButton("Close panel",wid)
        buttons["Close"].setStyleSheet("background-color: red")
        buttons["Close"].clicked.connect(self.button_close)
        pConn.addWidget(buttons["Close"])
        
        
        
        
        
        
        #Commands box --------------------------------------------------------------------------------------------------------
        bCmds=QtWidgets.QGroupBox("Commands:",wid)
        pCmds=QtWidgets.QHBoxLayout(bCmds)
        pane.addWidget(bCmds,4,0,1,6)
        buttons["Connect"]=QtWidgets.QPushButton("Connect",wid)
        buttons["Connect"].clicked.connect(self.button_connect)
        pCmds.addWidget(buttons["Connect"])
        
        #buttons["Enable"]=QtWidgets.QPushButton("Enable",wid)
        #buttons["Enable"].setStyleSheet("background-color: green")
        #buttons["Enable"].clicked.connect(self.TLU_enable)
        #pCmds.addWidget(buttons["Enable"])
        
        
        
        buttons["Run"]=QtWidgets.QPushButton("Run",wid)
        #buttons["Run"].setStyleSheet("background-color: orange")
        buttons["Run"].clicked.connect(self.button_run)
        pCmds.addWidget(buttons["Run"])
        
        self.fsm_label=QtWidgets.QLabel(wid)
        self.fsm_label.setText("Initial")
        pCmds.addWidget(self.fsm_label)
        
        buttons["ResetCounters"]=QtWidgets.QPushButton("Reset counters",wid)
        buttons["ResetCounters"].clicked.connect(self.button_reset_counters)
        pCmds.addWidget(buttons["ResetCounters"])
        
        
        #Debug box --------------------------------------------------------------------------------------------------------
        self.pLog = QtWidgets.QTextEdit(wid)
        self.pLog.resize(500,500)
        pane.addWidget(self.pLog,5,0,1,2)
        
        
        #Rates box--------------------------------------------------------------------------------------------------------
        bdtCounts=QtWidgets.QGroupBox("Delta t [s]",wid)
        pdtCounts=QtWidgets.QVBoxLayout(bdtCounts)
        pane.addWidget(bdtCounts,5,2,1,1)
        label="time"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pdtCounts.addWidget(b)
        box_counter_dt[0]=QtWidgets.QLineEdit(wid)
        box_counter_dt[0].setPlaceholderText("")
        pdtCounts.addWidget(box_counter_dt[0])
        '''
        for x in range(1,self.numberofplanes+1):
            #if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Time %i" % x
            b.setText(label)
            pdtCounts.addWidget(b)
            box_counter_dt[x]=QtWidgets.QLineEdit(wid)
            box_counter_dt[x].setPlaceholderText("")
            pdtCounts.addWidget(box_counter_dt[x])
             #   pass
            pass
        '''
        #Rates box--------------------------------------------------------------------------------------------------------
        bRCounts=QtWidgets.QGroupBox("Avg Rates [Hz]",wid)
        pRCounts=QtWidgets.QVBoxLayout(bRCounts)
        pane.addWidget(bRCounts,5,3,1,1)
        label="Rate L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pRCounts.addWidget(b)
        box_counter_avg_rate[0]=QtWidgets.QLineEdit(wid)
        box_counter_avg_rate[0].setPlaceholderText("")
        pRCounts.addWidget(box_counter_avg_rate[0])
        for x in range(1,self.numberofplanes+1):
            #if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane rate %i" % x
            b.setText(label)
            pRCounts.addWidget(b)
            box_counter_avg_rate[x]=QtWidgets.QLineEdit(wid)
            box_counter_avg_rate[x].setPlaceholderText("")
            pRCounts.addWidget(box_counter_avg_rate[x])
             #   pass
            pass
        #Current Rate box--------------------------------------------------------------------------------------------------------
        bcurRCounts=QtWidgets.QGroupBox("Current Rates [Hz]",wid)
        pcurRCounts=QtWidgets.QVBoxLayout(bcurRCounts)
        pane.addWidget(bcurRCounts,5,4,1,1)
        label="Rate L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pcurRCounts.addWidget(b)
        box_counter_cur_rate[0]=QtWidgets.QLineEdit(wid)
        box_counter_cur_rate[0].setPlaceholderText("")
        pcurRCounts.addWidget(box_counter_cur_rate[0])
        for x in range(1,self.numberofplanes+1):
            #if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane rate %i" % x
            b.setText(label)
            pcurRCounts.addWidget(b)
            box_counter_cur_rate[x]=QtWidgets.QLineEdit(wid)
            box_counter_cur_rate[x].setPlaceholderText("")
            pcurRCounts.addWidget(box_counter_cur_rate[x])
             #   pass
            pass
        #Counters box--------------------------------------------------------------------------------------------------------
        bCounts=QtWidgets.QGroupBox("Counters",wid)
        pCounts=QtWidgets.QVBoxLayout(bCounts)
        pane.addWidget(bCounts,5,5,1,5)
        label="Counter L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters[0]=QtWidgets.QLineEdit(wid)
        counters[0].setPlaceholderText("")
        pCounts.addWidget(counters[0])
        for x in range(1,self.numberofplanes+1):
           # if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane %i" % x
            b.setText(label)
            pCounts.addWidget(b)
            counters[x]=QtWidgets.QLineEdit(wid)
            counters[x].setPlaceholderText("")
            pCounts.addWidget(counters[x])
          #  pass
            pass
        self.show()
        self.timer = QtCore.QTimer()
        self.set_fsm(NOTCONNECTED)
        pass
    
    
    #fsm     1       not connected
    #fsm     2       connected
    #fsm     3       configured
    #fsm     4       running
    
    def button_connect(self):
        if debug==True: print ("button_connect")
        if debug==True: print (self.status)
        if   self.status==NOTCONNECTED:      # FSM IS NOT CONNECTED
            # wants to connect
            self.set_fsm(CONNECTED)
            pass
        elif self.status==CONFIGURED or self.status==CONNECTED:    # FSM IS CONNECTED
            # wants to disconnect
            self.set_fsm(NOTCONNECTED)
            pass
        else:           # FSM IS IN ILEGAL STATE
            pass
        pass
    
    
    def button_run(self):
        if debug==True: print ("button run")
        if debug==True: print (self.status)
        if self.status==CONFIGURED:      # FSM IS CONFIGURED
            self.set_fsm(RUNNING)
            pass
        elif self.status==RUNNING:    # FSM IS RUNNING
            self.set_fsm(CONFIGURED)
            pass
        else:           # FSM IS IN ILEGAL STATE
            pass
        pass
    
    def set_fsm(self, next_fsm):
        self.status=next_fsm
        if next_fsm   == NOTCONNECTED:
            if self.disconnect() == NOTCONNECTED:
                self.fsm_label.setText("Not connected")
                buttons["Connect"].setText("Connect")
                buttons["Connect"].setStyleSheet("");
                buttons["Run"].setEnabled(False)
                pass
            else:
                print ("ERROR 1")
            pass
        elif next_fsm == CONNECTED:
            if self.connect() == CONNECTED:
                self.fsm_label.setText("Connected")
                buttons["Connect"].setText("Disconnect")
                buttons["Connect"].setStyleSheet("background-color: green")
                buttons["Run"].setEnabled(True)
                self.set_fsm(CONFIGURED)
                pass
            else:
                self.set_fsm(NOTCONNECTED)
                print ("ERROR 2: Connection to TLU failed")
            pass
        elif next_fsm == CONFIGURED:
            self.TLU_set_NOT_RUNNING()
            buttons["Run"].setText("Run")
            self.fsm_label.setText("Configured")
            buttons["Connect"].setText("Disconnect")
            buttons["Run"].setEnabled(True)
            pass
        elif next_fsm == RUNNING:
            if self.TLU_set_RUNNING()==RUNNING:
                self.fsm_label.setText("Running")
                buttons["Run"].setText("Stop run")
                buttons["Run"].setEnabled(True)
                pass
            else:
                print ("ERROR 3: Cannot start TLU")
                self.set_fsm(CONFIGURED)
                pass
            pass
        self.updatestatusBar()
        pass
    
    def button_close(self):
        self.stop()
        pass
    
    def stop(self):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                "Are you sure you wish to quit?",
                QtWidgets.QMessageBox.Yes |
                QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            #self.timer.stop()
            if enableROOT==True:
                print ("Closing ROOT")
                self.ntuple.Write()
                self.mg.SetTitle("Trigger rate;T[s];R[times]")
                self.mg.Draw("AP")
                self.mg.Write("mg")
                self.rf.Close()
                pass
            #self.disconnect()
            tlu=None
            sys.exit()
            pass
        pass
    
    def killproc(self):
        sys.exit()
        pass
    
    
    def button_reset_counters(self):
        tlu.ResetCounters()
        pass
    
    
    
    
    
    def geTime(self):
        now=datetime.datetime.now()
        return now.strftime("%Y/%m/%d %H:%M:%S")
    
    def setPlanes(self,pn, state):
        print ("Set active plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        planes[pn]=state
        if state==True: buttons["iPlane%i" % pn].setStyleSheet("background-color: green")
        else: buttons["iPlane%i" % pn].setStyleSheet("background-color: grey")
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def setTriggerplanes(self,pn, state):
        #print ("Set active plane: %s as %s" % (str(pn), str(state)))
        global triggerplanes
        self.pending=True
        triggerplanes[pn]=state
        if state==True: buttons["tPlane%i" % pn].setStyleSheet("background-color: green")
        else: buttons["tPlane%i" % pn].setStyleSheet("background-color: grey")
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def SetVeto(self,pn, state):
        #print ("SetVeto plane: %s to %s" % (str(pn), str(state)))
        cc=int(state/cc_factor)
        self.pending=True
        self.pLog.append("%s: Trigger width of Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(cc)))
        vetos[pn]=state
        pass
    
    def SetLongInput(self,pn, state):
        #print ("Set long input plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        cc=int(state/cc_factor)
        widths[pn]=state
        self.pLog.append("%s: SetLongInput on Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(cc)))
        #self.settings.setValue('scan',v)
        pass
    
    def SetLongOutput(self, state):
        #print ("Set long input plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        cc=int(state/cc_factor)
        widths[0]=state
        self.pLog.append("%s: Set SetLongOutput on Plane[%s] to %s [clock cycles]" % (self.geTime(),str(0), str(cc)))
        #self.settings.setValue('scan',v)
        pass
    
    def SetMaxRate(self, val):
        global MaxRate
        cc=int(state/cc_factor)
        self.pending=True
        MaxRate=cc
        self.pLog.append("%s: Set SetMaxRate to %s [clock cycles]" % (self.geTime(),str(cc)))
        pass
    
    def TLU_reset_counters(self):
        tlu.Enable(False)
        time.sleep(0.2)
        '''
        for x in last_count:
            print "14",x
            last_count[x]=0
            pass
        '''
        tlu.ResetCounters()
        print ("----------------------------")
        print ("TLU_reset_counters:startingTime:", self.startingTime)
        self.startingTime = time.time()
        print ("TLU_reset_counters:startingTime:", self.startingTime)
        time.sleep(0.2)
        tlu.Enable(True)
        pass
    
    
    def disconnect(self):
        #self.timer.stop()
        tlu=None
        return NOTCONNECTED
    
    
    def connect(self):
        global tlu
        tlu=PyMaltaTLU.MaltaTLU()
        tlu.SetVerbose(verbose)
        connection_str="udp://%s:%s" % (selected_hostname,str( portname))
        tlu.Connect(connection_str)
        time.sleep(0.1)
        connectedTLU=tlu.GetVersion()
        tlu.SetVerbose(verbose)
        if connectedTLU in compatible_TLUs:
            #self.timer.start(1000)
            self.pLog.append("%s: Connected to %s" % (self.geTime(),connection_str))
            self.pLog.append("%s: Firmware version %s" % (self.geTime(),str(connectedTLU)))
            print ("Connected to %s" % connection_str)
            #self.timer.timeout.connect(self.updateRates)
            return CONNECTED
        else:
            self.pLog.append("%s: Error: Could not connect to %s. Wrong plane? Wrong host?" % (self.geTime(),connection_str))
            return NOTCONNECTED
        pass
    
    def updateRates(self):
        unit=1#KHz
        global t_counters
        print ("self.dt=time.time()-self.startingTime:", self.startingTime)
        self.dt=time.time()-self.startingTime
        if self.status==RUNNING:
            for x in range(0,numberofplanes+1):
                if x==0 and verbose: print ("-"*10)
                cur_value=self.TLU_GetCounter(x)
                print ("cur_value:", cur_value)
                print ("last_count[x]:", last_count[x])
                diff=cur_value - last_count[x]
                cur_rate=diff/updateRate
                avg_rate=float(cur_value)/float(self.dt)
                if verbose: print ("Plane[%i]\t Total value: %i.\t\tIncreased: %i.\t\tTotal Rate: %f.\t\tDt: %f." % (x, cur_value, diff, avg_rate, float(self.dt)))
                rates[x]=avg_rate
                
                box_counter_dt[0].setPlaceholderText("%i" % (self.dt))
                box_counter_avg_rate[x].setPlaceholderText("%.2f" % (avg_rate/unit))
                box_counter_cur_rate[x].setPlaceholderText("%.2f" % (cur_rate/unit))
                counters[x].setPlaceholderText("%i" % (cur_value))
                last_count[x]=cur_value
                pass
            if enableROOT==True: self.updatePlot()
            pass
        pass
    
    def getRate(self, value):
        pass
    
    def updatestatusBar(self):
        self.statusBar().showMessage('%s' % STATUS_NAMES[self.status])
        self.fsm_label.setText(STATUS_NAMES[self.status])
        pass
    
    def save(self):
        name = QtWidgets.QFileDialog.getSaveFileName(None)
        if not name[0]: return
        cwd = os.getcwd()
        f = open(name, "w")
        text = self.pLog.toPlainText()
        f.write(text)
        f.close()
        pass
    
    
    
    def closeEvent(self,event):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            #self.stop()
            event.accept()
            sys.exit()
        else:
            event.ignore()
            pass
        pass

    def execute(self,cmd):
        popen = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True,
                                 universal_newlines=True,
                                 preexec_fn=os.setsid)
        self.pid=popen.pid
        for stdout_line in iter(popen.stdout.readline, ""):
            yield stdout_line 
            pass
        popen.stdout.close()
        return_code = popen.wait()
        self.pid=0
        pass
    
    
    
    #TLU
    
    def TLU_ChangePort(self, p):
        global portname
        portname=int(p)
        self.pLog.append("%s: New port [%s]" % (self.geTime(),str(portname)))
        pass
    
    def TLU_ChangeHost(self, h):
        global selected_hostname
        selected_hostname=h
        self.pLog.append("%s: New host [%s]" % (self.geTime(),str(selected_hostname)))
        pass
    
    def TLU_GetCounter(self, plane):
        value=-1
        value=tlu.GetCounterTrigger(plane)
        return value
    

    def TLU_set_RUNNING(self):
        self.TLU_reset_counters()
        
        time.sleep(0.1)
        #Reset counters
        tlu.ResetCounters()
        #input planes
        tlu.SetPlanes(planes[1], planes[2], planes[3], planes[4], planes[5], False, False, False)
        if debug==True:tlu.GetPlanes()
        #vetos  
        for p in vetos:
            tlu.SetTriggerWidth(p, vetos[p])
            if debug==True:tlu.GetTriggerWidth(p)
        #long inputs
        for p in widths:
            tlu.SetLongInput(p, widths[p])
            if debug==True:tlu.GetLongInput(p)
            pass
        #MaxRate
        tlu.SetL1AWidth(MaxRate)
        if debug==True:tlu.GetL1AWidth()
        #outputs
        tlu.SetCoincidenceOutputPlanes(triggerplanes[1], triggerplanes[2], triggerplanes[3], triggerplanes[4], triggerplanes[5], False, False, False)
        if debug==True:tlu.GetCoincidenceOutputPlanes()
        #All ok
        tlu.Enable(True)
        '''
        print "startingTime", startingTime
        if startingTime==0:
            startingTime=time.time()
            pass
        '''
        self.pending=False
        self.timer.start(1000)
        self.timer.timeout.connect(self.updateRates)
        #self.pLog.append("%s: %s" % (self.geTime(),self.status))
        return RUNNING
        
    def TLU_set_NOT_RUNNING(self):
        self.timer.stop()
        tlu.Enable(False)
        #self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
    
    
    #GUI functions

    def prepareROOT(self):
        if enableROOT==False:
            print ("ROOT NOT ENABLED")
            pass
        else:
            
            now=datetime.datetime.now()
            now_st=now.strftime("%Y%m%d__%H_%M_%S")
            rfn="TLU_output_%s.root" % now_st
            self.rf = ROOT.TFile(rfn,"RECREATE")
            self.ntuple          = ROOT.TTree("tlu","")
            self.n_timestamp     = array.array('L',(0,)) 
            #self.n_rates={} 
            self.n_planes={}
            for x in range(1,self.numberofplanes+1):
                if x == 0 :
                    self.n_planes[x]     = array.array('f',(0,))
                    #self.n_rates[x]     = array.array('f',(0,))
                    self.ntuple.Branch("plane%i" % x, self.n_planes[x], "plane%i/f" % x)
                    #self.ntuple.Branch("rate%i" % x, self.n_rates[x], "rate%i/i" % x)
                    pass
                pass
            
            self.cs = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
            self.cs.SetLogy()
            #self.lg=ROOT.TLegend(0.2, 0.6, 0.4, 0.9)
            self.lg=ROOT.TLegend(0.2, 0.7, 0.3, 0.9)
            self.lg.SetHeader("Planes","C")
            self.mg = ROOT.TMultiGraph()
            title="Triggers;T[s];N"
            self.mg.SetTitle(title)
            for x in range(0,self.numberofplanes+1):
                if x==0:
                    self.g[x]=ROOT.TGraph()
                    lgname="Trigger rate"
                    pointSize=1.2
                    self.lg.AddEntry(self.g[x],lgname , "p")
                    self.g[x].SetMarkerColor(x+1)
                    self.g[x].SetLineColor(x+1)
                    self.g[x].SetMarkerStyle(20)
                    self.g[x].SetMarkerSize(pointSize)
                    #self.g[x].SetPoint(self.g[x].GetN(), x, x)
                    self.mg.Add(self.g[x])
                else:
                    lgname="Plane %i" % x
                    pointSize=1
                    pass
                pass
            pass
        self.lg.Draw()
        self.mg.Draw("APL")
        pass
    def updatePlot(self):
        #print "dt 2 :", self.dt
        for x in range(0,self.numberofplanes+1):
            if x==0:
                self.g[x].SetPoint(self.g[x].GetN(), float(self.dt), float(last_count[x]))
                self.mg.Add(self.g[x])
                self.n_timestamp[0]=int(time.time())
                if verbose: print ("rates[x]", rates[x])
                self.n_planes[x]=float(rates[x])
                pass
            else:
                self.n_planes[x]=float(last_count[x])
                pass
            #self.n_rates[x]=int(rates[x])
            self.ntuple.Fill()
            pass
        self.lg.Draw()
        self.cs.Update()
        self.cs.Draw()
        pass
    
    pass

if __name__=="__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("-nop"  ,"--numberofplanes",help="Number of planes",type=int,default=4)
    parser.add_argument("-p"    ,"--plot",help="Enable plotting",action='store_true')
    parser.add_argument("-a"    ,"--address",help="ipbus address")

    args=parser.parse_args()
    numberofplanes=args.numberofplanes
    enableROOT=args.plot
    print("Starting TLU GUI")
    updateRate=1
    app = QtWidgets.QApplication(sys.argv)
    
    gui = GUI(numberofplanes, enableROOT)
    '''
    timer = QtCore.QTimer()
    timer.timeout.connect(gui.updateRCounter)
    timer.timeout.connect(gui.updateCounter)
    timer.start(updateRate*1000)
    '''
    print ("END")
    sys.exit(app.exec_())

