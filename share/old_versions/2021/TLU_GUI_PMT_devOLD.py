#!/usr/bin/env python
##############################################
# Ignacio.Asensi@cern.ch
# Oct 2019
# updates Feb 2020
# adding scintillator and UI updates May 2020
##############################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLUPMT
import time
import datetime
import argparse
import ROOT
import AtlasStyle
import array

# CONNECTION SETTINGS
default_hostname="ep-ade-gw-01"
portname=50007
default_host=4
minport=50000
maxport=50021


# VARS INITIALIZATION
tlu=""
connection_str=""
scintillators={}
widths_cc={} 
widths_ns={} 
vetos_cc={}
vetos_ns={}

range_min_width={}
range_max_width={}
range_min_veto={}
range_max_veto={}


# TLU GENERAL SETTINGS
debug=False
defaultNumberOfPlanes=4
resetCountersOnEachStart=False
compatible_TLUs=[7]#Number of FW version. Check with FW
cc_factor=3.125 #at 320MHz
SC_enabled=True
scintillators[0]=SC_enabled


to_be_saved=["count", "rate"]
planes_to_be_plotted=[0] #0 is L1A                                                                                 
planes_to_be_saved=[0]


# INPUT SIGNAL FROM PLANES. FALSE MEANS PLANE IS DISABLED or IT IS DUT. CANNOT BE ENABLED
planes={}
planes[1]=True
planes[2]=False
planes[3]=True
planes[4]=True
planes[5]=False


# OUTPUT TRIGGER TO PLANES. FALSE MEANS TRIGGER NOT PROVIDED TO PLANE
triggerplanes={}
triggerplanes[1]=True
triggerplanes[2]=True
triggerplanes[3]=True
triggerplanes[4]=True
triggerplanes[5]=False


# Trigger counters
t_counters={}
t_counters["L1A"]=0
t_counters["1"]=0
t_counters["2"]=0
t_counters["3"]=0
t_counters["4"]=0
t_counters["5"]=0
last_count={}
last_count["L1A"]=0
last_count["1"]=0
last_count["2"]=0
last_count["3"]=0
last_count["4"]=0
last_count["5"]=0
last_count["SC"]=0


# GUI DEFAULT VALUES
range_min_width["L1A"]=0
range_min_width["1"]=0
range_min_width["2"]=0
range_min_width["3"]=0
range_min_width["4"]=0
range_min_width["5"]=0
range_min_width["SC"]=0
range_max_width["L1A"]=1099
range_max_width["1"]=100299
range_max_width["2"]=100299
range_max_width["3"]=100299
range_max_width["4"]=100299
range_max_width["5"]=100299
range_max_width["SC"]=299
widths_ns["L1A"]=6
widths_ns["1"]=30
widths_ns["2"]=30
widths_ns["3"]=30
widths_ns["4"]=30
widths_ns["5"]=30
widths_ns["SC"]=2

range_min_maxrate=0
range_max_maxrate=10000000
MaxRate_ns=8
range_min_veto["SC"]=1
range_min_veto["1"]=1
range_min_veto["2"]=1
range_min_veto["3"]=1
range_min_veto["4"]=1
range_min_veto["5"]=1
range_max_veto["SC"]=1000
range_max_veto["1"]=1000
range_max_veto["2"]=1000
range_max_veto["3"]=1000
range_max_veto["4"]=1000
range_max_veto["5"]=1000
vetos_ns["L1A"]=100
vetos_ns["1"]=44
vetos_ns["2"]=44
vetos_ns["3"]=44
vetos_ns["4"]=44
vetos_ns["5"]=44
vetos_ns["SC"]=1



# CONVERTING DEFAULT VALUES (NS) TO CLOCK CYCLES (CC)
for w in widths_ns:
    widths_cc[w]=int(widths_ns[w]/cc_factor)
    pass

for v in vetos_ns:
    vetos_cc[v]=int(vetos_ns[v]/cc_factor)
    pass

MaxRate_cc=int(MaxRate_ns/cc_factor)


# FSM
STATUS_NAMES={}
STATUS_NAMES[1]="Not connected"
STATUS_NAMES[2]="Connected"
STATUS_NAMES[3]="Configured"
STATUS_NAMES[4]="Running"
###################################################


class GUI(QtWidgets.QMainWindow):    
    def __init__(self, numberofplanes, plot, v,save):
        global verbose
        global SC_enabled
        global cc_factor
        global counters
        global box_counter_avg_rate
        global box_counter_dt
        global box_counter_cur_rate
        global box_counter_SC
        global tlu
        global buttons
        global scintillators
        global startingTime
        global last_read
        global last_count
        global connection_str
        global compatible_TLUs
        global minport
        global maxport
        global MaxRate_ns
        global MaxRate_cc
        global rates
        global selected_hostname
        global NOTCONNECTED
        global CONNECTED
        global CONFIGURED
        global RUNNING
        global resetCountersOnEachStart
        global LABEL
        global triggerplanes
        global to_be_saved
        global planes_to_be_plotted
        global planes_to_be_saved
        global labels_help
        super(GUI, self).__init__()
        verbose=v
        #FSM states
        NOTCONNECTED=1
        CONNECTED   =2
        CONFIGURED  =3
        RUNNING     =4
        #Buttons labels
        LABEL={}
        LABEL["MaxRate"] ="Max rate (Hz)"#"L1A width [ms]:"
        LABEL["Veto"]    ="Veto [ns]:"
        LABEL["Width"]   ="Width [ns]:"
        LABEL["Trigger"]  ="Trigger:"
        LABEL["widthL1A"]="Output length [ns]"
        LABEL["panel1.5"]="Provide trigger to:"
        LABEL["panel1.6"]="Connection:"
        #initialize vars
        self.dt=""
        self.startingTime=0
        self.numberofplanes=numberofplanes
        self.plotOnFly=plot
        self.saveToRoot=save
        self.timeoutstarted=False
        #GUI vars
        box_counter_SC={}
        box_counter_avg_rate={}
        box_counter_dt={}
        box_counter_cur_rate={}
        buttons={}
        labels_help={}
        #counters and triggers bufffers
        rates={}
        counters={}
        last_read=""
        #Prepare root 
        print("Save to root:",self.saveToRoot)
        if self.plotOnFly or self.saveToRoot:
            self.cs=""
            self.g={}
            self.mg =""
            AtlasStyle.SetAtlasStyle()
            self.prepareROOT()
            pass
        else:
            print("Root is disabled")
            pass
        #Qt GUI vars
        font = QtGui.QFont("Helvetica");
        QtWidgets.QApplication.setFont(font);
        QtWidgets.QApplication.setStyle("Plastique")
        self.settings=QtCore.QSettings('TLUGUI')
        self.setGeometry(50,50,850,800)
        self.setWindowTitle("TLU")
        wid=QtWidgets.QWidget(self)
        self.setCentralWidget(wid)
        pane=QtWidgets.QGridLayout(wid)
        self.pLog = QtWidgets.QTextEdit(wid)
        self.pLog.resize(100,800)
        pane.addWidget(self.pLog,6,0,1,6)
        #Planes box--------------------------------------------------------------------------------------------------------
        bPlanes=QtWidgets.QGroupBox("Planes:",wid)
        pPlanes=QtWidgets.QVBoxLayout(bPlanes)
        pane.addWidget(bPlanes,0,0,4,1)
        
        
        buttons["SC"]=QtWidgets.QPushButton("Scintillator",wid)
        buttons["SC"].setCheckable(SC_enabled)
        buttons["SC"].setStyleSheet("background-color: green") if SC_enabled else buttons["SC"].setStyleSheet("background-color: grey") 
        buttons["SC"].toggle()
        buttons["SC"].clicked.connect(lambda state: self.setSC(state))
        pPlanes.addWidget(buttons["SC"])
        for plane in range(1,numberofplanes+1):
            i="iPlane%i" % plane #"i" as input planes
            buttons[i]=QtWidgets.QPushButton("Plane %i" % plane,wid)
            buttons[i].setCheckable(True)
            buttons[i].setStyleSheet("background-color: green") if planes[plane] else buttons[i].setStyleSheet("background-color: grey") 
            if planes[plane]: buttons[i].toggle()
            buttons[i].clicked.connect(lambda state, pn=plane : self.setPlanes(pn,state))
            pPlanes.addWidget(buttons[i])
            pass
        
        #Veto box--------------------------------------------------------------------------------------------------------
        bVeto=QtWidgets.QGroupBox(LABEL["Veto"],wid)
        pVeto=QtWidgets.QVBoxLayout(bVeto)
        pane.addWidget(bVeto,0,1,4,1)
        
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_veto["SC"],range_max_veto["SC"])
        b.setSingleStep(1)
        b.setValue(vetos_ns["SC"])
        b.valueChanged.connect(lambda state: self.SetVeto("SC", state))
        pVeto.addWidget(b)
        if verbose:
            labels_help["veto"]={}
            labels_help["veto"]["SC"]=QtWidgets.QLabel(wid)
            labels_help["veto"]["SC"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["veto"]["SC"].setText(self.dolabel(vetos_cc["SC"]))
            pVeto.addWidget(labels_help["veto"]["SC"])
            pass

        for p in range(1,numberofplanes+1):
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_veto[str(p)],range_max_veto[str(p)])
            b.setSingleStep(1)
            b.setValue(vetos_ns[str(p)])
            b.valueChanged.connect(lambda state, pn=p : self.SetVeto(str(pn),state))
            pVeto.addWidget(b)
            if verbose:
                labels_help["veto"][str(p)]=QtWidgets.QLabel(wid)
                labels_help["veto"][str(p)].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["veto"][str(p)].setText(self.dolabel(vetos_cc[str(p)]))
                pVeto.addWidget(labels_help["veto"][str(p)])
                pass
            pass
        
        #Width box--------------------------------------------------------------------------------------------------------
        bWidth=QtWidgets.QGroupBox(LABEL["Width"],wid)
        pWidth=QtWidgets.QVBoxLayout(bWidth)
        pane.addWidget(bWidth,0,2,4,1)
                
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_width["SC"],range_max_width["SC"])
        b.setSingleStep(1)
        b.setValue(widths_ns["SC"]) 
        b.valueChanged.connect(lambda val : self.SetWidth("SC",val))
        pWidth.addWidget(b)

        if verbose:
            labels_help["width"]={}
            labels_help["width"]["SC"]=QtWidgets.QLabel(wid)
            labels_help["width"]["SC"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["width"]["SC"].setText(self.dolabel(widths_cc["SC"]))
            pWidth.addWidget(labels_help["width"]["SC"])
            pass
        
        for p in range(1,numberofplanes+1):
            if p== 0 : continue # 0 is widthL1A and it goes later
            b=QtWidgets.QSpinBox(wid)
            b.setRange(range_min_width[str(p)],range_max_width[str(p)])
            b.setSingleStep(1)
            b.setValue(widths_ns[str(p)])
            b.valueChanged.connect(lambda state, pn=p : self.SetWidth(str(pn),state))
            pWidth.addWidget(b)
            if verbose:
                labels_help["width"][str(p)]=QtWidgets.QLabel(wid)
                labels_help["width"][str(p)].setAlignment(QtCore.Qt.AlignCenter)
                labels_help["width"][str(p)].setText(self.dolabel(widths_cc[str(p)]))
                pWidth.addWidget(labels_help["width"][str(p)])
                pass
            pass
        

        #l1A trigger box--------------------------------------------------------------------------------------------------------
        bMaxRate=QtWidgets.QGroupBox(LABEL["Trigger"],wid)
        pMaxRate=QtWidgets.QVBoxLayout(bMaxRate)
        pane.addWidget(bMaxRate,0,3,4,1)
        b=QtWidgets.QLabel(wid)
        b.setText(LABEL["MaxRate"])
        pMaxRate.addWidget(b)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_maxrate, range_max_maxrate)
        b.setSingleStep(1)
        b.setValue(MaxRate_ns)
        b.valueChanged.connect(lambda val : self.SetMaxRate(val))
        pMaxRate.addWidget(b)
        if verbose:
            labels_help["MaxRate"]={}
            labels_help["MaxRate"]=QtWidgets.QLabel(wid)
            labels_help["MaxRate"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["MaxRate"].setText(self.dolabel(MaxRate_cc))
            pMaxRate.addWidget(labels_help["MaxRate"])
            pass

        b=QtWidgets.QLabel(wid)
        b.setText(LABEL["widthL1A"])
        pMaxRate.addWidget(b)
        b=QtWidgets.QSpinBox(wid)
        b.setRange(range_min_width["L1A"],range_max_width["L1A"])
        b.setSingleStep(1)
        b.setValue(widths_ns["L1A"]) 
        b.valueChanged.connect(lambda val : self.SetWidth("L1A", val))
        pMaxRate.addWidget(b)
        if verbose:
            labels_help["width"]["L1A"]=QtWidgets.QLabel(wid)
            labels_help["width"]["L1A"].setAlignment(QtCore.Qt.AlignCenter)
            labels_help["width"]["L1A"].setText(self.dolabel(widths_cc["L1A"]))
            pMaxRate.addWidget(labels_help["width"]["L1A"])
            pass

                
        #Provide trigger to box--------------------------------------------------------------------------------------------------------
        bTrig=QtWidgets.QGroupBox(LABEL["panel1.5"],wid)
        pTrig=QtWidgets.QVBoxLayout(bTrig)
        pane.addWidget(bTrig,0,4,4,1)
        
        for n in range(1,numberofplanes+1):
            i="tPlane%i" % n #t as trigger plane
            buttons[i]=QtWidgets.QPushButton("Plane %i" % n,wid)
            buttons[i].setCheckable(True)
            buttons[i].setStyleSheet("background-color: green") if triggerplanes[n] else buttons[i].setStyleSheet("background-color: grey")
            if triggerplanes[plane]: buttons[i].toggle()
            buttons[i].clicked.connect(lambda state, pn=n : self.setTriggerplanes(pn,state))
            pTrig.addWidget(buttons[i])
            pass
        
        #Connection box--------------------------------------------------------------------------------------------------------
        bConn=QtWidgets.QGroupBox(LABEL["panel1.6"],wid)
        pConn=QtWidgets.QVBoxLayout(bConn)
        pane.addWidget(bConn,0,5,3,1)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(minport,maxport)
        b.setSingleStep(1)
        b.setValue(portname)
        b.valueChanged.connect(lambda p : self.TLU_ChangePort(p))
        pConn.addWidget(b)
        
        addedhosts={}
        b=QtWidgets.QComboBox(wid)
        addedhosts[default_hostname]=""
        b.addItem(default_hostname)
        selected_hostname=default_hostname
        for x in range(1,10):
            name="ep-ade-gw-0%i" % x
            if name not in addedhosts:
                b.addItem(name)
                addedhosts[name]=""
                pass
            pass
        b.setCurrentIndex(0)
        b.currentTextChanged.connect(lambda h : self.TLU_ChangeHost(h))
        pConn.addWidget(b)
        
        buttons["Close"]=QtWidgets.QPushButton("Close panel",wid)
        buttons["Close"].setStyleSheet("background-color: red")
        buttons["Close"].clicked.connect(self.button_close)
        pConn.addWidget(buttons["Close"])
        
        #Commands box --------------------------------------------------------------------------------------------------------
        bCmds=QtWidgets.QGroupBox("Commands:",wid)
        pCmds=QtWidgets.QHBoxLayout(bCmds)
        pane.addWidget(bCmds,4,0,1,6)
        buttons["Connect"]=QtWidgets.QPushButton("Connect",wid)
        buttons["Connect"].clicked.connect(self.button_connect)
        pCmds.addWidget(buttons["Connect"])
        
        buttons["Run"]=QtWidgets.QPushButton("Run",wid)
        buttons["Run"].clicked.connect(self.button_run)
        pCmds.addWidget(buttons["Run"])
        
        self.fsm_label=QtWidgets.QLabel(wid)
        self.fsm_label.setAlignment(QtCore.Qt.AlignCenter)
        self.fsm_label.setText("Initial")
        pCmds.addWidget(self.fsm_label)
        
        #Avg Rates     box--------------------------------------------------------------------------------------------------------
        bRCounts=QtWidgets.QGroupBox("Avg Rates [Hz]",wid)
        pRCounts=QtWidgets.QVBoxLayout(bRCounts)
        pane.addWidget(bRCounts,5,0,1,1)
        #Scintillator rate
        b=QtWidgets.QLabel(wid)
        b.setText("Scintillator")
        pRCounts.addWidget(b)
        box_counter_avg_rate["SC"]=QtWidgets.QLineEdit(wid)
        box_counter_avg_rate["SC"].setPlaceholderText("")
        pRCounts.addWidget(box_counter_avg_rate["SC"])
        #L1A rate
        b=QtWidgets.QLabel(wid)
        b.setText("L1A")
        pRCounts.addWidget(b)
        box_counter_avg_rate["L1A"]=QtWidgets.QLineEdit(wid)
        box_counter_avg_rate["L1A"].setPlaceholderText("")
        pRCounts.addWidget(box_counter_avg_rate["L1A"])
        for x in range(1,self.numberofplanes+1):
            b=QtWidgets.QLabel(wid)
            x=str(x)
            label="Plane %s" % x
            b.setText(label)
            pRCounts.addWidget(b)
            box_counter_avg_rate[x]=QtWidgets.QLineEdit(wid)
            box_counter_avg_rate[x].setPlaceholderText("")
            pRCounts.addWidget(box_counter_avg_rate[x])
            pass
        #Current Rates     box--------------------------------------------------------------------------------------------------------
        bcurRCounts=QtWidgets.QGroupBox("Current Rates [Hz]",wid)
        pcurRCounts=QtWidgets.QVBoxLayout(bcurRCounts)
        pane.addWidget(bcurRCounts,5,1,1,1)
        #Scintillator current rate
        b=QtWidgets.QLabel(wid)
        b.setText("Scintillator")
        pcurRCounts.addWidget(b)
        box_counter_cur_rate["SC"]=QtWidgets.QLineEdit(wid)
        box_counter_cur_rate["SC"].setPlaceholderText("")
        pcurRCounts.addWidget(box_counter_cur_rate["SC"])
        #L1A current rate
        b=QtWidgets.QLabel(wid)
        b.setText("Rate L1A")
        pcurRCounts.addWidget(b)
        box_counter_cur_rate["L1A"]=QtWidgets.QLineEdit(wid)
        box_counter_cur_rate["L1A"].setPlaceholderText("")
        pcurRCounts.addWidget(box_counter_cur_rate["L1A"])
        #Planes current rate
        for x in range(1,self.numberofplanes+1):
            #if planes[x]==True:
            x=str(x)
            b=QtWidgets.QLabel(wid)
            label="Plane rate %s" % x
            b.setText(label)
            pcurRCounts.addWidget(b)
            box_counter_cur_rate[x]=QtWidgets.QLineEdit(wid)
            box_counter_cur_rate[x].setPlaceholderText("")
            pcurRCounts.addWidget(box_counter_cur_rate[x])
            pass

        #Counters      box--------------------------------------------------------------------------------------------------------
        bCounts=QtWidgets.QGroupBox("Counters",wid)
        pCounts=QtWidgets.QVBoxLayout(bCounts)
        pane.addWidget(bCounts,5,2,1,1)
        
        label="Scintillator"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters["SC"]=QtWidgets.QLineEdit(wid)
        counters["SC"].setPlaceholderText("")
        pCounts.addWidget(counters["SC"])
                
        label="Trigger"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters["L1A"]=QtWidgets.QLineEdit(wid)
        counters["L1A"].setPlaceholderText("")
        pCounts.addWidget(counters["L1A"])
        
        for x in range(1,self.numberofplanes+1):
            x=str(x)
            b=QtWidgets.QLabel(wid)
            label="Plane %s" % x
            b.setText(label)
            pCounts.addWidget(b)
            counters[x]=QtWidgets.QLineEdit(wid)
            counters[x].setPlaceholderText("")
            pCounts.addWidget(counters[x])
            pass
        
        #Run      box--------------------------------------------------------------------------------------------------------
        bdtCounts=QtWidgets.QGroupBox("Run",wid)
        pdtCounts=QtWidgets.QVBoxLayout(bdtCounts)
        pdtCounts.setAlignment(QtCore.Qt.AlignTop)
        pane.addWidget(bdtCounts,5,3,1,1)
        label="Time [s]"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pdtCounts.addWidget(b)
        box_counter_dt[0]=QtWidgets.QLineEdit(wid)
        box_counter_dt[0].setPlaceholderText("")
        pdtCounts.addWidget(box_counter_dt[0])
                
        buttons["ResetCounters"]=QtWidgets.QPushButton("Reset counters",wid)
        buttons["ResetCounters"].clicked.connect(self.TLU_reset_counters)
        pdtCounts.addWidget(buttons["ResetCounters"])
        
        # LAUNCH GUI -------------------------------------------------------------------------------------------------------
        self.show()
        self.timer = QtCore.QTimer()
        self.set_fsm(NOTCONNECTED)
        pass
    
    #fsm     1       not connected
    #fsm     2       connected
    #fsm     3       configured
    #fsm     4       running
    def button_connect(self):
        if debug==True: print("Status:  %s" % self.status)
        if   self.status==NOTCONNECTED:      # FSM IS NOT CONNECTED
            # wants to connect
            self.set_fsm(CONNECTED)
            pass
        elif self.status==CONFIGURED or self.status==CONNECTED:    # FSM IS CONNECTED
            # wants to disconnect
            self.set_fsm(NOTCONNECTED)
            pass
        else:           # FSM IS IN ILEGAL STATE
            pass
        pass
    
    
    def button_run(self):
        if debug==True: print("Status: %s" % self.status)
        if self.status==CONFIGURED:      # FSM IS CONFIGURED
            self.set_fsm(RUNNING)
            pass
        elif self.status==RUNNING:    # FSM IS RUNNING
            self.set_fsm(CONFIGURED)
            pass
        else:           # FSM IS IN ILEGAL STATE
            pass
        pass
    
    def set_fsm(self, next_fsm):
        self.status=next_fsm
        if next_fsm   == NOTCONNECTED:
            if self.disconnect() == NOTCONNECTED:
                self.fsm_label.setText("Not connected")
                buttons["Connect"].setText("Connect")
                buttons["Connect"].setStyleSheet("");
                buttons["Run"].setEnabled(False)
                pass
            else:
                print("ERROR 1")
            pass
        elif next_fsm == CONNECTED:
            if self.connect() == CONNECTED:
                self.fsm_label.setText("Connected")
                buttons["Connect"].setText("Disconnect")
                buttons["Connect"].setStyleSheet("background-color: green")
                buttons["Run"].setEnabled(True)
                self.set_fsm(CONFIGURED)
                pass
            else:
                self.set_fsm(NOTCONNECTED)
                print("ERROR 2: Connection to TLU failed")
            pass
        elif next_fsm == CONFIGURED:
            self.TLU_set_NOT_RUNNING()
            buttons["Run"].setText("Run")
            self.fsm_label.setText("Configured")
            buttons["Connect"].setText("Disconnect")
            buttons["Run"].setEnabled(True)
            pass
        elif next_fsm == RUNNING:
            if self.TLU_set_RUNNING()==RUNNING:
                self.fsm_label.setText("Running")
                buttons["Run"].setText("Stop run")
                buttons["Run"].setEnabled(True)
                pass
            else:
                print("ERROR 3: Cannot start TLU")
                self.set_fsm(CONFIGURED)
                pass
            pass
        self.updatestatusBar()
        pass
    
    def button_close(self):
        self.stop()
        pass
    
    def stop(self):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                "Are you sure you wish to quit?",
                QtWidgets.QMessageBox.Yes |
                QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            #self.timer.stop()
            if self.saveToRoot:
                print("Closing ROOT")
                self.ntuple.Write()
                self.mg.SetTitle("Trigger rate;T[s];R[times]")
                self.mg.Draw("AP")
                self.mg.Write("mg")
                self.rf.Close()
                pass
            #self.disconnect()
            tlu=None
            sys.exit()
            pass
        pass
    
    def killproc(self):
        sys.exit()
        pass
    
    def dolabel(self,cc):
        return "%i cc" % cc

    def button_reset_counters(self):
        tlu.ResetCounters()
        pass
    
    def geTime(self):
        now=datetime.datetime.now()
        return now.strftime("%Y/%m/%d %H:%M:%S")
    
    def setPlanes(self,pn, state):
        planes[pn]=state
        if state==True: buttons["iPlane%i" % pn].setStyleSheet("background-color: green")
        else: buttons["iPlane%i" % pn].setStyleSheet("background-color: grey")
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def setTriggerplanes(self,pn, state):
        #global triggerplanes FIXME is this needed
        triggerplanes[pn]=state
        if state==True: buttons["tPlane%i" % pn].setStyleSheet("background-color: green")
        else: buttons["tPlane%i" % pn].setStyleSheet("background-color: grey")
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        if verbose: print("Verbose\tPlane[%s] %s" % (str(pn), str(state)))
        pass
    
    def SetVeto(self,pn, state):
        cc=int(state/cc_factor)
        vetos_cc[pn]=cc
        self.pLog.append("%s: Set veto on plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(cc)))
        if verbose: print("Verbose:\tSet veto on plane[%s] to %s [clock cycles]" % (str(pn), str(cc)))
        if verbose: labels_help["veto"][pn].setText(self.dolabel(vetos_cc[pn]))
        pass
        
    def SetWidth(self,pn, state):
        cc=int(state/cc_factor)
        widths_cc[pn]=cc
        self.pLog.append("%s: Set width on Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(cc)))
        if verbose: print("Verbose\tSet width on Plane[%s] to %s [clock cycles]" % (str(pn), str(cc)))
        if verbose: labels_help["width"][pn].setText(self.dolabel(widths_cc[pn]))
        pass
    
    def SetMaxRate(self, state):
        global MaxRate_cc #FIXME is this needed
        cc=int(state/cc_factor)
        MaxRate_cc=cc
        self.pLog.append("%s: Set maxrate to %s [clock cycles]" % (self.geTime(), str(cc)))
        if verbose: print("Verbose:\tSet MaxRate to %s [clock cycles]" % (str(cc)))
        if verbose: labels_help["MaxRate"].setText(self.dolabel(MaxRate_cc))
        pass
    
    
    def setSC(self, state):
        if state==True: buttons["SC"].setStyleSheet("background-color: green")
        else: buttons["SC"].setStyleSheet("background-color: grey")
        if verbose: print("Scintillators -> ", state)
        scintillators[0]=state
        pass
    
    def disconnect(self):
        #self.timer.stop()
        tlu=None
        return NOTCONNECTED
    
    
    def connect(self):
        # initialize tlu class
        global tlu
        tlu=PyMaltaTLUPMT.MaltaTLUPMT()
        tlu.SetVerbose(verbose)
        # connect to tlu
        connection_str="udp://%s:%s" % (selected_hostname,str( portname))
        tlu.Connect(connection_str)
        time.sleep(0.1)
        # check version and return connection status
        connectedTLU=tlu.GetVersion()
        if connectedTLU in compatible_TLUs:
            self.pLog.append("%s: Connected to %s" % (self.geTime(),connection_str))
            self.pLog.append("%s: Firmware version %s" % (self.geTime(),str(connectedTLU)))
            print ("Connected to %s" % connection_str)
            return CONNECTED
        else:
            print("TLU versionr returned: %i" % connectedTLU)
            self.pLog.append("%s: Error: Could not connect to %s. Wrong plane? Wrong host? Or firmware not compatible with the GUI version" % (self.geTime(),connection_str))
            return NOTCONNECTED
        pass
    
    def updateRates(self ):
        debugthisfunction=False
        unit=1#KHz
        global t_counters
        # calculate dt
        self.dt=time.time()-self.startingTime
        if debugthisfunction: print("Dt[s]:\t%i" %  self.dt)
        # update rates
        if self.status==RUNNING:
            for x in range(0,numberofplanes+1):
                x=str(x)
                if x=="0": x="L1A"
                cur_value=self.TLU_GetCounter(x)
                #if debugthisfunction: print("last_count\t[%s]:\t%i " % (x, last_count[str(x)]))
                diff=cur_value - last_count[str(x)]
                cur_rate=diff/updateRate
                avg_rate=float(cur_value)/float(self.dt)
                if debugthisfunction: print("[%s]\t. Cur val: %i.\t\t Previous val: %i.\t\tIncreased: %i.\t\tTotal Rate: %f.\t\tDt: %f." % (x, cur_value, last_count[str(x)], diff, avg_rate, float(self.dt)))
                rates[x]=avg_rate
                
                box_counter_dt[0].setPlaceholderText("%i" % (self.dt))
                box_counter_avg_rate[x].setPlaceholderText("%.2f" % (avg_rate))
                box_counter_cur_rate[x].setPlaceholderText("%.2f" % (cur_rate))
                counters[x].setPlaceholderText("%i" % (cur_value))
                last_count[x]=cur_value
                pass
            #Same for the SCS
            #SC_enabled=False
            if SC_enabled==True:
                cur_value=self.TLU_GetCounter("SC")
                diff=cur_value - last_count["SC"]
                cur_rate=diff/updateRate
                avg_rate=float(cur_value)/float(self.dt)
                if debugthisfunction: print("last_count\t[SC]:\t%i" % last_count["SC"])
                rates["SC"]=avg_rate
                box_counter_avg_rate["SC"].setPlaceholderText("%.2f" % (avg_rate/unit))
                box_counter_cur_rate["SC"].setPlaceholderText("%.2f" % (cur_rate/unit))
                counters["SC"].setPlaceholderText("%i" % (cur_value))
                last_count["SC"]=cur_value
                pass
            if self.saveToRoot or self.plotOnFly: self.updatePlot()
            pass
        pass
    
    def updatestatusBar(self):
        self.statusBar().showMessage('%s' % STATUS_NAMES[self.status])
        self.fsm_label.setText(STATUS_NAMES[self.status])
        pass
    
    def save(self):
        name = QtWidgets.QFileDialog.getSaveFileName(None)
        if not name[0]: return
        cwd = os.getcwd()
        f = open(name, "w")
        text = self.pLog.toPlainText()
        f.write(text)
        f.close()
        pass
    
    def closeEvent(self,event):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            #self.stop()
            event.accept()
            sys.exit()
        else:
            event.ignore()
            pass
        pass
    
    def TLU_ChangePort(self, p):
        global portname
        portname=int(p)
        self.pLog.append("%s: New port [%s]" % (self.geTime(),str(portname)))
        pass
    
    def TLU_ChangeHost(self, h):
        global selected_hostname
        selected_hostname=h
        self.pLog.append("%s: New host [%s]" % (self.geTime(),str(selected_hostname)))
        pass
    
    def TLU_GetCounter(self, s_plane):
        value=-1
        value=tlu.GetCounter(s_plane)
        return value
    
    def TLU_reset_counters(self):
        # disable TLU
        tlu.Enable(False)
        time.sleep(0.2)
        # reset counters
        tlu.ResetCounters()
        # reset starting time for time measurements
        self.startingTime = time.time()
        if verbose:
            print("Reseting counters at ", self.startingTime)
            pass
        time.sleep(0.01)
        for c in last_count:
            print
            last_count[c]=0
            pass
        #tlu.Enable(True)FIXME should this be enabled? Not when reseting counters and TLU is disabled!
        pass
    
    def TLU_set_RUNNING(self):
        #OBdebug=True
        #Reset counters
        self.TLU_reset_counters()
        #Set input planes
        tlu.SetNumberOfPlanes(False,False , False,False , False , False, False, False)
        tlu.SetNumberOfPlanes(planes[1], planes[2], planes[3], planes[4], planes[5], False, False, False)
        if debug==True:tlu.GetNumberOfPlanes()
        # Set scintillator
        if SC_enabled==True:
            tlu.SetSCenable(scintillators[0])
            tlu.SetVeto("SC",vetos_cc["SC"])
            tlu.SetWidth("SC",widths_cc["SC"])
            pass
        # Set vetos
        tlu.SetVeto("L1A",MaxRate_cc)
        print("Setting maxrate at %scc " % MaxRate_cc)
        #qif debug==True:tlu.GetMaxRate()
        for p in range(1,numberofplanes+1):
            s=str(p)
            tlu.SetVeto(s, vetos_cc[s])
            if debug==True:tlu.GetVeto(s)
            pass
        # Set widths
        tlu.SetWidth("L1A",widths_cc["L1A"])
        if debug==True:tlu.GetWidth("L1A")
        for p in range(1,numberofplanes+1):
            s=str(p)
            tlu.SetWidth(s, widths_cc[s])
            if debug==True:tlu.GetWidth(s)
            pass
        # Set trigger
        tlu.SendTriggerToPlanes(triggerplanes[1], triggerplanes[2], triggerplanes[3], triggerplanes[4], triggerplanes[5], False, False, False)
        if debug==True:tlu.GetSendTriggerToPlanes()
        # Enable
        tlu.Enable(True)
        # Start GUI loop
        self.timer.start(1000)
        if self.timeoutstarted==False:
            self.timer.timeout.connect(self.updateRates)
            self.timeoutstarted=True
            pass
        # Return state
        return RUNNING
        
    def TLU_set_NOT_RUNNING(self):
        #self.timer.stop()
        tlu.Enable(False)
        #self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
            
    def prepareROOT(self):
        if self.saveToRoot and self.plotOnFly:
            print("ROOT is disabled. Use -p to plot real time")
            pass
        else:
            if self.saveToRoot==True:
                # Prepare root file
                now=datetime.datetime.now()
                now_st=now.strftime("%Y%m%d__%H_%M_%S")
                rfn="TLU_output_%s.root" % now_st
                self.rf = ROOT.TFile(rfn,"RECREATE")
                self.ntuple          = ROOT.TTree("tlu","")
                self.n_timestamp     = array.array('L',(0,)) 
                if "rate"  in to_be_saved: self.n_rate={} 
                if "count" in to_be_saved: self.n_count={}
                for x in planes_to_be_saved:
                    if x == 0 :
                        if "count" in to_be_saved: 
                            self.n_count[x] = array.array('f',(0,))
                            self.ntuple.Branch("count%i" % x, self.n_count[x], "count%i/f" % x)
                            pass
                        if "rate" in to_be_saved: 
                            self.n_rate[x]  = array.array('f',(0,))
                            self.ntuple.Branch("rate%i" % x, self.n_rate[x], "rate%i/i" % x)
                            pass
                        pass
                    pass
                pass
            if self.plotOnFly==True:
                self.cs = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
                self.cs.SetLogy()
                self.lg=ROOT.TLegend(0.2, 0.7, 0.3, 0.9)
                self.lg.SetHeader("Planes","C")
                self.mg = ROOT.TMultiGraph()
                title="Triggers;T[s];N"
                self.mg.SetTitle(title)
                for x in planes_to_be_plotted:
                    if x != 0 :
                        lgname="Plane %i" % x
                        pointSize=1
                        pass
                    else:    
                        lgname="Trigger rate"
                        pointSize=1.2
                        pass
                    self.g[x]=ROOT.TGraph()
                    self.lg.AddEntry(self.g[x],lgname , "p")
                    self.g[x].SetMarkerColor(x+1)
                    self.g[x].SetLineColor(x+1)
                    self.g[x].SetMarkerStyle(20)
                    self.g[x].SetMarkerSize(pointSize)
                    self.mg.Add(self.g[x])
                    pass
                self.lg.Draw()
                self.mg.Draw("APL")
                pass
            pass
        pass

    def updatePlot(self):
        if self.saveToRoot:
            for x in planes_to_be_saved:
                self.n_timestamp[0]=int(time.time())
                if "count"  in to_be_saved: self.n_planes[x]=float(rates[x])
                if "rate"  in to_be_saved: self.n_rates[x]=int(rates[x])
                pass
            self.ntuple.Fill()
            pass
        if self.plotOnFly:
            for x in planes_to_be_plotted:
                if "count" in to_be_plotted: self.g[x].SetPoint(self.g[x].GetN(), float(self.dt), float(last_count[x]))
                if "rate" in to_be_plotted: self.g[x].SetPoint(self.g[x].GetN(), float(self.dt), float(rates[x]))
                self.mg.Add(self.g[x])
                pass
            self.lg.Draw()
            self.cs.Update()
            self.cs.Draw()
            pass
        pass
    
if __name__=="__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("-nop"  ,"--numberofplanes",help="Number of planes",type=int,default=4)
    parser.add_argument("-p"    ,"--plot",help="Enable plotting",action='store_true')
    parser.add_argument("-s"    ,"--save",help="Save to root file",action='store_true')
    parser.add_argument("-v"    ,"--verbose",help="Enable verbose",action='store_true')
    parser.add_argument("-a"    ,"--address",help="ipbus address")

    args=parser.parse_args()
    numberofplanes=args.numberofplanes
    plotOnFly=args.plot
    saveToRoot=args.save
    verbose=args.verbose
    print("Starting TLU GUI v4")
    updateRate=1
    app = QtWidgets.QApplication(sys.argv)
    
    gui = GUI(numberofplanes, plotOnFly, verbose, saveToRoot)
    sys.exit(app.exec_())
