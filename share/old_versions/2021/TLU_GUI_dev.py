#!/usr/bin/env python
#########################################
# Ignacio.Asensi@cern.ch
# Oct 2019
# updates Feb 2020
#########################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLU
import time
import datetime
import argparse
import ROOT
import AtlasStyle
import array

hostname="ep-ade-gw-01"
portname=50007
L1AWidth=100
default_host=4
minport=50000
maxport=50021
debug=True
defaultNumberOfPlanes=4
defaultTriggerWidth=3
defaultL1ATriggerWidth=100#ms

compatible_TLUs=[3]
tlu=""

t_counters={}
t_counters[0]=0
t_counters[1]=0
t_counters[2]=0
t_counters[3]=0
t_counters[4]=0
t_counters[5]=0
t_counters[6]=0


min_width=0
max_width=10
widths={}
widths[0]=2
widths[1]=4
widths[2]=4
widths[3]=4
widths[4]=4
widths[5]=4
widths[6]=4


planes={}
planes[1]=True
planes[2]=True
planes[3]=True
planes[4]=True
planes[5]=False
planes[6]=False

min_veto=0
max_veto=200
vetos={}
vetos[1]=32
vetos[2]=32
vetos[3]=32
vetos[4]=32
vetos[5]=32
vetos[6]=32

triggerplanes={}
triggerplanes[1]=True
triggerplanes[2]=True
triggerplanes[3]=True
triggerplanes[4]=True
triggerplanes[5]=False
triggerplanes[6]=False

last_count={}
last_count[0]=0
last_count[1]=0
last_count[2]=0
last_count[3]=0
last_count[4]=0
last_count[5]=0
last_count[6]=0

connection_str=""
    
class GUI(QtWidgets.QMainWindow):    
    def __init__(self, numberofplanes, enableROOT):
        global verbose
        verbose=True
        super(GUI, self).__init__()
        global counters
        global rcounters
        global tlu
        global buttons
        global startingTime
        global last_read
        global last_count
        global connection_str
        global compatible_TLUs
        global minport
        global maxport
        global L1AWidth
        global rates
        #connection_str="udp://%s:%s" % (hostname,str( portname))
        connection_str="%s" % (hostname)
        startingTime=0
        tlu=PyMaltaTLU.MaltaTLU()
        tlu.SetVerbose(verbose)
        rates={}
        counters={}
        rcounters={}
        buttons={}
        last_read=""
        self.numberofplanes=numberofplanes
        self.dt=""
        #timing
        if enableROOT:
            self.cs=""
            self.g={}
            self.mg =""
            AtlasStyle.SetAtlasStyle()
            self.prepareROOT()
            pass
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateRates)
        
        font = QtGui.QFont("Helvetica");
        QtWidgets.QApplication.setFont(font);
        QtWidgets.QApplication.setStyle("Plastique")
        self.settings=QtCore.QSettings('TLUGUI')
        self.setGeometry(50,50,850,800)
        self.setWindowTitle("TLU GUI")
        wid=QtWidgets.QWidget(self)
        self.setCentralWidget(wid)
        self.status="Disconnected"
        self.log=""
        self.plane=self.settings.value('plane')
        self.mods=[]
        self.tlu=""
        self.pending=False
        
        
        #Planes box--------------------------------------------------------------------------------------------------------
        pane=QtWidgets.QGridLayout(wid)
        bPlanes=QtWidgets.QGroupBox("Planes:",wid)
        pPlanes=QtWidgets.QVBoxLayout(bPlanes)
        pane.addWidget(bPlanes,0,0,4,1)
        
        #for plane in planes:
        for plane in xrange(1,numberofplanes+1):
            b=QtWidgets.QPushButton("Plane %i" % plane,wid)
            b.setCheckable(planes[plane])
            b.toggle()
            b.clicked.connect(lambda state, pn=plane : self.setPlanes(pn,state))
            pPlanes.addWidget(b)
            pass
        
        #Width box--------------------------------------------------------------------------------------------------------
        bWidth=QtWidgets.QGroupBox("Veto:",wid)
        pWidth=QtWidgets.QVBoxLayout(bWidth)
        pane.addWidget(bWidth,0,1,4,1)
        
        for w in xrange(1,numberofplanes+1):
            b=QtWidgets.QSpinBox(wid)
            b.setRange(min_veto,max_veto)
            b.setSingleStep(1)
            b.setValue(vetos[w])
            b.valueChanged.connect(lambda state, pn=w : self.SetTriggerWidth(pn,state))
            pWidth.addWidget(b)
            pass
        
        
        #Long input box--------------------------------------------------------------------------------------------------------
        bLinput=QtWidgets.QGroupBox("Width:",wid)
        pLinput=QtWidgets.QVBoxLayout(bLinput)
        pane.addWidget(bLinput,0,2,4,1)
        

        for p in xrange(1,numberofplanes+1):
            if p== 0 : continue # 0 is longoutput
            b=QtWidgets.QSpinBox(wid)
            b.setRange(min_width,max_width)
            b.setSingleStep(1)
            b.setValue(widths[p])
            b.valueChanged.connect(lambda state, pn=p : self.SetLongInput(pn,state))
            pLinput.addWidget(b)
            pass
        

        
        
        #l1A trigger box--------------------------------------------------------------------------------------------------------
        bTWidth=QtWidgets.QGroupBox("Others:",wid)#Khz
        pTWidth=QtWidgets.QVBoxLayout(bTWidth)
        pane.addWidget(bTWidth,0,3,4,1)
        
        
        
        b=QtWidgets.QLabel(wid)
        b.setText("L1A width [ms]:")
        pTWidth.addWidget(b)
        
        
        #global twidths
        b=QtWidgets.QSpinBox(wid)
        b.setRange(0,200)
        b.setSingleStep(1)
        b.setValue(L1AWidth)
        b.valueChanged.connect(lambda val : self.SetL1AWidth(val))
        pTWidth.addWidget(b)
        
        
        b=QtWidgets.QLabel(wid)
        b.setText("Long output")
        pTWidth.addWidget(b)
        
        
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(0,10)
        b.setSingleStep(1)
        b.setValue(widths[0]) 
        b.valueChanged.connect(lambda val : self.SetLongOutput(val))
        pTWidth.addWidget(b)
        
        
        
        #Provide trigger to box--------------------------------------------------------------------------------------------------------
        bTrig=QtWidgets.QGroupBox("Provide trigger to:",wid)
        pTrig=QtWidgets.QVBoxLayout(bTrig)
        pane.addWidget(bTrig,0,4,4,1)
        
        for n in xrange(1,numberofplanes+1):
            b=QtWidgets.QPushButton("Plane %i" % n,wid)
            b.setCheckable(triggerplanes[n])
            b.setChecked(triggerplanes[n])
            b.clicked.connect(lambda state, pn=n : self.setTriggerplanes(pn,state))
            pTrig.addWidget(b)
            pass
        
        
        
        #Connection box--------------------------------------------------------------------------------------------------------
        bConn=QtWidgets.QGroupBox("Connection:",wid)
        pConn=QtWidgets.QVBoxLayout(bConn)
        pane.addWidget(bConn,0,5,3,1)
        
        b=QtWidgets.QSpinBox(wid)
        b.setRange(minport,maxport)
        b.setSingleStep(1)
        b.setValue(portname)
        b.valueChanged.connect(lambda p : self.TLU_ChangePort(p))
        pConn.addWidget(b)
        
        b=QtWidgets.QComboBox(wid)
        for x in xrange(1,10):
            b.addItem("ep-ade-gw-0%i" % x)
            pass
        b.setCurrentIndex(0)
        b.currentTextChanged.connect(lambda h : self.TLU_ChangeHost(h))
        pConn.addWidget(b)
        
        
        
        #Commands box --------------------------------------------------------------------------------------------------------
        bCmds=QtWidgets.QGroupBox("Commands:",wid)
        pCmds=QtWidgets.QHBoxLayout(bCmds)
        pane.addWidget(bCmds,4,0,1,6)
        buttons["Connect"]=QtWidgets.QPushButton("Connect",wid)
        buttons["Connect"].clicked.connect(self.connect)
        pCmds.addWidget(buttons["Connect"])
        
        buttons["Enable"]=QtWidgets.QPushButton("Enable",wid)
        buttons["Enable"].setStyleSheet("background-color: green")
        buttons["Enable"].clicked.connect(self.TLU_enable)
        pCmds.addWidget(buttons["Enable"])
        
        
        
        buttons["Disable"]=QtWidgets.QPushButton("Disable",wid)
        buttons["Disable"].setStyleSheet("background-color: orange")
        buttons["Disable"].clicked.connect(self.TLU_disable)
        pCmds.addWidget(buttons["Disable"])
        
        buttons["Close"]=QtWidgets.QPushButton("Close",wid)
        buttons["Close"].setStyleSheet("background-color: red")
        buttons["Close"].clicked.connect(self.stop)
        pCmds.addWidget(buttons["Close"])
        
        buttons["ResetCounters"]=QtWidgets.QPushButton("Reset counters",wid)
        buttons["ResetCounters"].clicked.connect(self.TLU_reset_counters)
        pCmds.addWidget(buttons["ResetCounters"])
        
        self.disableButtons()
        
        #Debug box --------------------------------------------------------------------------------------------------------
        self.pLog = QtWidgets.QTextEdit(wid)
        self.pLog.resize(500,500)
        pane.addWidget(self.pLog,5,0,1,4)
        
        #Counters
        #Rate box--------------------------------------------------------------------------------------------------------
        bRCounts=QtWidgets.QGroupBox("Rates:",wid)
        pRCounts=QtWidgets.QVBoxLayout(bRCounts)
        pane.addWidget(bRCounts,5,4,1,1)
        label="Rate L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pRCounts.addWidget(b)
        rcounters[0]=QtWidgets.QLineEdit(wid)
        rcounters[0].setPlaceholderText("")
        pRCounts.addWidget(rcounters[0])
        for x in xrange(1,self.numberofplanes+1):
            #if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane rate %i" % x
            b.setText(label)
            pRCounts.addWidget(b)
            rcounters[x]=QtWidgets.QLineEdit(wid)
            rcounters[x].setPlaceholderText("")
            pRCounts.addWidget(rcounters[x])
             #   pass
            pass
        
        #Counters box--------------------------------------------------------------------------------------------------------
        bCounts=QtWidgets.QGroupBox("Counters:",wid)
        pCounts=QtWidgets.QVBoxLayout(bCounts)
        pane.addWidget(bCounts,5,5,1,5)
        label="Counter L1A"
        b=QtWidgets.QLabel(wid)
        b.setText(label)
        pCounts.addWidget(b)
        counters[0]=QtWidgets.QLineEdit(wid)
        counters[0].setPlaceholderText("")
        pCounts.addWidget(counters[0])
        for x in xrange(1,self.numberofplanes+1):
           # if planes[x]==True:
            b=QtWidgets.QLabel(wid)
            label="Plane %i" % x
            b.setText(label)
            pCounts.addWidget(b)
            counters[x]=QtWidgets.QLineEdit(wid)
            counters[x].setPlaceholderText("")
            pCounts.addWidget(counters[x])
          #  pass
            pass
        self.show()
        pass
    
    def geTime(self):
        now=datetime.datetime.now()
        return now.strftime("%Y/%m/%d %H:%M:%S")
    
    def setPlanes(self,pn, state):
        #print ("Set active plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        planes[pn]=state
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def setTriggerplanes(self,pn, state):
        #print ("Set active plane: %s as %s" % (str(pn), str(state)))
        global triggerplanes
        self.pending=True
        triggerplanes[pn]=state
        self.pLog.append("%s: Plane[%s] %s" % (self.geTime(),str(pn), str(state)))
        pass
    
    def SetTriggerWidth(self,pn, state):
        #print ("SetTriggerWidth plane: %s to %s" % (str(pn), str(state)))
        self.pending=True
        self.pLog.append("%s: Trigger width of Plane[%s] to %s [clock cycles]" % (self.geTime(),str(pn), str(state)))
        vetos[pn]=state
        pass
    
    def SetLongInput(self,pn, state):
        #print ("Set long input plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        widths[pn]=state
        self.pLog.append("%s: Set long input on Plane[%s] to %s" % (self.geTime(),str(pn), str(state)))
        #self.settings.setValue('scan',v)
        pass
    
    def SetLongOutput(self, state):
        #print ("Set long input plane: %s as %s" % (str(pn), str(state)))
        self.pending=True
        widths[0]=state
        self.pLog.append("%s: Set long input on Plane[%s] to %s" % (self.geTime(),str(0), str(state)))
        #self.settings.setValue('scan',v)
        pass
    
    def SetL1AWidth(self, val):
        global L1AWidth
        self.pending=True
        L1AWidth=val
        self.pLog.append("%s: Set L1AWidth to %s [ms]" % (self.geTime(),str(val)))
        pass
    
    def TLU_reset_counters(self):
        if self.status=="Enabled":
            global startingTime
            tlu.Enable(False)
            time.sleep(0.2)
            for x in last_count:
                print "14",x
                last_count[x]=0
                pass
            tlu.ResetCounters()
            startingTime = time.time()
            time.sleep(0.2)
            tlu.Enable(True)
            pass
        else:
            print "TLU status is [%s]. Cannot reset counters. " % self.status
            pass
        pass
    def connect(self):
        connection_str="udp://%s:%s" % (hostname,str( portname))
        if self.status == "Connected":
            self.pLog.append("Already connected. Disconnecting....")
            self.timer.stop()
            self.TLU_disable()
            self.status = "Disconnected"
            buttons["Disable"].setEnabled(False)
            buttons["Enable"].setEnabled(False)
            pass
        else:
            self.TLU_connect(connection_str)
            time.sleep(0.2)
            connectedTLU=tlu.GetVersion()
            tlu.SetVerbose(verbose)
            if connectedTLU in compatible_TLUs:
                self.status="Connected"
                self.timer.start(1000)
                self.pLog.append("%s: Connected to %s" % (self.geTime(),connection_str))
                self.pLog.append("%s: Firmware version %s" % (self.geTime(),str(connectedTLU)))
                print ("Connected to %s" % connection_str)
                buttons["Disable"].setEnabled(False)
                buttons["Enable"].setEnabled(True)
                pass
            else:
                self.status="Disconnected"
                self.pLog.append("%s: Error: Could not connect to %s. Wrong plane? Wrong host?" % (self.geTime(),connection_str))
                pass
            pass
        self.updatestatusBar()
        #timer = QtCore.QTimer()
        #timer.timeout.connect(self.updateRates)
        #timer.start(1000)  # every 10,000 milliseconds
        pass
    
   
    

    def updateRates(self):
        global t_counters
        if self.status=="Enabled":
            for x in xrange(0,numberofplanes+1):
                value=self.TLU_GetCounter(x)
                print "Plane [%i] \t Value [%s] \t Last count [%s]" % (x, str(value),str(last_count[x]) )
                self.dt=time.time()-startingTime
                diff=value - last_count[x]
                #print "Updating rate plane diff", diff
                rate=diff/updateRate
                totalrate=float(value)/float(self.dt)
                #print "Updating rate plane totalrate", totalrate
                #print "Updating rate plane value", value
                rates[x]=totalrate
                rcounters[x].setPlaceholderText("%i [Tot %.2f] Hz" % (rate, totalrate))
                counters[x].setPlaceholderText(str(value))
                last_count[x]=value
                pass
            if enableROOT==True: self.updatePlot()
            pass
        pass
    
    def getRate(self, value):
        pass
    
    def updatestatusBar(self):
        self.statusBar().showMessage('%s' % self.status)
        pass
    
    def save(self):
        name = QtWidgets.QFileDialog.getSaveFileName(None)
        if not name[0]: return
        cwd = os.getcwd()
        f = open(name, "w")
        text = self.pLog.toPlainText()
        f.write(text)
        f.close()
        pass
    
    def stop(self):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                "Are you sure you wish to quit?",
                QtWidgets.QMessageBox.Yes |
                QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            self.timer.stop()
            if enableROOT==True:
                self.ntuple.Write()
                self.mg.SetTitle("Trigger rate;T[s];R[times]")
                self.mg.Draw("AP")
                self.mg.Write("mg")
                self.rf.Close()
                pass
            self.TLU_disable()
            self.killproc()
            pass
        else:
            pass
        pass
    
    def closeEvent(self,event):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            self.stop()
            event.accept()
            sys.exit()
        else:
            event.ignore()
            pass
        pass

    def execute(self,cmd):
        popen = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True,
                                 universal_newlines=True,
                                 preexec_fn=os.setsid)
        self.pid=popen.pid
        for stdout_line in iter(popen.stdout.readline, ""):
            yield stdout_line 
            pass
        popen.stdout.close()
        return_code = popen.wait()
        self.pid=0
        pass
    
    def killproc(self):
        #if self.pid==0: return
        #print("Kill process %i" % self.pid)
        sys.exit()
        pass
    
    #TLU
    
    def TLU_ChangePort(self, p):
        global portname
        portname=int(p)
        self.pLog.append("%s: New port [%s]" % (self.geTime(),str(portname)))
        pass
    
    def TLU_ChangeHost(self, h):
        global hostname
        hostname=h
        self.pLog.append("%s: New host [%s]" % (self.geTime(),str(hostname)))
        pass
    
    def TLU_GetCounter(self, plane):
        value=-1
        value=tlu.GetCounterTrigger(plane)
        return value
    
    def TLU_connect(self, str):
        tlu.Connect(str)
        print ("Connecting to %s" % str)
        pass
    
    def TLU_enable(self):
        
        global startingTime
        try:
            time.sleep(0.1)
            #Reset counters
            tlu.ResetCounters()
            #input planes
            tlu.SetPlanes(planes[1], planes[2], planes[3], planes[4], planes[5], False, False, False)
            if debug==True:tlu.GetPlanes()
            #vetos  
            for p in vetos:
                tlu.SetTriggerWidth(p, vetos[p])
                if debug==True:tlu.GetTriggerWidth(p)
            #long inputs
            for p in widths:
                tlu.SetLongInput(p, widths[p])
                if debug==True:tlu.GetLongInput(p)
                pass
            #trigger width
            tlu.SetL1AWidth(L1AWidth)
            if debug==True:tlu.GetL1AWidth()
            #outputs
            tlu.SetCoincidenceOutputPlanes(triggerplanes[1], triggerplanes[2], triggerplanes[3], triggerplanes[4], triggerplanes[5], False, False, False)
            if debug==True:tlu.GetCoincidenceOutputPlanes()
            #All ok
            tlu.Enable(True)
            print "startingTime", startingTime
            if startingTime==0:
                startingTime=time.time()
                pass
            self.status="Enabled"
            self.pending=False
            buttons["Disable"].setEnabled(True)
            buttons["Enable"].setEnabled(False)
            pass
        except:
            print "Unexpected error...."
            self.status="Disabled"
            #tlu.Enable(False)
            self.TLU_disable()
            pass
        self.updatestatusBar()
        self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
    
    def TLU_disable(self):
        buttons["Disable"].setEnabled(False)
        buttons["Enable"].setEnabled(True)
        tlu.Enable(False)
        self.status="Disabled"
        self.updatestatusBar()
        self.pLog.append("%s: %s" % (self.geTime(),self.status))
        pass
    
    
    #GUI functions
    def enableButtons(self):
        buttons["Enable"].setEnabled(True)
        buttons["Disable"].setEnabled(True)
        pass
    
    def disableButtons(self):
        buttons["Enable"].setEnabled(False)
        buttons["Disable"].setEnabled(False)
        pass
    def prepareROOT(self):
        if enableROOT==False:
            print "ROOT NOT ENABLED"
            pass
        else:
            now=datetime.datetime.now()
            now_st=now.strftime("%Y%m%d__%H_%M_%S")
            rfn="TLU_output_%s.root" % now_st
            self.rf = ROOT.TFile(rfn,"RECREATE")
            self.ntuple          = ROOT.TTree("tlu","")
            self.n_timestamp     = array.array('L',(0,)) 
            #self.n_rates={} 
            self.n_planes={}
            for x in xrange(1,self.numberofplanes+1):
                if x == 0 :
                    self.n_planes[x]     = array.array('f',(0,))
                    #self.n_rates[x]     = array.array('f',(0,))
                    self.ntuple.Branch("plane%i" % x, self.n_planes[x], "plane%i/f" % x)
                    #self.ntuple.Branch("rate%i" % x, self.n_rates[x], "rate%i/i" % x)
                    pass
                pass
            
            self.cs = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
            self.cs.SetLogy()
            #self.lg=ROOT.TLegend(0.2, 0.6, 0.4, 0.9)
            self.lg=ROOT.TLegend(0.2, 0.7, 0.3, 0.9)
            self.lg.SetHeader("Planes","C")
            self.mg = ROOT.TMultiGraph()
            title="Triggers;T[s];N"
            self.mg.SetTitle(title)
            for x in xrange(0,self.numberofplanes+1):
                
                
                if x==0:
                    self.g[x]=ROOT.TGraph()
                    lgname="Trigger rate"
                    pointSize=1.2
                    self.lg.AddEntry(self.g[x],lgname , "p")
                    self.g[x].SetMarkerColor(x+1)
                    self.g[x].SetLineColor(x+1)
                    self.g[x].SetMarkerStyle(20)
                    self.g[x].SetMarkerSize(pointSize)
                    #self.g[x].SetPoint(self.g[x].GetN(), x, x)
                    self.mg.Add(self.g[x])
                else:
                    lgname="Plane %i" % x
                    pointSize=1
                    pass
                pass
            pass
        self.lg.Draw()
        self.mg.Draw("APL")
        pass
    def updatePlot(self):
        #print "dt 2 :", self.dt
        for x in xrange(0,self.numberofplanes+1):
            if x==0:
                self.g[x].SetPoint(self.g[x].GetN(), float(self.dt), float(last_count[x]))
                self.mg.Add(self.g[x])
                self.n_timestamp[0]=int(time.time())
                #print rates[x]
                self.n_planes[x]=float(rates[x])
                pass
            else:
                self.n_planes[x]=float(last_count[x])
                pass
            #self.n_rates[x]=int(rates[x])
            self.ntuple.Fill()
            pass
        self.lg.Draw()
        self.cs.Update()
        self.cs.Draw()
        pass
    
    pass

if __name__=="__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("-nop","--numberofplanes",help="Number of planes",type=int,default=4)
    parser.add_argument("-p","--plot",help="Enable plotting",action='store_true')
    parser.add_argument("-a","--address",help="ipbus address")

    args=parser.parse_args()
    numberofplanes=args.numberofplanes
    enableROOT=args.plot
    print("Starting TLU GUI")
    updateRate=1
    app = QtWidgets.QApplication(sys.argv)
    
    gui = GUI(numberofplanes, enableROOT)
    '''
    timer = QtCore.QTimer()
    timer.timeout.connect(gui.updateRCounter)
    timer.timeout.connect(gui.updateCounter)
    timer.start(updateRate*1000)
    '''
    sys.exit(app.exec_())

