####################################################
# This is the configuration file for the TLU command line interface: TLU_cmd.py 
# THIS IS PYTHON CODE. USE PYTHON SYNTAX
# ignacio.asensi@cern.ch
#
####################################################
conf={}
#conf["connect_string"]="udp://ep-ade-gw-01:50007"
conf["connect_string"]="udp://ep-ade-gw-02:50000"
conf["numberofplanes"]=5
conf["planes"]={}
conf["trigger"]={}
conf["veto"]={}
conf["width"]={}
conf["SC_enabled"]=True# True if there is one scintillator
conf["HGTD_enabled"]=False# True if there is one scintillator


# Listen from planes. True/False
conf["planes"]["1"]=True # 2nd telescope plane (checked in May 2024)
conf["planes"]["2"]=False # 3rd tel. plane 
conf["planes"]["3"]=True # 5th tel. plane
conf["planes"]["4"]=True # 6th tel. plane
conf["planes"]["5"]=False # False (TLU input is not connected)
conf["planes"]["HGTD"]=False #False
conf["planes"]["SC"]=True #scintillator

# Provide trigger to planes. True/False
conf["trigger"]["1"]=True
conf["trigger"]["2"]=True
conf["trigger"]["3"]=True
conf["trigger"]["4"]=True
conf["trigger"]["5"]=True



# Veto in ns (will be converted n/3.125 to clock cycles)
#conf["veto"]["L1A"]=10000 # for high intensity: 60000 #10000 # 6000 #L1A veto time 
#conf["veto"]["L1A"]=20000 # set to 20000 for intensity 4e5
conf["veto"]["L1A"]=8000 # set to 8000 for intensity 4e4 #Anusree
conf["veto"]["1"]=44
conf["veto"]["2"]=44
conf["veto"]["3"]=44
conf["veto"]["4"]=44
conf["veto"]["5"]=44
conf["veto"]["SC"]=33

# Signal width  in ns (will be converted n/3.125 to clock cycles)
conf["width"]["L1A"]=120
conf["width"]["1"]=30
conf["width"]["2"]=30
conf["width"]["3"]=30
conf["width"]["4"]=30
conf["width"]["5"]=30
conf["width"]["SC"]=158
