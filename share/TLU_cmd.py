#!/usr/bin/env python
##############################################
# TLU command tool. This tool requires a configuration file
# ignacio.asensi@cern.ch
# 2020 Sept
# 2023 February updates. Versions are unified
##############################################

import os
import sys
import signal
import subprocess 
from PyQt5 import QtWidgets,QtCore,QtGui
import PyMaltaTLU
import time
import datetime
import argparse
import ROOT
import array


tlulog=0

class cmd ():
    def __init__(self, cnf, v):
        global tlu
        global conf
        global total_triggers, update_freq
        
        self.verbose= v
        self.reading_counter=0
        self.startingTime=int(time.time())
        self.dt=0
        self.cf=3.125 # conversion factor for 320Mhz clock
        self.print_counters_rate=False
        conf=cnf
        tlu=PyMaltaTLU.MaltaTLU()
        tlu.SetVerbose(self.verbose)
        tlu.Connect(conf["connect_string"])
        time.sleep(0.1)
        connectedTLU=tlu.GetVersion()
        self.last_count={}
        self.last_count["L1A"]=0
        self.diff=0
        self.total_triggers=0
        update_freq=100
        total_triggers=0
        if connectedTLU not in compatible_TLUs:
            print("ERROR: Firmware is not in the list of supported FWs. Process will continue...")
            pass
        pass
    def start(self):
        # disable TLU
        if self.verbose: print("Configuration [from file]:")
        if self.verbose: print(conf)
        tlu.Enable(False)
        time.sleep(0.01)
        tlu.SetNumberOfPlanes(False, False, False,False , False , False, False, False)        
        # Planes
        for p in conf["planes"]:
            if conf["planes"][p]==False or p=="SC" or p=="HGTD": continue
            if self.verbose: print("Plane %s is enabled. Veto\t%s[ns].\tWidth\t%s[ns]." % (p,conf["veto"][p],conf["width"][p]))
            tlu.SetVeto(p, int(conf["veto"][p]/self.cf))
            tlu.SetWidth(p, int(conf["width"][p]/self.cf))
            pass
        #L1A
        tlu.SetWidth("L1A", int(conf["width"]["L1A"]/self.cf))
        tlu.SetVeto("L1A", int(conf["veto"]["L1A"]/self.cf))
        if self.verbose: print("L1A.\tVeto\t%s[ns].\tWidth\t%s[ns]." % (conf["veto"]["L1A"],conf["width"]["L1A"]))
        # Others
        if conf["SC_enabled"]==True:
            tlu.SetSCenable(True)
            tlu.SetVeto("SC", int(conf["veto"]["SC"]/self.cf))
            tlu.SetWidth("SC", int(conf["width"]["SC"]/self.cf))
            if self.verbose: print("SC is enabled.\tVeto\t%s[ns].\tWidth\t%s[ns]." % (conf["veto"]["SC"],conf["width"]["SC"]))
            pass
        else:
            tlu.SetSCenable(False)
            pass
        if conf["HGTD_enabled"]==True:
            tlu.SetHGTDenable(True)
            if self.verbose: print("HGTD is enabled")
            pass
        # Set trigger
        tlu.SendTriggerToPlanes(conf["trigger"]["1"],conf["trigger"]["2"],conf["trigger"]["3"],conf["trigger"]["4"], True, True, True, True, True, True)
        # Enable
        tlu.SetNumberOfPlanes(conf["planes"]["1"], conf["planes"]["2"], conf["planes"]["3"], conf["planes"]["4"], conf["planes"]["5"], False,False, False)
        tlu.ResetCounters()
        tlu.Enable(True)
        self.reading_counter=0
        pass
    def stop(self):
        tlu.Enable(False)
        tlu.ResetCounters()
        pass
    def TLU_GetCounter(self, s_plane):
        value=-1
        value=tlu.GetCounter(s_plane)
        return value

    def updateRates(self):
        global tlulog
        x="L1A"# for the moment we only monitor L1A
        unit=1#KHz
        cur_value=0
        m_value=0
        m_value= self.TLU_GetCounter(x)
        self.dt= int(time.time())- self.startingTime
        #print("cur_value: %i" % m_value)
        self.reading_counter+= 1
        self.total_triggers= m_value
        if (float(self.dt)%10<0.1 and self.print_counters_rate == True):
            diff= cur_value - self.last_count[x]
            cur_rate= diff/update_freq
            self.last_count[x]= cur_value
            line="Triggers (FW): %i.\tDt[s]: %i. \tTimes updated: %i." % (self.total_triggers, self.dt, self.reading_counter)
            print(line)
            tlulog.write(line)
            self.print_counters_rate=False
            pass
        if (float(self.dt)%10>0.1 and self.print_counters_rate== False): self.print_counters_rate= True
        return self.total_triggers
    pass

if __name__=="__main__":
    # Settings
    compatible_TLUs=[2,7,8]
    cont=True
    def signal_handler(signal, frame):
        print('You pressed ctrl+C')
        print("Stop TLU run")
        global cont
        c.stop()
        cont = False
        return
        pass

    signal.signal(signal.SIGINT, signal_handler)
    # Arguments
    parser=argparse.ArgumentParser()
    parser.add_argument('--start',help="Start run", action='store_true')
    parser.add_argument('--stop',help="Stop run", action='store_true')
    parser.add_argument('-d','--durationlimit',help="Time [seconds] to stop",type=int,default=10000000)
    parser.add_argument('-t','--triggerlimit',help="Number of triggers to stop.", type=int, default=50000000)
    parser.add_argument('-r','--runNumber',help="RunNumber",type=int,default=0)
    #parser.add_argument('--delay',help="Delay time to start [minutes]. No delay by default",type=int,default=0)
    parser.add_argument('-c','--conf',help="Configuration file. Default conf_TLU_cmd.py", default="conf_TLU_cmd.py")
    parser.add_argument('-v','--verbose',help="Verbose mode",action='store_true')
    parser.add_argument('-b','--batch',help='sort by noise',action='store_true')
    parser.add_argument('--counters',help="Debug counters",action='store_true')
    args=parser.parse_args()
    
    start=args.start
    stop=args.stop
    durationlimit=args.durationlimit
    triggerlimit=args.triggerlimit
    verbose=args.verbose
    debug_counters=args.counters#deprecated
    conf_file=args.conf
    tlulogpath="%s/tlu%06i.txt"%(os.getenv("MALTA_LOGS_PATH"),args.runNumber)
    tlulog = open (tlulogpath,'a' if os.path.exists(tlulogpath) else 'w')
    if ".py" in conf_file:
        import importlib
        config = importlib.import_module(conf_file.replace(".py", ""))
        pass
    else:
        print("Error in configuration file!! Check is python file")
        cont=False
        pass


    c=cmd(config.conf, verbose)
    #print(dir(c))
    #Action!
    if cont==False:
        print("Stop TLU requested")
    if start and stop:
        print("Error: start and stop are not compatible")
        pass
    elif start:
        print("TLU is set to: Start Run")
        c.start()
        while cont:
            #print("Loop")
            time.sleep(0.1)
            current_triggers=c.updateRates()
            if cont==False:
                print("Run unexpected stop")
                c.stop()
                print("Total triggers: %i" % current_triggers)
                break
            elif current_triggers >= triggerlimit: 
                print("Trigger limit reached. Total triggers: %i" % current_triggers)
                c.stop()
                break
            elif c.dt >= durationlimit: 
                print("Time is over [%i s]. Total triggers: %i" % (durationlimit,current_triggers))
                c.stop()
                break
            pass
        pass
    elif stop:
        c.stop()
        print("TLU is set to: Stop")
        pass
    else:
        print("No action requested!")
    pass

