# MaltaTLU {#PackageMaltaTLU}

MaltaTLU is the software for the MALTA TLU operation.

## Applications

- TLU_GUI: GUI to manually operate the TLU, Start,stop and configure TLU settings. 
- TLU_CMD: CMD tool to launch automatic runs from terminal or other scripts. 

## Libraries

- Malta2: Library containing DAQ classes for MALTA2

## Malta2 library

- MaltaTLUv3: Control TLU
