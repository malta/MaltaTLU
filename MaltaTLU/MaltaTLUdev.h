#ifndef MALTATLUDEV_H
#define MALTATLUDEV_H

#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <stdint.h>
#include "ipbus/Uhal.h"



class MaltaTLUdev{
 public:

  //! FIFO status register
  static const uint32_t TLU_VERSION    		= 4;
  static const uint32_t TLU_MAIN    		= 5;
  static constexpr uint32_t TLU_VETO[5]   	={7,8,9,10,11};//Maxrate(L1A),p1&p2&p3&p4,SC,SC2,HGTD
  static constexpr uint32_t TLU_WIDTH[5]   	={12,13,14,15,16};//L1A,p1&p2&p3&p4,SC,SC2,HGTD
  static constexpr uint32_t TLU_COUNTER	[9] ={17,18,19,20,21,22,23,24,25};//L1A, p1,p2,p3,p4,SC,SC2,HGTD

  //Defaults
  static const uint32_t d_MAXRATE    	= 100;//100'000
  static const uint32_t d_TRIGGER_WIDTH = 0x3;
  static const uint32_t d_LONG_INPUT 	= 0;

  static const uint32_t MS_TO_CYCLES	= 950;//FIXME

  MaltaTLUdev();
  ~MaltaTLUdev();
  uint32_t GetVersion();
  void SetVerbose(bool val);
  void Connect(std::string address);
  uint32_t GetCounter(std::string plane);
  //void SetMaxRate(int value);
  void SetVeto(std::string plane, int value);
  void SetWidth(std::string plane, int value);
  uint32_t GetVeto(std::string plane);
  uint32_t GetWidth(std::string plane);
  void SetMain(bool enable, bool plane1, bool plane2, bool plane3, bool plane4, bool SCenable, bool SC2enable, bool HGTDenable, bool diffOutenable);
  uint32_t GetMain();
  //void SetSCenable(bool val);
  //  void SetSC2enable(bool val);
  //void SetHGTDenable(bool val);
  //void Enable(bool flag);

  std::stringstream systemcall(std::string cmd);
 private:
  bool m_verbose;
  ipbus::Uhal * m_ipb;
  int FWlimit_veto=50000;
  int FWlimit_width=50000;

};
#endif

