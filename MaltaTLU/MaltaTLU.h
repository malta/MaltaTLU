#ifndef MALTATLU_H
#define MALTATLU_H

#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <stdint.h>
#include "ipbus/Uhal.h"



class MaltaTLU{
 public:

  //! FIFO status register
  static const uint32_t TLU_VERSION    		= 63;
  //counters
  static constexpr uint32_t TLU_COUNTER	[9]    	={80,81,82,83,84,85,86,87,88};//L1A:L1A, Planes: 81 to 85, SC: 86, SC2:87, HGTD: 88
    
  //settings
  static constexpr uint32_t TLU_VETO[9]   	={70,71,72,73,74,75,76,79,60};//70:L1A, from 71 to 76:planes, 79:SC, 60:SC2
  static const uint32_t TLU_PLANES    		= 66;
  static const uint32_t TLU_COIN_OUT_PLANES    	= 67;
  //static const uint32_t TLU_LONG_INOUT		= 90;
  static const uint32_t TLU_ENABLE		= 91;
  static const uint32_t TLU_RESET_COUNTERS	= 92;
  static constexpr uint32_t TLU_WIDTH[9]   	={93,94,95,96,97,98,33,99,61};//93:L1A, 94 to 98:planes, 99:SC, 61:SC2
  static const uint32_t TLU_SC_ENABLE		= 50;
  static const uint32_t TLU_SC2_ENABLE		= 51;




  //Defaults
  static const uint32_t d_MAXRATE    	= 100;//100'000
  static const uint32_t d_TRIGGER_WIDTH 	= 0x3;
  static const uint32_t d_LONG_INPUT 	= 0;

  static const uint32_t MS_TO_CYCLES	= 950;//FIXME
  
  MaltaTLU();
  ~MaltaTLU();
  uint32_t GetVersion();
  void SetVerbose(bool val);
  void Connect(std::string address);
  uint32_t GetCounter(std::string plane);
  //void SetMaxRate(int value);
  void SetVeto(std::string plane, int value);
  void SetWidth(std::string plane, int value);
  uint32_t GetVeto(std::string plane);
  uint32_t GetWidth(std::string plane);
  void SetNumberOfPlanes(bool plane1, bool plane2, bool plane3, bool plane4, bool plane5, bool plane6, bool plane7, bool plane8);
  uint32_t GetNumberOfPlanes();
  void SendTriggerToPlanes(bool plane1, bool plane2, bool plane3, bool plane4, bool plane5, bool plane6, bool plane7, bool plane8, bool plane9, bool plane10);
  uint32_t GetSendTriggerToPlanes();
  void SetSCenable(bool val);
  void SetSC2enable(bool val);
  void SetHGTDenable(bool val);
  void Enable(bool flag);
  void Reset();
  void ResetCounters();
  
  std::stringstream systemcall(std::string cmd);
 private:
  bool m_verbose;
  ipbus::Uhal * m_ipb;

  //SetDeadTime();
  //GetDeadTime();
  
  //----Reduce number of trigger for high beam
  //SetPrescale(int scale);
  //GetPrescale(int scale);
};
#endif

